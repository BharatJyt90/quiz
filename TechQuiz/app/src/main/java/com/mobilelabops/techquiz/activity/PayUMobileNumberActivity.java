package com.mobilelabops.techquiz.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;

import com.mobilelabops.techquiz.BuildConfig;
import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.payment.PayUMoney;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.CryptoUtils;
import com.mobilelabops.techquiz.util.Util;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;

import org.json.JSONException;
import org.json.JSONObject;

public class PayUMobileNumberActivity extends BaseActivity implements IServerCallBack {

    private CustomEditText mPhoneNum;
    private User mCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_umobile_number);

        mCurrentUser = getIntent().getParcelableExtra(User.class.getSimpleName());
        setTitle("");
        mPhoneNum = (CustomEditText) findViewById(R.id.mobile_num);

        int status = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (status != PackageManager.PERMISSION_GRANTED) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        1);
        }else{

            TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();

            mPhoneNum.setText(mPhoneNumber);
        }



    }

    public void onClick(View v){

        String phoneNum = mPhoneNum.getText().toString();

        if(android.util.Patterns.PHONE.matcher(phoneNum).matches()){

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("firstName",mCurrentUser.getName());
                jsonObject.put("email",mCurrentUser.getEmailId());

                ServerRequest request = new ServerRequest(Constant.KEY_GET_PAYU_HASH,jsonObject.toString(), Util.getJsonContentTypeHeader(this));

                ServerAsynTask asynTask = new ServerAsynTask(this);
                asynTask.execute(request);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        else{

            mPhoneNum.setError("Please enter a valid phone number");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay!
            int status = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (status == PackageManager.PERMISSION_GRANTED) {

                TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                String mPhoneNumber = tMgr.getLine1Number();
                mPhoneNum.setText(mPhoneNumber);
            }
        }
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        closeProgressDialog();

        if(result != null && result.getResponseCode() == 200){

            try {

                JSONObject jsonObject = new JSONObject(result.getResponseString());

                if(jsonObject.has(Constant.KEY_STATUS)) {

                    String hash = jsonObject.getString("hash");

                    String key = jsonObject.getString("k");
                    String merchantId = getString(R.string.payU_mId);
                    int amount = jsonObject.getInt("amount");
                    String txnId = jsonObject.getString("txnId");

                    PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

                    String phoneNum = mPhoneNum.getText().toString();
                    builder.setAmount(amount)
                            .setTxnId(txnId)
                            .setPhone(phoneNum)
                            .setProductName(PayUMoney.productName)
                            .setFirstName(mCurrentUser.getName())
                            .setEmail(mCurrentUser.getEmailId())
                            .setsUrl(PayUMoney.sUrl)
                            .setfUrl(PayUMoney.fUrl)
                            .setUdf1("")
                            .setUdf2("")
                            .setUdf3("")
                            .setUdf4("")
                            .setUdf5("")
                            .setIsDebug(PaymentGatewaySelectionActivity.DEBUG)
                            //.setIsDebug(BuildConfig.DEBUG)
                            .setKey(key)
                            .setMerchantId(merchantId);

                    com.payumoney.core.PayUmoneySdkInitializer.PaymentParam paymentParam = builder.build();
                    paymentParam.setMerchantHash(hash);

                    PayUmoneyFlowManager.startPayUMoneyFlow(
                            paymentParam,this, R.style.payuAppTheme_Green, false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.setResult(resultCode,data);
        finish();
    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog("Please wait...");
    }
}

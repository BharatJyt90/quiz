package com.mobilelabops.techquiz.loginhelper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.mobilelabops.techquiz.activity.LoginActivity;

/**
 * Created by bhazarix on 9/11/2017.
 */

public class GoogleLoginHelper implements GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
   // private AppCompatActivity activity;

    private static GoogleLoginHelper mInstance;

    private ILoginHelper mLoginHelper;

    public static final int RC_GOOGLE_SIGN_IN = 1;

    public void setLoginHelper(ILoginHelper helper){

        mLoginHelper = helper;
    }

    public static GoogleLoginHelper getInstance() {

        if (mInstance == null) {
            mInstance = new GoogleLoginHelper();

        }
        return mInstance;
    }

    public void fireSignInIntent(Activity activity){

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(getGoogleApiClient());
        activity.startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        handleSignInResult(result);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("Google Login", "handleSignInResult:" + result.isSuccess());

        if (result.isSuccess()) {

//            Person person  = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String name = acct.getDisplayName();
            String email = acct.getEmail();
            Uri photoUrl = acct.getPhotoUrl();
  //          String birthDay = person.getBirthday();

            String profile_pic = null;

            if(photoUrl != null)
                profile_pic = photoUrl.toString();

            if(mLoginHelper != null){

                mLoginHelper.onLoginSuccess(email,name,profile_pic,null, LoginActivity.LOGIN_TYPE_GOOGLE);
            }

        }
    }

    public boolean checkGoogleLogin(){

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(getGoogleApiClient());
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d("Google Login", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);

            return true;

        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.

            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {

                    handleSignInResult(googleSignInResult);
                }
            });

            return false;
        }

    }

    public void init(AppCompatActivity activity) {

        // [START configure_signin]
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestProfile()
                    /*.requestIdToken("33361209586-u319e3iq12it277avp8tjg79aga9cn7n.apps.googleusercontent.com")*/
                .requestId()
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("Google Login", "onConnectionFailed:" + connectionResult);
    }

    public void signOut(final ResultCallback<Status> callback) {

        mGoogleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {

                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(callback);
            }

            @Override
            public void onConnectionSuspended(int i) {


            }
        });

        mGoogleApiClient.connect();

        /**/
    }


    public GoogleApiClient getGoogleApiClient() {

        return mGoogleApiClient;
    }
}
package com.mobilelabops.techquiz.util;

/**
 * Created by bhazarix on 9/2/2017.
 */

public class Constant {

    public static final String KEY_IP = "http://54.218.250.45:8080";

    public static final String KEY_UPLOAD_IMAGE_IP = KEY_IP+"/fileservice/rest/generic/upload";
    public static final String KEY_DOWNLOAD_IMAGE_IP = KEY_IP+"/fileservice/rest/generic/download/";

    public static final String KEY_BASE_URL = KEY_IP+"/TechQuiz/webresources/";

    public static final String KEY_REG_URL = KEY_BASE_URL+"UserDataResource/register";
    public static final String KEY_USER_VERIFY_URL = KEY_BASE_URL+"UserDataResource/verifyEmailCode";
    public static final String KEY_USER_REQ_CODE_URL = KEY_BASE_URL+"UserDataResource/sendVerification";
    public static final String KEY_LOGIN_URL = KEY_BASE_URL+"UserDataResource/login";
    public static final String KEY_UPDATE_USER_URL = KEY_BASE_URL+"UserDataResource/updateUserProfile";
    public static final String KEY_FORGOT_PASS_URL = KEY_BASE_URL+"UserDataResource/forgotPassword";
    public static final String KEY_UPDATE_PASS_URL = KEY_BASE_URL+"UserDataResource/resetPassword";
	public static final String KEY_SUBJECT_URL = KEY_BASE_URL+"SubjectDataResource/getSubjects";
    public static final String KEY_COIN_USES_URL = KEY_BASE_URL+"CoinDataResources/getCoinUseHistory";
    public static final String KEY_COIN_PURCHASE_URL = KEY_BASE_URL+"CoinDataResources/getCoinPurchaseHistory";
    public static final String KEY_COIN_PURCHASE_SAVE_URL = KEY_BASE_URL+"CoinDataResources/saveCoinPurchaseHistory";
    public static final String KEY_LEVEL_URL = KEY_BASE_URL+"QuizDataResources/getLevels";
    public static final String KEY_QUIZ_URL = KEY_BASE_URL+"QuizDataResources/getQuestions";
    public static final String KEY_UPLOAD_QUIZ_RESULT = KEY_BASE_URL+"UserDataResource/saveResult";
    public static final String KEY_GET_QUIZ_RESULT = KEY_BASE_URL+"UserDataResource/getResult";
    public static final String KEY_SAVE_FEEDBACK_URL = KEY_BASE_URL+"UserInteractionDataResources/saveFeedback";
    public static final String KEY_GENERATE_TOKEN = KEY_BASE_URL+"SubmitQuizTokenResource/generateToken";
    public static final String KEY_GET_COIN_PASS_URL = KEY_BASE_URL+"UserDataResource/getCoins";// for this URL input data NULL
    public static final String KEY_GET_PAYU_HASH = KEY_BASE_URL+"PayuHashGenResource/genHash";
    public static final String KEY_GET_CONTRIBUTION = KEY_BASE_URL+"ContributionResource/getContributions";
    public static final String KEY_REQUEST_REDEEM = KEY_BASE_URL+"ReedemResource/createRequest";
    public static final String KEY_RESULT = "result";
    public static final String KEY_STATUS = "status";
    public static final String KEY_EXCEPTION = "exception";

    public static final int COIN_PURCHASE_TYPE_REFER = 1;
    public static final int COIN_PURCHASE_TYPE_VIDEO = 0;
    public static final int COIN_PURCHASE_TYPE_MONEY = 2;
    public static final int COIN_PURCHASE_TYPE_SIGN_UP = 3;
    public static final int COIN_PURCHASE_TYPE_QUIZ_SUBMIT = 6;
    public static final int COIN_PURCHASE_TYPE_FEEDBACK = 5;
    public static final int COIN_PURCHASE_TYPE_RATING = 4;

    public static final int ERROR_CODE_SUCCESS = 0;
	public static final int HEADER_PARAM_ERROR = -1;
    public static final int ERROR_CODE_NO_DATA_RECEIVED = -101;
    public static final int ERROR_CODE_OPERATION_FAILED = -102;
    public static final int ERROR_CODE_DB_CREATION_FAILED = -103;
    public static final int ERROR_CODE_DB_DROP_FAILED = -104;
    public static final int ERROR_CODE_NONE_OF_THE_ROWS_AFFECTED = -105;
    public static final int ERROR_CODE_EMPTYLIST = -106;
    public static final int ERROR_CODE_NULL_POINTER = -107;
    public static final int ERROR_CODE_NO_RECORDS_FOUND = -108;
    public static final int ERROR_CODE_DUPLICATE_ENTRY = -109;
    public static final int ERROR_CODE_PASSWORD_MISMATCH = -110;
    public static final int ERROR_USER_VERIFICATION_PENDING = -111;

    public static final int ERROR_USER_VERIFICATION_CODE_EXPIRED = -112;
    public static final int ERROR_USER_VERIFICATION_CODE_MISMATCH = -113;
    public static final int ERROR_USER_VERIFICATION_CODE_INVALID_USER = -114;
    public static final int ERROR_USER_INVALID_EMAIL = -115;
	public static final int EMAIL_VERIFICATION_SENT = -116;
	public static final int ERROR_USER_INVALID_USER_ID = -117;
    public static final int ERROR_USER_INVALID_SESSION = -118;
    public static final String CONTENT_TYPE= "Content-Type";
    public static final String ACCEPT ="Accept";
    public static final String	USER_AGENT="User-Agent";
    public static final String	ACCEPT_ENCODING	= "Accept-Encoding";
    private static final String USER_AGENT_NAME = "TechQuiz Client Android/";
    public static final String COOKIE = "Cookie";

    // Home Screen Category
    public static final int CATEGORY_OTHERS = 0;
    public static final int CATEGORY_LANGUAGE = 1;
    public static final int CATEGORY_DOMAIN_PLATFORM = 2;
    public static String getUserAgent(String appVersion){

        return  USER_AGENT_NAME+appVersion;
    }
}

package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.SubLevelAdapter;
import com.mobilelabops.techquiz.model.Level;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomTextView;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class SubLevelActivity extends BaseActivity implements IServerCallBack {

    private int level_id = 0, sub_id = 0;
    private String subject;
    GridView sublevel_gridview;
    Level mLevel;
    SubLevelAdapter subLevelAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sublevel);

        CustomTextView title = (CustomTextView) findViewById(R.id.toolbar_title);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String LEVEL_ID = "level_id", SUBJECT_ID = "sub_id", SUBJECT_NAME="sub_name";

        level_id = getIntent().getIntExtra(LEVEL_ID, 0) ;
        sub_id = getIntent().getIntExtra(SUBJECT_ID, 0);
        subject = getIntent().getStringExtra(SUBJECT_NAME);
        sublevel_gridview = findViewById(R.id.subLvlGridView);
        title.setText(subject +" " + getResources().getString(R.string.nav_quizes));

        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics());
        DisplayMetrics dm = getResources().getDisplayMetrics();
        sublevel_gridview.setColumnWidth(dm.widthPixels/ 3 -px);

        // Gridview click handler
        sublevel_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Server API : Load Question related to subjects
                //Intent intent = new Intent(TechQMainActivity.this, ActiveQuizSessionActivity.class);

                if(Util.isNetworkConnected(getApplicationContext())) {

                    Intent intent = new Intent(SubLevelActivity.this, QuizActivity.class);

                    intent.putExtra(Quiz.KEY_LEVEL_ID, level_id);
                    intent.putExtra(Quiz.KEY_SUB_ID, sub_id);

                    intent.putExtra(Quiz.KEY_SUB_LEVEL_ID, mLevel.getSubLevels()[position]);

                    startActivity(intent);
                }else{

                    Util.showNoInternetDialog(getApplicationContext());
                }

               //getQuestionDetails(position);

            }
        });

        getSubLevelDetails(level_id);

    }

    private void getSubLevelDetails(int level){

        Log.i("ServerResponse", "getLevelDetails");

        JSONObject params = new JSONObject();
        try {

            params.put(Level.KEY_SUB_ID,sub_id);
            params.put(Level.KEY_LEVEL_ID,level);

            ServerRequest request = new ServerRequest(Constant.KEY_LEVEL_URL,params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }
        }
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    try {
                        if (jsonObject.has("result")) {

                            String resultStr = jsonObject.getString("result");
                            JSONObject jObjLvl = new JSONObject(resultStr);

                            mLevel = Level.createObjectFromJson(jObjLvl);

                            /*if (jObjLvl.has(Level.KEY_SUB_LEVELS)){
                                int len = jObjLvl.getInt(Level.KEY_SUB_LEVELS);

                                for (int i=0; i< len; i++){
                                    levelsArrayList.add(new Level(i,len,sub_id,1,"Level Description"));
                                }
                            }*/

                            subLevelAdapter = new SubLevelAdapter(this, mLevel);
                            sublevel_gridview.setAdapter(subLevelAdapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                 //   subLevelAdapter.notifyDataSetInvalidated();

                    break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_NO_RECORDS_FOUND",Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this,"ERROR_CODE_PASSWORD_MISMATCH",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this,"ERROR_USER_VERIFICATION_PENDING",Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        closeProgressDialog();
    }

    @Override
    public void onServerReqError(Exception e) {
        closeProgressDialog();
    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog("Fetching Levels...");
    }
}

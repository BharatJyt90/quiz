package com.mobilelabops.techquiz.loginhelper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.mobilelabops.techquiz.activity.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by bhazarix on 9/11/2017.
 */

public class FbLoginHelper {

    CallbackManager callbackManager;
    private static FbLoginHelper mInstance;
    private ILoginHelper mLoginHelper;

    public void setLoginHelper(ILoginHelper helper){

        mLoginHelper = helper;
    }

    public static FbLoginHelper getInstance(){

        if(mInstance == null)
            mInstance = new FbLoginHelper();

        return mInstance;
    }

    private FbLoginHelper(){

    }

    /*
   Initialize the facebook sdk.
   And then callback manager will handle the login responses.
   */
    public void facebookSDKInitialize(Context context) {

        FacebookSdk.sdkInitialize(context);
        callbackManager = CallbackManager.Factory.create();
    }

    public void getLoginDetails(LoginButton loginButton) {

        loginButton.setReadPermissions("email");

        List<String> permissionNeeds = Arrays.asList("user_photos", "email",
                "user_birthday", "public_profile", "AccessToken");
        loginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        System.out.println("onSuccess");

                        String accessToken = loginResult.getAccessToken()
                                .getToken();
                        Log.i("accessToken", accessToken);

                        updateWithToken(loginResult.getAccessToken());

                    }

                    @Override
                    public void onCancel() {
                        System.out.println("onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("onError");
                        Log.v("LoginActivity", exception.toString());
                    }
                });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void updateWithToken(AccessToken currentAccessToken) {

        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object,
                                            GraphResponse response) {

                        Log.i("LoginActivity",
                                response.toString());
                        try {
                            String id = object.getString("id");

                            String profile_pic = new String(
                                    "http://graph.facebook.com/" + id + "/picture?type=large");
                            Log.i("profile_pic",
                                    profile_pic + "");

                            String name = object.getString("name");
                            String email = object.getString("email");
                        //    String dob = object.getString("user_birthday");

                            if(mLoginHelper != null){

                                mLoginHelper.onLoginSuccess(email,name,profile_pic,null, LoginActivity.LOGIN_TYPE_FB);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields",
                "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public boolean checkFbLogin(){

        AccessToken crntToken = AccessToken.getCurrentAccessToken();

        if(crntToken != null){

            // logged in fb
            updateWithToken(crntToken);

            return true;
        }

        return false;
    }
}

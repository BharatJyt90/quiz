package com.mobilelabops.techquiz.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Feedback;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.net.UploadImageToServer;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class UserFeedbackActivity extends BaseActivity implements IServerCallBack {

    private File mAttachmentFile;
    private ProgressDialog mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_feedback);

        setTitle(getString(R.string.feedback));

        checkReadWritePermission();
    }

    private void checkReadWritePermission() {

        int status = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (status != PackageManager.PERMISSION_GRANTED) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay!

        }
    }

    public void chooseAttachment(View v){

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);

            if (cursor == null || cursor.getCount() < 1) {
                return; // no cursor or no record. DO YOUR ERROR HANDLING
            }

            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            if(columnIndex < 0) // no column index
                return; // DO YOUR ERROR HANDLING

            String picturePath = cursor.getString(columnIndex);

            cursor.close(); // close cursor

            mAttachmentFile = new File(picturePath);

            CustomEditText attachmentTxt = (CustomEditText) findViewById(R.id.attachmentTxt);
            attachmentTxt.setText(mAttachmentFile.getName());
        }
    }

    public void sendFeedback(View v){


        CustomEditText feedbackTxt = (CustomEditText) findViewById(R.id.feedbackTxt);

        if(feedbackTxt.getText().toString().isEmpty()){

            feedbackTxt.setError(getString(R.string.error_enter_txt));
            return;
        }

        mProgressBar = new ProgressDialog(this);
        mProgressBar.setMessage("Uploading...");
        mProgressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setCancelable(false);
        mProgressBar.show();

        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        String userId = preferences.getString(User.KEY_USER_ID,"");

        RadioGroup feedbackType = (RadioGroup) findViewById(R.id.feedbackType);

        final Feedback feedback = new Feedback();

        int radioButtonID = feedbackType.getCheckedRadioButtonId();
        View radioButton = feedbackType.findViewById(radioButtonID);
        int index = feedbackType.indexOfChild(radioButton);

        feedback.setFeedbackType(index);
        feedback.setUserId(userId);
        feedback.setFeedback(feedbackTxt.getText().toString());

        if(mAttachmentFile != null) {

            final String fileName = "Attachment_" + userId + "_" + mAttachmentFile.getName();

            UploadImageToServer upload = new UploadImageToServer(this, new UploadImageToServer.IUploadImageCallBack() {

                @Override
                public void onUploadStatus(ServerResponse response) {

                    if (response != null){

                        if (response.getResponseCode() == 200) {

                            String responseTxt = response.getResponseString();
                            String[] splittedArr = responseTxt.split("/");

                            String fileName = splittedArr[splittedArr.length - 1];

                            feedback.setImagePath(fileName);
                        }

                    }
                    uploadFeedBack(feedback);
                }
            });

            upload.execute(mAttachmentFile.getPath().toString(), fileName);

        }else {

            uploadFeedBack(feedback);
        }

    }

    private void uploadFeedBack(Feedback feedback){

        try {

            JSONObject json = feedback.getJsonObj();

            ServerRequest request = new ServerRequest(Constant.KEY_SAVE_FEEDBACK_URL,json.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(mProgressBar != null)
            mProgressBar.dismiss();

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() > 0){

                Toast.makeText(this,"Success: "+result.getResponseCode(),Toast.LENGTH_SHORT).show();

                return;
            }

            Toast.makeText(this,"Error in upload: "+result.getResponseCode(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

    }
}

package com.mobilelabops.techquiz.net;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by bhazarix on 8/29/2017.
 */

public class HttpsRequestBuilder {

    public static String TAG = HttpsRequestBuilder.class.getSimpleName();
    /**
     * Opens a TLS connection for the URL given.
     * @param url - HTTPS URL
     * @param context Application context
     * @return HttpsURLConnection on success
     * @throws IOException
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    public static HttpsURLConnection getHttpsUrlConnection(String url, Context context, int certRes) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, KeyManagementException {

        InputStream caInput = new BufferedInputStream(context.getResources().openRawResource(certRes));
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        Certificate ca = cf.generateCertificate(caInput);
        System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        // Create an SSLContext that uses our TrustManager
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, tmf.getTrustManagers(), null);

        URL urlObj = new URL(url);
        // Tell the URLConnection to use a SocketFactory from our SSLContext
        HttpsURLConnection urlConnection = (HttpsURLConnection) urlObj.openConnection();
        urlConnection.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return HttpsURLConnection.getDefaultHostnameVerifier().verify("NiagaraAX", session);
            }
        });

        urlConnection.setSSLSocketFactory(new NoSSLv3Factory(sslContext.getSocketFactory()));
        return urlConnection;
    }

    public static ServerResponse doHttpPost(HashMap<String, String> headerParams, String url, String bodyContent)throws IOException{

        URL urlObj = new URL(url);

        HttpURLConnection httpURLConnection = (HttpURLConnection) urlObj.openConnection();

        httpURLConnection.setRequestMethod("POST");

        if(headerParams != null) {

            for (Map.Entry<String, String> entry : headerParams.entrySet()) {

                httpURLConnection.setRequestProperty(entry.getKey(), entry.getValue());
            }

        }

        httpURLConnection.connect();

        OutputStream os = httpURLConnection.getOutputStream();

        if(bodyContent != null)
            os.write(bodyContent.getBytes());

//        Log.i(TAG," POST req data : "+bodyContent);

        BufferedReader br =
                new BufferedReader(
                        new InputStreamReader(httpURLConnection.getInputStream()));  //read response from gateway
        StringBuilder response = null;

        String line;
        while ((line = br.readLine()) != null) {


            if (response == null)
                response = new StringBuilder();

            response.append(line);

        }

        ServerResponse serverResponse = new ServerResponse();

        if(response != null)
            serverResponse.setResponseString(response.toString());

        Map<String,List<String>> headers = httpURLConnection.getHeaderFields();

        serverResponse.setHeaderFields(httpURLConnection.getHeaderFields());

        serverResponse.setResponseCode(httpURLConnection.getResponseCode());

        os.close();
        br.close();
        httpURLConnection.disconnect();

        return serverResponse;

    }

    public static ServerResponse doHttpsPost(HashMap<String, String> headerParams, String url, Context context, String bodyContent,int certRes)throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, KeyManagementException{

        HttpsURLConnection httpsURLConnection = getHttpsUrlConnection(url,context,certRes);
        httpsURLConnection.setRequestMethod("POST");

        for (Map.Entry<String,String> entry : headerParams.entrySet()) {

            httpsURLConnection.setRequestProperty(entry.getKey(),entry.getValue());
        }

        httpsURLConnection.connect();

        OutputStream os = httpsURLConnection.getOutputStream();

        os.write(bodyContent.getBytes());

        Log.i(TAG," POST req data : "+bodyContent);

        BufferedReader br =
                new BufferedReader(
                        new InputStreamReader(httpsURLConnection.getInputStream()));  //read response from gateway
        StringBuilder response = null;

        String line;
        while ((line = br.readLine()) != null) {


            if (response == null)
                response = new StringBuilder();

            response.append(line);

        }

        ServerResponse serverResponse = new ServerResponse();

        serverResponse.setResponseString(response.toString());

        Map<String,List<String>> headers = httpsURLConnection.getHeaderFields();

        serverResponse.setHeaderFields(httpsURLConnection.getHeaderFields());

        os.close();
        br.close();
        httpsURLConnection.disconnect();

        return serverResponse;

    }
}

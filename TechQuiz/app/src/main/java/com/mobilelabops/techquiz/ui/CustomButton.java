package com.mobilelabops.techquiz.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.mobilelabops.techquiz.R;

/**
 * Created by bhazarix on 9/2/2017.
 */

public class CustomButton extends AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        init(null,context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs,context);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs,context);
    }

    public void setTypeFace(String fontName,Context context){

        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        setTypeface(myTypeface);
    }

    private void init(AttributeSet attrs,Context context) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            String fontName = a.getString(R.styleable.CustomTextView_fontName);

            try {
                if (fontName != null) {
                    Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
                    setTypeface(myTypeface);
                }
            } catch ( Exception e) {
                e.printStackTrace();
            }

            a.recycle();
        }
    }
}

package com.mobilelabops.techquiz.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.database.UserDbAdapter;
import com.mobilelabops.techquiz.loginhelper.FbLoginHelper;
import com.mobilelabops.techquiz.loginhelper.GoogleLoginHelper;
import com.mobilelabops.techquiz.loginhelper.ILoginHelper;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.ui.CustomTextView;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.CryptoUtils;
import com.mobilelabops.techquiz.util.QuizConfig;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;

/**
 * Created by bhazarix on 9/1/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,IServerCallBack,ILoginHelper{

    public static final int MANUAL_LOGIN = 0,LOGIN_TYPE_GOOGLE = 1, LOGIN_TYPE_FB = 2;
    private LoginButton loginButton;
    private GoogleLoginHelper googleLoginHelper;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.mobilelabops.techquiz", PackageManager.GET_SIGNATURES);
            for (Signature signature: info.signatures)  {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("FACEBOOK APP SIGNATURE", key );
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        CustomTextView signUpTxt = (CustomTextView) findViewById(R.id.register);
        signUpTxt.setOnClickListener(this);

        CustomTextView forgotPassTxt = (CustomTextView) findViewById(R.id.forgot_pass);
        forgotPassTxt.setOnClickListener(this);

        loginButton = (LoginButton) findViewById(R.id.login_button);

        FbLoginHelper fbLoginHelper = FbLoginHelper.getInstance();
        fbLoginHelper.facebookSDKInitialize(getApplicationContext());
        fbLoginHelper.setLoginHelper(this);
        fbLoginHelper.getLoginDetails(loginButton);

        Button signInButton = (Button) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.register:

                Intent regIntent = new Intent(this,RegistrationActivity.class);
                startActivity(regIntent);

                break;

            case R.id.forgot_pass:

                Intent passRecoveryIntent = new Intent(this,PasswordRecoveryActivity.class);
                CustomEditText emailEditText = (CustomEditText) findViewById(R.id.emailId);

                String emailId = (emailEditText).getText().toString();
                passRecoveryIntent.putExtra(User.KEY_EMAIL_ID,emailId);

                startActivity(passRecoveryIntent);

                break;

            case R.id.continueBtn:

//                startActivity(new Intent(this,MainActivity.class));

                doSignIn();

                break;

            case R.id.sign_in_button:

                goggleSignIn();

                break;
            case R.id.fb:

                loginButton.performClick();

                break;
        }

    }

    private void goggleSignIn(){

        googleLoginHelper = GoogleLoginHelper.getInstance();
        googleLoginHelper.init(this);
        googleLoginHelper.setLoginHelper(this);
        googleLoginHelper.fireSignInIntent(this);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GoogleLoginHelper.RC_GOOGLE_SIGN_IN) {
            googleLoginHelper.onActivityResult(requestCode,resultCode,data);
        }
        else {

            FbLoginHelper fbLoginHelper = FbLoginHelper.getInstance();

            fbLoginHelper.onActivityResult(requestCode, resultCode, data);

        }
        // Log.e("data",data.toString());
    }

    private void doSignIn() {

        CustomEditText emailEditText = (CustomEditText) findViewById(R.id.emailId);

        String emailId = (emailEditText).getText().toString();

        if(Util.isValidEmail(emailId) == false){

            emailEditText.setError(getString(R.string.email_error));

            return;
        }

        CustomEditText passEditText = (CustomEditText) findViewById(R.id.password);

        String password = passEditText.getText().toString();

        if(password.length() < 6){

            passEditText.setError(getString(R.string.password_error));
            return;
        }

        JSONObject params = new JSONObject();
        try {

            params.put(User.KEY_EMAIL_ID,emailId);

            params.put(User.KEY_PASSWORD, CryptoUtils.applyDoubleEncryption(emailId,password));

            params.put(User.KEY_REG_TYPE, MANUAL_LOGIN);

            ServerRequest request = new ServerRequest(Constant.KEY_LOGIN_URL,params.toString(),Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

            QuizConfig.setString(QuizConfig.CFG_PARAM_S_PASSWORD, Base64.encodeToString(password.getBytes(),Base64.DEFAULT), true); ;

            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).edit();
            editor.putString(User.KEY_PASSWORD, Base64.encodeToString(password.getBytes(),Base64.DEFAULT));

            editor.commit();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }

        mProgressDialog.dismiss();

    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    User user = User.createObjectFromJson(jsonObject);

                    Intent mainActIntent = new Intent(this,MainActivity.class);
                    mainActIntent.putExtra(User.class.getSimpleName(),user);

                    startActivity(mainActIntent);

                    SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                    editor.putInt(User.KEY_REG_TYPE,user.getRegType());
                    editor.putString(User.KEY_EMAIL_ID,user.getEmailId());
                    editor.putString(User.KEY_USER_ID,user.getUserId());
                    editor.commit();

                    QuizConfig.setInt(QuizConfig.CFG_PARAM_I_USER_REG_TYPE,user.getRegType(),true);
                    QuizConfig.setString(QuizConfig.CFG_PARAM_S_USER_EMAIL, user.getEmailId(), true);
                    QuizConfig.setString(QuizConfig.CFG_PARAM_S_USER_ID, user.getUserId(), true);

                    UserDbAdapter.open(this);
                    UserDbAdapter userDB = new UserDbAdapter();
                    userDB.insertData(user);


                    finish();

                    break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_NO_RECORDS_FOUND",Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this,"ERROR_CODE_PASSWORD_MISMATCH",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this,"ERROR_USER_VERIFICATION_PENDING",Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServerReqError(Exception e) {

        mProgressDialog.dismiss();
    }

    @Override
    public void onServerReqStarted() {

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMessage("Login in...");
        mProgressDialog.show();
    }

    private void doSignIn(String emailId, String name,int regType) {

        JSONObject params = new JSONObject();
        try {

            params.put(User.KEY_EMAIL_ID,emailId);

            params.put(User.KEY_NAME,name);

            params.put(User.KEY_REG_TYPE, regType);

            ServerRequest request = new ServerRequest(Constant.KEY_LOGIN_URL,params.toString(),Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLoginSuccess(String email, String name, String profilePic, String dob, int loginType) {

        // fire login

//        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).edit();
//        editor.putString(User.KEY_PROFILE_PIC,profilePic);
//        editor.commit();

        QuizConfig.setString(QuizConfig.CFG_PARAM_S_USER_PIC, profilePic, true);

        doSignIn(email,name,loginType);
    }
}

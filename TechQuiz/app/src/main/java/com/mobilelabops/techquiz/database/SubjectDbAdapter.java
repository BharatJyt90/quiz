/**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.database;

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.model.Subject;


public class SubjectDbAdapter extends AbstractSqliteDbAdapter<Subject> {

    public SubjectDbAdapter() {

        super(Subject.DATABASE_TABLE_NAME);

    }

    @Override
    String[] getColumnNames() {

        return Subject.COLUMNS;

    }


    @Override
    protected ContentValues getContentValues(Subject object) {

        return object.getContentValues();
    }

    @Override
    Subject createObjectFromCursor(Cursor cursor) {
        return Subject.createObjectFromCursor(cursor);
    }

}

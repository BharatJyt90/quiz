/**
Auto Generated using DbCodeGenerator..
Please use appropriate import statement and modify as required.
**/
package com.mobilelabops.techquiz.database;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mobilelabops.techquiz.model.Level;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.Result;
import com.mobilelabops.techquiz.model.Subject;
import com.mobilelabops.techquiz.model.User;


public class DbTableManager {


    public static final String DATABASE_NAME = "tech_quiz.db";
    public static final int DATABASE_VERSION = DbConstants.DB_VERSION_BASE;
	private static final String tag = DbTableManager.class.getSimpleName();


	public void alterTables(SQLiteDatabase db) throws SQLException{
		
		//TODO use your alter statement as required.
	
	}

	public void dropTables( SQLiteDatabase db ) throws SQLException
	{
        //TODO use your delete statement as required.

	}
	
	public void createTables( SQLiteDatabase db ) throws SQLException
	{


		/*db.execSQL(CoinUseHistory.getCrateTableStatement() );
		Log.i( tag, CoinUseHistory.DATABASE_TABLE_NAME + " table creation completed");

		db.execSQL(LeaderboardQuizSubmit.getCrateTableStatement() );
		Log.i( tag, LeaderboardQuizSubmit.DATABASE_TABLE_NAME + " table creation completed");

		db.execSQL(Level.getCrateTableStatement() );
		Log.i( tag, Level.DATABASE_TABLE_NAME + " table creation completed");*/

		db.execSQL(PurchaseHistory.getCrateTableStatement() );
		Log.i( tag, PurchaseHistory.DATABASE_TABLE_NAME + " table creation completed");

		/*db.execSQL(Quiz.getCrateTableStatement() );
		Log.i( tag, Quiz.DATABASE_TABLE_NAME + " table creation completed");

		db.execSQL(Result.getCrateTableStatement() );
		Log.i( tag, Result.DATABASE_TABLE_NAME + " table creation completed");*/

		db.execSQL(Subject.getCreateTableStatement() );
		Log.i( tag, Subject.DATABASE_TABLE_NAME + " table creation completed");

		db.execSQL(User.getCreateTableStatement() );
		Log.i( tag, User.DATABASE_TABLE_NAME + " table creation completed");

	}
}
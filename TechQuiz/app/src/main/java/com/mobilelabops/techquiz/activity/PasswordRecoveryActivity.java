package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.TempRegTable;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class PasswordRecoveryActivity extends AppCompatActivity implements IServerCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);

        CustomEditText emailEditText = (CustomEditText) findViewById(R.id.emailId);
        emailEditText.setText(getIntent().getStringExtra(User.KEY_EMAIL_ID));
    }

    private void sendRecoveryRequset(){

        CustomEditText emailEditText = (CustomEditText) findViewById(R.id.emailId);

        String emailId = (emailEditText).getText().toString();

        if(Util.isValidEmail(emailId) == false){

            emailEditText.setError(getString(R.string.email_error));

            return;
        }


        JSONObject params = new JSONObject();
        try {

            params.put(User.KEY_EMAIL_ID,emailId);

            ServerRequest request = new ServerRequest(Constant.KEY_FORGOT_PASS_URL,params.toString(),Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void onClick(View v){

        switch (v.getId()){

            case R.id.reqBtn:

                sendRecoveryRequset();

                break;
        }
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }
    }

    @Override
    public void onServerReqError(Exception e) {


    }

    @Override
    public void onServerReqStarted() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RegistrationActivity.ACTIVITY_RESULT_CODE_SUCCESS){

            finish();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    String verificationCode = jsonObject.getString(Constant.KEY_RESULT);

                    Intent forgotPassIntent = new Intent(this,EmailVerificationActivity.class);

                    CustomEditText emailEditText = (CustomEditText) findViewById(R.id.emailId);

                    String emailId = (emailEditText).getText().toString();

                    forgotPassIntent.putExtra(TempRegTable.KEY_VERFICATION_CODE,verificationCode);
                    forgotPassIntent.putExtra(TempRegTable.KEY_EMAIL_ID,emailId);

                    startActivityForResult(forgotPassIntent,0);

                    break;


                case Constant.ERROR_USER_INVALID_EMAIL:

                    Toast.makeText(this,"ERROR_USER_INVALID_EMAIL",Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

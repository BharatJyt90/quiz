package com.mobilelabops.techquiz.database;


public class DbConstants {

	public static final int ERROR_CODE_NULL_POINTER = -3;
	public static final int ERROR_CODE_SUCCESS = 0;
	public static final int ERROR_CODE_OPERATION_FAILED = -1;
	public static final int ERROR_CODE_INVALID_WHERE_CONDITION = -2;
	
	public static final String DATA_TYPE_INTEGER = "INTEGER";
	public static final String DATA_TYPE_REAL = "REAL";
	public static final String DATA_TYPE_NUMERIC = "NUMERIC";
	public static final String DATA_TYPE_TEXT = "TEXT";
	public static final String DATA_TYPE_NONE = "NONE";
	public static final String PRIMARY_KEY = "PRIMARY KEY";
	public static final String DB_FUNCTION_MAX = "max";
	public static final String DB_FUNCTION_MIN = "min";
	
	public static final int DB_VERSION_BASE = 1;
	public static final int DB_VERSION_UPDATED = 2;
	public static final int DB_VERSION_ALTERED = 3;
}

package com.mobilelabops.techquiz.loginhelper;

/**
 * Created by bhazarix on 9/11/2017.
 */

public interface ILoginHelper {

    public void onLoginSuccess(String email, String name,String profilePic, String dob,int loginType);
}

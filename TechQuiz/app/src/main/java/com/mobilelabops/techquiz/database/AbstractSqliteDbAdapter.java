package com.mobilelabops.techquiz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
/**
 * Abstract Base class for all table adapters
 * @author dbCodeGen
 * @param <T> Generic model class
 */
public abstract class AbstractSqliteDbAdapter<T> {

	private static MyDbOpenHelper mDbOpenHelper = null;
	private static SQLiteDatabase mSqliteDb = null;
	private String mTableName;
	private static final String tag = AbstractSqliteDbAdapter.class.getSimpleName();

    /**
     * Abstract method to be implemented by dbAdapter classes. Creates {@link ContentValues} for update and insert operation.
     * @param object The generic model object
     * @return ContentValues object of the Model
     */
	abstract protected ContentValues getContentValues(T object);

    /**
     * Abstract method to be implemented by dbAdapter classes. Create the model object from the db cursor
     * @param cursor db cursor from which model object need to be created.
     * @return model object
     */
	abstract T createObjectFromCursor(Cursor cursor);

    /**
     * Abstract method to be implemented by dbAdapter classes to get the all the columns of the table.
     * @return return array of column names
     */
	abstract String[] getColumnNames();

	/**
	 * Method to query for a single row from the table
	 * @param whereStr The condition for the table to fetch the row. Can not be NULL.
	 * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort order, which may be unordered.
	 * @return Model object of the table
	 */
	public T getData(String whereStr, String orderBy) {
		Cursor cur = query(mTableName, getColumnNames(), whereStr,orderBy,null);

		if (cur == null)
			return null;

		if( cur != null && cur.moveToFirst() ) {

			return createObjectFromCursor(cur);
		}
		if( cur != null )
			cur.close();
		return null;
	}

    /**
     * Method to query the the table for multiple rows based on condition.
     * @param whereStr Optional query string for the table to return rows. For e.g. where key = 0. Can be NULL to fetch the entire table.
     * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort order, which may be unordered.
     * @return List rows if available.
     */
	public ArrayList<T> getList(String whereStr, String orderBy) {
		Cursor cur = query(mTableName, getColumnNames(), whereStr,orderBy,null);

		if (cur == null)
			return null;

		if( cur != null && cur.moveToFirst() ) {
			ArrayList<T> list = new ArrayList<T>();
			do {

				list.add(createObjectFromCursor(cur));

			} while (cur.moveToNext());
			cur.close();
			return list;
		}
		if( cur != null )
			cur.close();
		return null;
	}

	private static class MyDbOpenHelper extends SQLiteOpenHelper {

		public MyDbOpenHelper(Context context) {
			super(context, DbTableManager.DATABASE_NAME, null, DbTableManager.DATABASE_VERSION);
		}

		@Override
		public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			onUpgrade(db,oldVersion,newVersion);

		};

		@Override
		public void onCreate(SQLiteDatabase db) {

			Log.i( tag, "DB creation started. Version: " + DbTableManager.DATABASE_VERSION);

			try {
				DbTableManager tableManager = new DbTableManager();
				tableManager.createTables(db);
				Log.i( tag, "DB creation successful" );
			}
			catch (SQLException e){

				e.printStackTrace();
				Log.e( tag, "DB creation failed "+e.getMessage() );
			}

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			Log.i( tag, "DB upgrade from version " + oldVersion + " to "
					+ newVersion );

			DbTableManager tableManager = new DbTableManager();

			switch (newVersion) {

			case DbConstants.DB_VERSION_ALTERED:

				tableManager.alterTables(db);

				break;

			case DbConstants.DB_VERSION_UPDATED:

				tableManager.dropTables(db);

				break;

			default:
				break;
			}

		}
	}

	public AbstractSqliteDbAdapter(String tableName) {
		this.mTableName = tableName;
	}

    /**
     *Create the DB connection. It is advisable to call this method before any db operation.
	 * @param context Application context
     * @throws SQLException
     */
	public static void open(Context context) throws SQLException {

		if( mDbOpenHelper == null )
			mDbOpenHelper = new MyDbOpenHelper(context);

		if( mSqliteDb == null || mSqliteDb.isOpen() == false )
			mSqliteDb = mDbOpenHelper.getWritableDatabase();

	}

    /**
     * Update a table row.
     * @param data the updated model object that need to be set in db table
     * @param whereClause he update condition for the table. For e.g. key = 1 where id = 0
     * @return Error code of the operation. returns {@link DbConstants#ERROR_CODE_SUCCESS} on success
     */
	public long updateData(T data, String whereClause){

		if(whereClause == null)
			return DbConstants.ERROR_CODE_INVALID_WHERE_CONDITION;

		int ret = DbConstants.ERROR_CODE_OPERATION_FAILED;

		ContentValues contentValues = getContentValues(data);

        SQLiteStatement insertStmt;

        if(contentValues != null){

			String tableName = mTableName;

			if(tableName != null)
			{
				mSqliteDb.update( tableName, contentValues,whereClause,null );

				Log.i( tag,  tableName + " Update successful" );
				ret = DbConstants.ERROR_CODE_SUCCESS;
			}
			else{

				ret = DbConstants.ERROR_CODE_NULL_POINTER;
				Log.e( tag, "Update failed...table name null" );
			}

		}
		else{

			ret = DbConstants.ERROR_CODE_NULL_POINTER;
			Log.e( tag, "Update failed...contentValues null" );
		}

		return ret;
	}

    /**
     *Insert data object into the table
     * @param data Data object to insert
     * @return Error code of the operation. Returns {@link DbConstants#ERROR_CODE_SUCCESS} on success.
     */
	public long insertData(T data)
	{

		long ret = DbConstants.ERROR_CODE_OPERATION_FAILED;

		if(data == null)
			return ret;

		ContentValues contentValues = getContentValues(data);

		if(contentValues != null){

			String tableName = mTableName;

			if(tableName != null)
			{
				ret = mSqliteDb.insert( tableName, null, contentValues );

				Log.i( tag,  tableName + " insert successful" );

			}
			else{

				ret = DbConstants.ERROR_CODE_NULL_POINTER;
				Log.e( tag, "Insert failed...table name null" );
			}

		}
		else{

			ret = DbConstants.ERROR_CODE_NULL_POINTER;
			Log.e( tag, "Insert failed...contentValues null" );
		}

		return ret;
	}

    /**
     * Query the given table, returning a Cursor over the result set
     * @param tableName The table name to compile the query against.
     * @param columns A list of which columns to return. Passing null will return all columns, which is discouraged to prevent reading data from storage that isn't going to be used.
     * @param whereString A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given table.
     * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause (excluding the ORDER BY itself). Passing null will use the default sort order, which may be unordered.
     * @param limit Limits the number of rows returned by the query, formatted as LIMIT clause. Passing null denotes no LIMIT clause.
     * @return A Cursor object, which is positioned before the first entry.
     */
	public Cursor query(String tableName, String[] columns, String whereString, String orderBy, String limit){

		try {
			return mSqliteDb.query(true, tableName, columns,
					whereString, null, null, null, orderBy, limit);
		} catch (Exception e) {

			Log.e( tag,tableName+" query failed" );
			Log.e(tag,e.getLocalizedMessage());

			return null;
		}
	}

    /**
     * Deletes a row form the table based on the condition provided.
     * @param aWhereStr The condition for the table to delete row(s). Must not be NULL.
     * @return Error code of the operation. Returns {@link DbConstants#ERROR_CODE_SUCCESS} on success.
     */
	public long delete(String aWhereStr) {

		int ret = DbConstants.ERROR_CODE_OPERATION_FAILED;

		String tableName = mTableName;

		try {

			if (tableName == null)
				return ret;

			int noOfRowsDeleted = mSqliteDb.delete(tableName, aWhereStr, null);

			if (noOfRowsDeleted > 0) {

				Log.i(tag, tableName + "  row(s) deleted successfully");
				ret = DbConstants.ERROR_CODE_SUCCESS;

				return (long) noOfRowsDeleted;
			}

		} catch (Exception e) {
			Log.e(tag, tableName + " delete failed");
			Log.e(tag, e.getLocalizedMessage());
		}

		return ret;
	}

    /**
     * Closes the DB connection. It is advisable to call this method after finishing all db operations.
     */
	public static void close() {

		if (mSqliteDb != null) {
			mSqliteDb.close();
			mSqliteDb = null;
		}
		
		if (mDbOpenHelper != null) {
			mDbOpenHelper.close();
			mDbOpenHelper = null;
		}		
	}	
}
package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.ContributionAdapter;
import com.mobilelabops.techquiz.model.Contribution;
import com.mobilelabops.techquiz.model.LeaderboardQuizSubmit;
import com.mobilelabops.techquiz.model.RedeemablePoints;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ContributionActivity extends BaseActivity implements IServerCallBack {

    ArrayList<Contribution> sContriList = new ArrayList<Contribution>();
    ContributionAdapter contributionAdapter;
    RedeemablePoints redeemablePoints;
    double equiv_amnt = 0.0;
    int minValue = 0;
    String currency = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contribution);
        setTitle(getString(R.string.dash_contrib));

        final ListView listview = (ListView) findViewById(R.id.contribution_list);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        String user_id = preferences.getString(User.KEY_USER_ID,null);

        //JSONObject params = new JSONObject();
        try {

            //params.put(User.KEY_USER_ID,user_id);

            ServerRequest request = new ServerRequest(Constant.KEY_GET_CONTRIBUTION,"",Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }

        contributionAdapter = new ContributionAdapter(this, sContriList);
        listview.setAdapter(contributionAdapter);

    }

    public void onRedeemClicked(View v) {
        // Todo : Min redeemable points
        Intent userIntent = new Intent(this,ReedemablePointsActivity.class);
        userIntent.putExtra(RedeemablePoints.KEY_TOTAL_AVAILABLE, redeemablePoints.getTotalEarned());
        userIntent.putExtra("equiv_amnt", equiv_amnt);
        startActivity(userIntent);
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }

    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status) {

                case Constant.ERROR_CODE_SUCCESS:

                        String resultStr = jsonObject.getString(Constant.KEY_RESULT);
                        JSONObject names = new JSONObject(resultStr);

                        //RedeemablePoints redeemablePoints;
                        JSONObject redeemableJson = names.getJSONObject("redeemable_points");
                        redeemablePoints = RedeemablePoints.createObjectFromJson(redeemableJson);

                        Contribution contribElement;
                        JSONArray jsonarray = names.getJSONArray("contribution");
                        int iTotalContribution = 0, iTotalViewed = 0;

                        for (int i = 0; i < jsonarray.length(); i++) {
                            contribElement = Contribution.createObjectFromJson(jsonarray.getJSONObject(i));
                            iTotalViewed += contribElement.getViews();
                            sContriList.add(contribElement);
                        }

                        LeaderboardQuizSubmit contributionPoints;
                        JSONObject contribJson = names.getJSONObject("leaderboard_quiz_submit");
                        contributionPoints = LeaderboardQuizSubmit.createObjectFromJson(contribJson);

                        equiv_amnt = names.getDouble("equiv_amnt");
                        minValue = names.getInt("min_poins");
                        currency = names.getString("currency");

                        updateUI(redeemablePoints, contributionPoints.getQuizCount(), iTotalViewed);

                    contributionAdapter.notifyDataSetInvalidated();
                    break;
                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this, exception, Toast.LENGTH_SHORT).show();

                    break;
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void updateUI(RedeemablePoints redeemablePoints, int totalContribution, int totalViews){
        TextView tv = findViewById(R.id.quizRedeemed);
        tv.setText(""+redeemablePoints.getTotalRedeemed());

        tv = findViewById(R.id.quizEarning);
        tv.setText(""+redeemablePoints.getTotalEarned());

        tv = findViewById(R.id.quizContribution);
        tv.setText(""+totalContribution + " Questions");

        tv = findViewById(R.id.quizViewed);
        tv.setText(""+totalViews);
    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {
    }

        @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

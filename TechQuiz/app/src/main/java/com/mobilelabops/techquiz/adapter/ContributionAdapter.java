package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.database.QuizLocalDB;
import com.mobilelabops.techquiz.model.Contribution;
import com.mobilelabops.techquiz.model.Subject;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 18/11/17.
 */

public class ContributionAdapter extends BaseAdapter {

    ArrayList<Contribution> sArrayList = new ArrayList<Contribution>();
    private LayoutInflater mInflater;
    private ArrayList<Subject> quizArrayList = new ArrayList<Subject>();

    public ContributionAdapter(Context context, ArrayList<Contribution> notifications){
        sArrayList = notifications;
        mInflater = LayoutInflater.from(context);
        quizArrayList = QuizLocalDB.getAllSubjects();
    }

    @Override
    public int getCount() {
        if (sArrayList == null)
            return 0;
        return sArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return sArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public TextView subject;
        public TextView level;
        public TextView coin_credit;
        public TextView contrib_date;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = view;
        // reuse views
        if (rowView == null) {
            rowView = mInflater.inflate(R.layout.contribution_list_element, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.subject = (TextView) rowView.findViewById(R.id.subject_name);
            viewHolder.level = (TextView) rowView.findViewById(R.id.subject_level);
            viewHolder.coin_credit = (TextView) rowView.findViewById(R.id.coin_credited);
            viewHolder.contrib_date = (TextView) rowView.findViewById(R.id.coin_credited_date);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.subject.setText(quizArrayList.get(sArrayList.get(i).getSubId()).getName());
        //String coin_credited = Integer.toString(sArrayList.get(i).getCoinsCredit());
        holder.level.setText(getLevelName(sArrayList.get(i).getLevelId()));
        holder.contrib_date.setText("10Dec 2017");

        return rowView;
    }

    private String getLevelName(int level_id){
        String level="";
        switch(level_id){
            case 0: level= "Beginners";break;
            case 1: level= "Intermediate";break;
            case 2: level= "Advanced";break;
        }
        return level;
    }


}

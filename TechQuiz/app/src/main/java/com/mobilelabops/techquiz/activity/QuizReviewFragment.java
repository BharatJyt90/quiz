package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.QuizAnswer;
import com.mobilelabops.techquiz.ui.CustomRadioButton;
import com.mobilelabops.techquiz.ui.CustomTextView;

import io.github.kbiakov.codeview.CodeView;

/**
 * Created by Bharat on 2/1/2018.
 */

public class QuizReviewFragment extends Fragment {

    private Quiz mQuiz;
    private QuizAnswer mOptedAnswer;

    public  QuizReviewFragment(){}

    public static QuizReviewFragment newInstance(Quiz quiz, QuizAnswer optedAnswer){

        QuizReviewFragment fragment = new QuizReviewFragment();
        fragment.mQuiz = quiz;
        fragment.mOptedAnswer = optedAnswer;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void setCrctWrngDrawable(View op, boolean isCrct){

        if(isCrct)
            op.setBackground( getContext().getDrawable(R.drawable.ans_crct));
        else
            op.setBackground( getContext().getDrawable(R.drawable.ans_wrong));
    }

    private void fillBgDrawable(int byteVal, View op1, View op2,View op3,View op4, boolean isCrct){

        switch (byteVal){

            case 0:

                setCrctWrngDrawable(op1,isCrct);

                break;

            case 1:

                setCrctWrngDrawable(op2,isCrct);

                break;

            case 2:

                setCrctWrngDrawable(op3,isCrct);

                break;

            case 3:

                setCrctWrngDrawable(op4,isCrct);

                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        RadioGroup radioButton = (RadioGroup) view.findViewById(R.id.options);
        radioButton.setClickable(false);

        if(mQuiz != null){

            CustomTextView questionTxt = (CustomTextView) view.findViewById(R.id.qstnTxt);
            questionTxt.setText(mQuiz.getQuestion().trim());

            if(mQuiz.getCodeSegment() != null && !mQuiz.getCodeSegment().isEmpty() ) {

                CodeView codeView = (CodeView) view.findViewById(R.id.code);

                codeView.setCode(mQuiz.getCodeSegment().trim());
            }
            LayoutInflater inflater = getLayoutInflater();

            View op1 = getRadioButton(inflater,mQuiz.getOp1(),0);
            View op2 = getRadioButton(inflater,mQuiz.getOp2(),1);
            View op3 = getRadioButton(inflater,mQuiz.getOp3(),2);
            View op4 = getRadioButton(inflater,mQuiz.getOp4(),3);

            int optedAns = mOptedAnswer.getParcelableOptedAns();

            String crctOps = mQuiz.getCrctOp();

            int frthByte = ((optedAns >> 12) & 0x000f);

            if(frthByte != 0xf){

                fillBgDrawable(frthByte,op1,op2,op3,op4,crctOps.contains(String.valueOf(frthByte)));
            }

            int thrdByte = ((optedAns >> 8) & 0x000f);

            if(thrdByte != 0xf){

                fillBgDrawable(thrdByte,op1,op2,op3,op4,crctOps.contains(String.valueOf(thrdByte)));
            }

            int scndByte = ((optedAns >> 4) & 0x000f);

            if(scndByte != 0xf){

                fillBgDrawable(scndByte,op1,op2,op3,op4,crctOps.contains(String.valueOf(scndByte)));
            }

            int frstByte = ((optedAns) & 0x000f);

            if(frstByte != 0xf){

                fillBgDrawable(frstByte,op1,op2,op3,op4,crctOps.contains(String.valueOf(frstByte)));

            }

            for(int i=0; i<mOptedAnswer.getCrctAns().length;i++){

                switch (mOptedAnswer.getCrctAns()[i]){

                    case 0:

                        op1.setBackground( getContext().getDrawable(R.drawable.ans_crct));

                        break;
                    case 1:

                        op2.setBackground( getContext().getDrawable(R.drawable.ans_crct));

                        break;
                    case 2:

                        op3.setBackground( getContext().getDrawable(R.drawable.ans_crct));

                        break;
                    case 3:

                        op4.setBackground( getContext().getDrawable(R.drawable.ans_crct));

                        break;

                }
            }

            if(op1 != null) {
                radioButton.addView(op1);
            }
            if(op2 != null)
                radioButton.addView(op2);
            if(op3 != null)
                radioButton.addView(op3);
            if(op4 != null)
                radioButton.addView(op4);

        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quiz, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private CustomRadioButton getRadioButton(LayoutInflater inflater, String option, int id){

        if(option == null || option.isEmpty())
            return null;

        CustomRadioButton customRadioButton = (CustomRadioButton) inflater.inflate(R.layout.option_radio,null);
        customRadioButton.setText(option);
        customRadioButton.setId(id);
        customRadioButton.setClickable(false);

        Resources r = getResources();
        int dp15 =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, r.getDisplayMetrics());
        int dp5 =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, r.getDisplayMetrics());

        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
        customRadioButton.setPadding(dp5,dp15,dp5,dp15);
        params.bottomMargin = dp5;

        customRadioButton.setLayoutParams(params);

        return customRadioButton;
    }

}

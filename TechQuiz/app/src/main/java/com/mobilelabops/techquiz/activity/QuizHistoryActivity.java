package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.UserHistoryAdapter;
import com.mobilelabops.techquiz.database.SubjectDbAdapter;
import com.mobilelabops.techquiz.model.CoinUseHistory;
import com.mobilelabops.techquiz.model.Result;
import com.mobilelabops.techquiz.model.Subject;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuizHistoryActivity extends BaseActivity implements IServerCallBack{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_history);

        setTitle(getString(R.string.nav_history));

        sendHistoryRequest();

    }

    private void sendHistoryRequest() {

        JSONObject params = new JSONObject();
        try {

            SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
            params.put(User.KEY_USER_ID,preferences.getString(User.KEY_USER_ID,null));

            ServerRequest request = new ServerRequest(Constant.KEY_GET_QUIZ_RESULT,params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if (result != null) {

            if (result.getResponseCode() == 200) {

                parseServerResponse(result.getResponseString());
            }
        }

        closeProgressDialog();
    }

    private void parseServerResponse(String responseString) {

        try {
            JSONObject jsonObject = new JSONObject(responseString);
            int status = jsonObject.getInt(Constant.KEY_STATUS);

            ArrayList<Result> results = new ArrayList<>();

            switch (status) {

                case Constant.ERROR_CODE_SUCCESS:

                    String resultStr = jsonObject.getString(Constant.KEY_RESULT);
                    CoinUseHistory coinUseHistory;
                    JSONArray jsonarray = new JSONArray(resultStr);
                    for (int i = 0; i < jsonarray.length(); i++) {

                        Result result = Result.createObjectFromJson(jsonarray.getJSONObject(i));
                        results.add(result);

                    }

                    break;
                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this, exception, Toast.LENGTH_SHORT).show();

                    break;
            }

            RecyclerView historyList = (RecyclerView) findViewById(R.id.historyList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            historyList.setLayoutManager(mLayoutManager);
            historyList.setItemAnimator(new DefaultItemAnimator());
            UserHistoryAdapter historyAdapter = new UserHistoryAdapter(this);
            historyAdapter.setResultHistory(results);
            SubjectDbAdapter dbAdapter = new SubjectDbAdapter();
            ArrayList<Subject> subjects = dbAdapter.getList(null,null);
            historyAdapter.setSubjects(subjects);
            historyList.setAdapter(historyAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServerReqError(Exception e) {

        closeProgressDialog();
    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog(getString(R.string.cb_please_wait));
    }
}

package com.mobilelabops.techquiz.util;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by sumanranjan on 13/11/17.
 */

public class QuizDebug extends AppCompatActivity
{
    public static final boolean DEBUG = true;

    private static final List<QuizDebugLogEntry> logEntries = new LinkedList<QuizDebugLogEntry>();

    public static void e(String tag, String msg)
    {
        if (DEBUG)
        {
            Log.e(tag, msg);
            logEntries.add(new QuizDebugLogEntry(QuizDebugLogEntry.TYPYE_ERROR, tag, msg));
        }
    }

    public static void w(String tag, String msg)
    {
        if (DEBUG)
        {
            Log.w(tag, msg);
            logEntries.add(new QuizDebugLogEntry(QuizDebugLogEntry.TYPYE_WARNING, tag, msg));
        }
    }

    public static void i(String tag, String msg)
    {
        if (DEBUG)
        {
            Log.i(tag, msg);
            logEntries.add(new QuizDebugLogEntry(QuizDebugLogEntry.TYPYE_INFO, tag, msg));
        }
    }

    public static void clearDebugLog(int mask)
    {
        ListIterator<QuizDebugLogEntry> iter = logEntries.listIterator(0);

        while (iter.hasNext())
        {
            QuizDebugLogEntry e = iter.next();
            if ((e.mType & mask) != 0)
            {
                iter.remove();
            }
        }
    }

    public static String getDebugLog(int mask)
    {
        String ret = new String();
        ListIterator<QuizDebugLogEntry> iter = logEntries.listIterator(0);

        while (iter.hasNext())
        {
            QuizDebugLogEntry e = iter.next();
            if ((e.mType & mask) != 0)
            {
                ret = ret + QuizDebugLogEntry.getTypeString(e.mType) + ":" + e.mDate + ":" + e.mTag + ":" + e.mText + "\n";
            }
        }
        return ret;
    }

    @Override public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.debug_main);
        //TextView info = (TextView) findViewById(R.id.debuglog);
        //info.setText(getDebugLog(QuizDebugLogEntry.TYPYE_ALL));
    }
}

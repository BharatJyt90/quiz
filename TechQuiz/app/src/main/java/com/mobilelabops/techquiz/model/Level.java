/**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Level {

    public final static String DATABASE_TABLE_NAME = "level";

    public static final String KEY_LEVEL_ID = "level_id";
    public static final String KEY_SUB_LEVELS = "sub_levels";
    public static final String KEY_SUB_ID = "sub_id";

    public static final String[] COLOUMNS = new String[]{KEY_LEVEL_ID, KEY_SUB_LEVELS,};

    private int mLevelId;
    private String mSubLevels;
    private int mSubId;
    private int mLockSstatus;
    private String mLvlDesc;
    private String[] mSubLevelsArr;

    public Level(){

    }

    public Level(int mLevelId, String mSubLevelCount, int mSubId, int mLockSstatus, String mLvlDesc) {
        this.mLevelId = mLevelId;
        this.mSubLevels = mSubLevelCount;
        this.mSubId = mSubId;
        this.mLockSstatus = mLockSstatus;
        this.mLvlDesc = mLvlDesc;
    }

    public String[] getSubLevels(){

        if(mSubLevels != null){

            if(mSubLevelsArr == null)
                mSubLevelsArr = mSubLevels.split(",");

        }

        return mSubLevelsArr;
    }

    public String getLvlDesc() {
        return mLvlDesc;
    }

    public void setLvlDesc(String mLvlDesc) {
        this.mLvlDesc = mLvlDesc;
    }

    public int getLockSstatus() {
        return mLockSstatus;
    }

    public void setLockSstatus(int mLockSstatus) {
        this.mLockSstatus = mLockSstatus;
    }

    public void setLevelId(int levelId) {

        this.mLevelId = levelId;

    }

    public int getLevelId() {

        return this.mLevelId;

    }

    public void setSubLevelCount(String subLevelCount) {

        this.mSubLevels = subLevelCount;

    }

    public String getSubLevelCount() {

        return this.mSubLevels;

    }

    public void setSubId(int subId) {

        this.mSubId = subId;

    }

    public int getSubId() {

        return this.mSubId;

    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_LEVEL_ID, this.mLevelId);
        json.put(KEY_SUB_LEVELS, this.mSubLevels);
        json.put(KEY_SUB_ID, this.mSubId);

        return json;

    }

    public static Level createObjectFromJson(JSONObject json) throws JSONException {

        Level obj = new Level();
        if (!json.isNull(KEY_LEVEL_ID)) {

            obj.mLevelId = json.getInt(KEY_LEVEL_ID);
        }
        if (!json.isNull(KEY_SUB_LEVELS)) {

            obj.mSubLevels = json.getString(KEY_SUB_LEVELS);
        }
        if (!json.isNull(KEY_SUB_ID)) {

            obj.mSubId = json.getInt(KEY_SUB_ID);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<Level> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (Level obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }
}

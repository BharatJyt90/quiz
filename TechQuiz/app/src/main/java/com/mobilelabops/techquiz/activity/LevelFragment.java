package com.mobilelabops.techquiz.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.ui.CarouselLinearLayout;
import com.mobilelabops.techquiz.ui.CustomTextView;

public class LevelFragment extends Fragment {

    private static final String POSITON = "position";
    private static final String SCALE = "scale";
    private static final String DRAWABLE_RESOURE = "resource";
    private static final String LEVEL_INDEX = "level_id";
    private static final String SUBJECT_INDEX = "sub_id";
    private static final String SUBJECT_NAME = "sub_name";

    private int screenWidth;
    private int screenHeight;
    private static int subject_id;
    private static String subject_name;

    // TODO : Replace with actual images later
//    private int[] imageArray = new int[]{R.drawable.image1, R.drawable.image2,
//            R.drawable.image3, R.drawable.image4};

    // String Array for Level Text
    String[] lvlArray;
    String[] motoArray;
    String[] lvlDesc;

    public static Fragment newInstance(LevelActivity context, int pos, float scale, int sub_id, String sub_name) {
        Bundle b = new Bundle();
        b.putInt(POSITON, pos);
        b.putFloat(SCALE, scale);
        subject_id = sub_id;
        subject_name = sub_name;
        return Fragment.instantiate(context, LevelFragment.class.getName(), b);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWidthAndHeight();
        lvlArray = getResources().getStringArray(R.array.levelArray);
        motoArray = getResources().getStringArray(R.array.levelMoto);
        lvlDesc = getResources().getStringArray(R.array.levelDesc);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        final int position = this.getArguments().getInt(POSITON);
        float scale = this.getArguments().getFloat(SCALE);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth / 2 + 200, screenHeight / 2 + 300) ;
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_level, container, false);

        CustomTextView levelTxt, levelMoto, levelDesc;
        ImageView levelImage;
        RelativeLayout llText, lvlButton;
        CarouselLinearLayout root;

        levelTxt = linearLayout.findViewById(R.id.lvlText);
        levelMoto = linearLayout.findViewById(R.id.lvlMotoText);
        root = linearLayout.findViewById(R.id.root_container);
        llText = linearLayout.findViewById(R.id.llText);
        lvlButton = linearLayout.findViewById(R.id.lvlButton);
        levelDesc = linearLayout.findViewById(R.id.lvlTextMsg);

        llText.setLayoutParams(new LinearLayout.LayoutParams((screenWidth / 2 + 200), screenHeight / 2 + 300));

        //levelImage.setLayoutParams(layoutParams);
        //levelImage.setImageResource(imageArray[position]);

        levelTxt.setText(lvlArray[position]);
        levelMoto.setText(motoArray[position]);
        levelDesc.setText(lvlDesc[position]);

        //handle click event
        lvlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SubLevelActivity.class);
                //intent.putExtra(DRAWABLE_RESOURE, imageArray[position]);
                intent.putExtra(LEVEL_INDEX, position);
                intent.putExtra(SUBJECT_INDEX, subject_id);
                intent.putExtra(SUBJECT_NAME, subject_name);
                startActivity(intent);
            }
        });
        root.setScaleBoth(scale);

        return linearLayout;
    }

    /**
     * Get device screen width and height
     */
    private void getWidthAndHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }
}

package com.mobilelabops.techquiz.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.SubjectGridAdapter;
import com.mobilelabops.techquiz.database.QuizLocalDB;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.model.Subject;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.QuizDebug;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,IServerCallBack,View.OnClickListener {

    SubjectGridAdapter quizAdapter;
    ArrayList<Subject> quizArrayList = new ArrayList<Subject>();

    ProgressDialog mProgressDialog;
    ImageView mUserImg;
    public static final int RESULT_CODE_PROFILE_PIC_CHANGED = 1;
    public User mCurrentUser;
	public static final String SUB_ID_INDEX = "sub_id", SUB_NAME="name";

    private GridView mQuizGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int contentInsetStartWithNavigation = toolbar.getContentInsetStartWithNavigation();
        toolbar.setContentInsetsRelative(0, contentInsetStartWithNavigation);

        Log.d("mainactivity","density = " + getResources().getDisplayMetrics().density);


        if (Util.isNetworkConnected(this)) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

            // Server API : Load Subjects
            getSubjectDetails();
        } else {
            Util.showNoInternetDialog(this);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mQuizGrid = (GridView)findViewById(R.id.quizGridView);
        quizAdapter = new SubjectGridAdapter(this, quizArrayList);
        mQuizGrid.setAdapter(quizAdapter);

        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics());
        DisplayMetrics dm = getResources().getDisplayMetrics();
        mQuizGrid.setColumnWidth(dm.widthPixels/ 3 -px);

        QuizDebug.e("MainActivity", "size = " + quizArrayList.size());

        // Language click handler
        mQuizGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, LevelActivity.class);
                intent.putExtra(SUB_ID_INDEX, quizArrayList.get(position).getSubId());
                intent.putExtra(SUB_NAME, quizArrayList.get(position).getName());
                startActivity(intent);

            }
        });

        mUserImg = (ImageView) findViewById(R.id.userImg);
        mUserImg.setOnClickListener(this);

        mCurrentUser = getIntent().getParcelableExtra(User.class.getSimpleName());

        setProfilePic();

        TextView userNameTxt = (TextView) findViewById(R.id.userName);
        userNameTxt.setText(mCurrentUser.getName());


        TextView userEmailTxt = (TextView) findViewById(R.id.userEmail);
        userEmailTxt.setText(Integer.toString(mCurrentUser.getCoins())+ " Coins");
    }

    public void quizClicked(View v) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void leadershipClicked(View v) {
        startActivity(new Intent(this,LeadershipActivity.class));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void historyClicked(View v) {
        startActivity(new Intent(this,QuizHistoryActivity.class));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void dashboardClicked(View v) {
        startActivity(new Intent(this,DashboardActivity.class));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void buyCoinClicked(View v){

        startActivity(new Intent(this,PaymentGatewaySelectionActivity.class));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void referralClicked(View v) {

        Intent EarnCoinActivityIntent = new Intent(this,ReferralActivity.class);

        EarnCoinActivityIntent.putExtra(User.class.getSimpleName(),mCurrentUser);

        startActivity(EarnCoinActivityIntent);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
    public void termClicked(View v) {
        Intent TermsActivityIntent = new Intent(this,TermsActivity.class);
        startActivity(TermsActivityIntent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void settingClicked(View v) {
        Intent userIntent = new Intent(this,UserActivity.class);
        userIntent.putExtra(User.class.getSimpleName(),mCurrentUser);
        startActivityForResult(userIntent,0);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void logoutClicked(View v) {
        startActivity(new Intent(this,EarnCoinActivity.class));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void getSubjectDetails() {

        JSONObject params = new JSONObject();
        try {

            ServerRequest request = new ServerRequest(Constant.KEY_SUBJECT_URL, params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    long mBackPressed;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (mBackPressed + 1000 > System.currentTimeMillis())
            {
                super.onBackPressed();
                return;
            }
            else {

                LinearLayout coordinatorLayout = (LinearLayout) findViewById(R.id
                        .cordinatorLayout);
                Snackbar.make(coordinatorLayout, "Tap back button twice to exit", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }

            mBackPressed = System.currentTimeMillis();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.nav_earn) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_earn) {
            startActivity(new Intent(this,EarnCoinActivity.class));
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private IServerCallBack pendingPurchaseUploadCallBack = new IServerCallBack() {
        @Override
        public void onServeReqResult(ServerResponse result) {

            if(result != null && result.getResponseCode() == 200){

                try {
                    JSONObject jsonObject = new JSONObject(result.getResponseString());
                    int status = jsonObject.getInt(Constant.KEY_STATUS);

                    if(status == Constant.ERROR_CODE_SUCCESS){

                        int amount = jsonObject.getInt(PurchaseHistory.KEY_COINS_CREDIT);
                        mCurrentUser.setCoins(mCurrentUser.getCoins()+amount);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onServerReqError(Exception e) {

        }

        @Override
        public void onServerReqStarted() {

        }
    };

    @Override
    public void onServeReqResult(ServerResponse result) {

        Util.uploadPurchaseDataIfAny(this,pendingPurchaseUploadCallBack);
        mProgressDialog.dismiss();
        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }
            else{
                quizArrayList = QuizLocalDB.getAllSubjects();

                quizAdapter.notifyDataSetInvalidated();
            }//else
        }
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                try {
                    QuizLocalDB.deleteSubjectTable();

                    if (jsonObject.has(Constant.KEY_RESULT)) {

                        String resultStr = jsonObject.getString(Constant.KEY_RESULT);
                        Subject subEntry;
                        JSONArray jsonarray = new JSONArray(resultStr);

                        for (int i = 0; i < jsonarray.length(); i++) {
                            subEntry = Subject.createObjectFromJson(jsonarray.getJSONObject(i));
                            QuizLocalDB.addSingleSubject(subEntry);

                            quizArrayList.add(subEntry);

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                quizAdapter.notifyDataSetInvalidated();

                   break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_NO_RECORDS_FOUND",Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this,"ERROR_CODE_PASSWORD_MISMATCH",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this,"ERROR_USER_VERIFICATION_PENDING",Toast.LENGTH_SHORT).show();

                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        mProgressDialog.dismiss();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_CODE_PROFILE_PIC_CHANGED ){

            setProfilePic();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setProfilePic(){

        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE);
        String profilePic = preferences.getString(User.KEY_PROFILE_PIC,null);

        if(profilePic != null){

            mUserImg.setImageURI(null);
            if(profilePic.startsWith("http"))
                Glide.with(getApplicationContext()).load(profilePic).thumbnail(0.2f).into(mUserImg);
            else
            mUserImg.setImageURI(Uri.parse(profilePic));
          //  Glide.with(getApplicationContext()).load(profilePic).thumbnail(0.2f).into(mUserImg);
        }else{

            if(mCurrentUser.getRegType() == LoginActivity.MANUAL_LOGIN){

                profilePic = Constant.KEY_DOWNLOAD_IMAGE_IP+mCurrentUser.getUserId()+".jpg";

                Glide.with(getApplicationContext()).load(profilePic).thumbnail(0.2f).into(mUserImg);

            }
        }
    }

    @Override
    public void onServerReqError(Exception e) {

        mProgressDialog.dismiss();

        // Fetch data from database
        quizArrayList = QuizLocalDB.getAllSubjects();

        quizAdapter.notifyDataSetInvalidated();

    }

    @Override
    public void onServerReqStarted() {

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.userImg:


                Intent userIntent = new Intent(this,UserActivity.class);

                userIntent.putExtra(User.class.getSimpleName(),mCurrentUser);

                startActivityForResult(userIntent,0);

                break;
        }
    }
}

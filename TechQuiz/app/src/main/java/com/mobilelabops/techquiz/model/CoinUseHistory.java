/**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CoinUseHistory implements Parcelable {

    public final static String DATABASE_TABLE_NAME = "coin_use_history";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_DATE_TIME = "date_time";
    public static final String KEY_SUB_ID = "sub_id";
    public static final String KEY_LEVEL_ID = "level_id";
    public static final String KEY_SUB_LEVEL_ID = "sub_level_id";
    public static final String KEY_COINS_DEBIT = "coins_debit";

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_DATE_TIME, KEY_SUB_ID, KEY_LEVEL_ID, KEY_SUB_LEVEL_ID, KEY_COINS_DEBIT,};

    private int mUserId;
    private long mDateTime;
    private int mSubId;
    private int mLevelId;
    private int mSubLevelId;
    private int mCoinsDebit;

    public CoinUseHistory(){

    }

    public void setUserId(int userId) {

        this.mUserId = userId;

    }

    public int getUserId() {

        return this.mUserId;

    }

    public void setDateTime(long dateTime) {

        this.mDateTime = dateTime;

    }

    public long getDateTime() {

        return this.mDateTime;

    }

    public void setSubId(int subId) {

        this.mSubId = subId;

    }

    public int getSubId() {

        return this.mSubId;

    }

    public void setLevelId(int levelId) {

        this.mLevelId = levelId;

    }

    public int getLevelId() {

        return this.mLevelId;

    }

    public void setSubLevelId(int subLevelId) {

        this.mSubLevelId = subLevelId;

    }

    public int getSubLevelId() {

        return this.mSubLevelId;

    }

    public void setCoinsDebit(int coinsDebit) {

        this.mCoinsDebit = coinsDebit;

    }

    public int getCoinsDebit() {

        return this.mCoinsDebit;

    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_DATE_TIME, this.mDateTime);
        json.put(KEY_SUB_ID, this.mSubId);
        json.put(KEY_LEVEL_ID, this.mLevelId);
        json.put(KEY_SUB_LEVEL_ID, this.mSubLevelId);
        json.put(KEY_COINS_DEBIT, this.mCoinsDebit);

        return json;

    }

    public static CoinUseHistory createObjectFromJson(JSONObject json) throws JSONException {

        CoinUseHistory obj = new CoinUseHistory();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getInt(KEY_USER_ID);
        }
        if (!json.isNull(KEY_DATE_TIME)) {

            obj.mDateTime = json.getLong(KEY_DATE_TIME);
        }
        if (!json.isNull(KEY_SUB_ID)) {

            obj.mSubId = json.getInt(KEY_SUB_ID);
        }
        if (!json.isNull(KEY_LEVEL_ID)) {

            obj.mLevelId = json.getInt(KEY_LEVEL_ID);
        }
        if (!json.isNull(KEY_SUB_LEVEL_ID)) {

            obj.mSubLevelId = json.getInt(KEY_SUB_LEVEL_ID);
        }
        if (!json.isNull(KEY_COINS_DEBIT)) {

            obj.mCoinsDebit = json.getInt(KEY_COINS_DEBIT);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<CoinUseHistory> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (CoinUseHistory obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }

    private CoinUseHistory(Parcel in) {

        mUserId = in.readInt();
        mDateTime = in.readLong();
        mSubId = in.readInt();
        mLevelId = in.readInt();
        mSubLevelId = in.readInt();
        mCoinsDebit = in.readInt();

    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeInt(mUserId);
        out.writeLong(mDateTime);
        out.writeInt(mSubId);
        out.writeInt(mLevelId);
        out.writeInt(mSubLevelId);
        out.writeInt(mCoinsDebit);
    }

    public static final Parcelable.Creator<CoinUseHistory> CREATOR = new Parcelable.Creator<CoinUseHistory>() {

        public CoinUseHistory createFromParcel(Parcel in) {
            return new CoinUseHistory(in);
        }

        public CoinUseHistory[] newArray(int size) {
            return new CoinUseHistory[size];
        }
    };

    public int describeContents() {
        return 0;
    }
}

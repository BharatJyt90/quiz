package com.mobilelabops.techquiz.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mobilelabops.techquiz.database.PurchaseHistoryDbAdapter;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.util.UploadCoinPurchaseData;
import com.mobilelabops.techquiz.util.Util;

import java.util.ArrayList;

public class NetwrokConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Util.uploadPurchaseDataIfAny(context,null);
    }
}

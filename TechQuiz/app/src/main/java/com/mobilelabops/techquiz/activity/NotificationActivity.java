package com.mobilelabops.techquiz.activity;

import android.os.Bundle;
import android.widget.ListView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.NotificationAdapter;
import com.mobilelabops.techquiz.model.Notification;

import java.util.ArrayList;

public class NotificationActivity extends BaseActivity {

    ArrayList<Notification> sArrayList = new ArrayList<Notification>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setTitle(getString(R.string.notification_title));

        final ListView listview = (ListView) findViewById(R.id.notification_list);

        // Get Notifications from Database
        // TODO : temp code for testing
        Notification ntc = new Notification();
        ntc.setTitle("Quiz update");
        ntc.setMsg("A new Quiz has been added to C-Beginners. Check out !!");
        ntc.setTime("17 Nov 2017, 23:12:05");

        Notification ntc1 = new Notification();
        ntc1.setTitle("Level update");
        ntc1.setMsg("A new Level has been added to C-Beginners. Check out !!");
        ntc1.setTime("17 Nov 2017, 23:12:05");

        sArrayList.add(0, ntc);
        //sArrayList.add(ntc1);
        sArrayList.add(1,ntc1);

        NotificationAdapter ntcAdapter = new NotificationAdapter(this, sArrayList);
        listview.setAdapter(ntcAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

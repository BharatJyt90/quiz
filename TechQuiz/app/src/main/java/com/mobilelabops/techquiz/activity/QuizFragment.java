package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.QuizAnswer;
import com.mobilelabops.techquiz.ui.CustomCheckBox;
import com.mobilelabops.techquiz.ui.CustomRadioButton;
import com.mobilelabops.techquiz.ui.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import io.github.kbiakov.codeview.CodeView;
import io.github.kbiakov.codeview.adapters.Options;
import io.github.kbiakov.codeview.highlight.ColorTheme;
import io.github.kbiakov.codeview.highlight.ColorThemeData;
import io.github.kbiakov.codeview.highlight.SyntaxColors;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuizFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuizFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuizFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener{

    private OnFragmentInteractionListener mListener;
    private Quiz mQuiz;
    private boolean mIsAttempted;
    private int mFragmentPostion;

    private static HashMap<Integer,QuizAnswer> mQuizAnswerMap;

    public static HashMap<Integer,QuizAnswer> getQuizAnswerMap(){

        return mQuizAnswerMap;
    }

    public static void clearAnswerMap(){

        mQuizAnswerMap.clear();
    }

    public QuizFragment() {
        // Required empty public constructor
    }


    public static QuizFragment newInstance(Quiz quiz,int position) {
        QuizFragment fragment = new QuizFragment();
        fragment.mQuiz = quiz;
        fragment.mFragmentPostion = position;

        if(mQuizAnswerMap == null)
            mQuizAnswerMap = new HashMap<>();

        QuizAnswer quizAnswer = mQuizAnswerMap.get(position);

        if(quizAnswer == null){

            quizAnswer = new QuizAnswer();

            quizAnswer.setCrctAns(quiz.getCrctOp());
            quizAnswer.setQId(quiz.getQId());

            mQuizAnswerMap.put(position,quizAnswer);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        RadioGroup radioButton = (RadioGroup) view.findViewById(R.id.options);
        radioButton.setOnCheckedChangeListener(this);

        if(mQuiz != null){

            CustomTextView questionTxt = (CustomTextView) view.findViewById(R.id.qstnTxt);
            questionTxt.setText(mQuiz.getQuestion().trim());

            if(mQuiz.getCodeSegment() != null && !mQuiz.getCodeSegment().isEmpty() ) {

                CodeView codeView = (CodeView) view.findViewById(R.id.code);

                codeView.setCode(mQuiz.getCodeSegment().trim());
            }
            LayoutInflater inflater = getLayoutInflater();

            View op1 = mQuiz.getType() == 1 ? getCheckBox(inflater,mQuiz.getOp1(),0) : getRadioButton(inflater,mQuiz.getOp1(),0);
            View op2 = mQuiz.getType() == 1 ? getCheckBox(inflater,mQuiz.getOp2(),1) : getRadioButton(inflater,mQuiz.getOp2(),1);
            View op3 = mQuiz.getType() == 1 ? getCheckBox(inflater,mQuiz.getOp3(),2) : getRadioButton(inflater,mQuiz.getOp3(),2);
            View op4 = mQuiz.getType() == 1 ? getCheckBox(inflater,mQuiz.getOp4(),3) : getRadioButton(inflater,mQuiz.getOp4(),3);

            if(op1 != null)
                radioButton.addView(op1);
            if(op2 != null)
                radioButton.addView(op2);
            if(op3 != null)
                radioButton.addView(op3);
            if(op4 != null)
                radioButton.addView(op4);

        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_quiz, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    private CustomRadioButton getRadioButton(LayoutInflater inflater, String option, int id){

        if(option == null || option.isEmpty())
            return null;

        CustomRadioButton customRadioButton = (CustomRadioButton) inflater.inflate(R.layout.option_radio,null);
        customRadioButton.setText(option);
        customRadioButton.setId(id);

        Resources r = getResources();
        int dp15 =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, r.getDisplayMetrics());
        int dp5 =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, r.getDisplayMetrics());

        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
        customRadioButton.setPadding(dp5,dp15,dp5,dp15);
        params.bottomMargin = dp5;

        customRadioButton.setLayoutParams(params);

        return customRadioButton;
    }

    private CustomCheckBox getCheckBox(LayoutInflater inflater, String option, int id){

        if(option == null || option.isEmpty())
            return null;

        CustomCheckBox customCheckBox = (CustomCheckBox) inflater.inflate(R.layout.option_checkbox,null);
        customCheckBox.setText(option);
        customCheckBox.setId(id);
        customCheckBox.setOnCheckedChangeListener(this);

        Resources r = getResources();
        int dp15 =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, r.getDisplayMetrics());
        int dp5 =  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, r.getDisplayMetrics());

        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
        customCheckBox.setPadding(dp5,dp15,dp5,dp15);
        params.bottomMargin = dp5;

        customCheckBox.setLayoutParams(params);
        return customCheckBox;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        CustomRadioButton rb = (CustomRadioButton) group.findViewById(checkedId);
        rb.setTypeFace("Rubik-Medium.ttf",getContext());

        int count = group.getChildCount();

        if(mIsAttempted == false){

            if(mListener != null)
                mListener.onQuestionAttempted(mFragmentPostion);
        }

        mIsAttempted = true;

        QuizAnswer answer = mQuizAnswerMap.get(mFragmentPostion);

        HashSet<Integer> optedAns = answer.getOptedAns();

        optedAns.clear();
        optedAns.add(checkedId);

        for(int i=0 ; i< count; i++){

            CustomRadioButton customRadioButton = (CustomRadioButton) group.getChildAt(i);

            if(customRadioButton.getId() != checkedId)
            {
                customRadioButton.setTypeFace("Rubik-Regular.ttf",getContext());
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        CustomCheckBox customCheckBox = (CustomCheckBox) buttonView;

        QuizAnswer answer = mQuizAnswerMap.get(mFragmentPostion);

        HashSet<Integer> optedAns = answer.getOptedAns();

        if(isChecked){

            customCheckBox.setTypeFace("Rubik-Medium.ttf",getContext());

            if(mIsAttempted == false){

                if(mListener != null)
                    mListener.onQuestionAttempted(mFragmentPostion);
            }

            mIsAttempted = true;

            optedAns.add(customCheckBox.getId());


        }else{

            customCheckBox.setTypeFace("Rubik-Regular.ttf",getContext());

            optedAns.remove(customCheckBox.getId());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onQuestionAttempted(int fragmentNum);
    }
}

package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.PurchaseHistory;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 18/11/17.
 */

public class CoinHistoryAdapter extends BaseAdapter {

    ArrayList<PurchaseHistory> sArrayList = new ArrayList<PurchaseHistory>();
    private LayoutInflater mInflater;

    public CoinHistoryAdapter(Context context, ArrayList<PurchaseHistory> notifications){
        sArrayList = notifications;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (sArrayList == null)
            return 0;
        return sArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return sArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public TextView earning_mode;
        public TextView coin_credit;
        public TextView credit_date;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = view;
        // reuse views
        if (rowView == null) {
            rowView = mInflater.inflate(R.layout.coin_purchase_element, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.earning_mode = (TextView) rowView.findViewById(R.id.earning_mode);
            viewHolder.coin_credit = (TextView) rowView.findViewById(R.id.coin_credited);
            viewHolder.credit_date = (TextView) rowView.findViewById(R.id.coin_credited_date);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        String purchase_type = "";
        switch(sArrayList.get(i).getPurchaseType()){
            case 1: break;
            case 5: purchase_type = "Quiz Feedback";break;
            default:purchase_type = "Video Ads";
        }
        holder.earning_mode.setText(purchase_type);
        String coin_credited = Integer.toString(sArrayList.get(i).getCoinsCredit());
        holder.coin_credit.setText(coin_credited);
        holder.credit_date.setText("10Dec 2017");

        return rowView;
    }
}

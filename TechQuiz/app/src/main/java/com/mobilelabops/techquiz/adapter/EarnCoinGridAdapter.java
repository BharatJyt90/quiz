package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;

/**
 * Created by bhazarix on 10/19/2017.
 */

public class EarnCoinGridAdapter extends BaseAdapter {

    private Context mContext;
    private int[] imgSrc,textDesc,cointTxt;

    public EarnCoinGridAdapter(Context context)
    {
        super();
        mContext = context;

        imgSrc = new int[]{R.drawable.watch_vdo,R.drawable.buy_coins,R.drawable.rate,R.drawable.invite_frnd,R.drawable.submit,R.drawable.submit_quiz};
        textDesc = new int[]{R.string.watch_ad,R.string.buy_coins,R.string.rate,R.string.invite_frnd,R.string.send_feedback,R.string.submit_quiz};
        cointTxt = new int[]{R.string.earn_20,R.string.real_money,R.string.earn_10,R.string.earn_25,R.string.earn_10,R.string.earn_15};
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.grid_box_earn_coin, null);
        }

        ImageView imageIcon = convertView.findViewById(R.id.imageIcon);
        imageIcon.setImageResource(imgSrc[position]);

        TextView textView = (TextView) convertView.findViewById(R.id.title);
        textView.setText(textDesc[position]);

        TextView tv = (TextView) convertView.findViewById(R.id.pointsCount);
        tv.setText(cointTxt[position]);

        return convertView;
    }
}

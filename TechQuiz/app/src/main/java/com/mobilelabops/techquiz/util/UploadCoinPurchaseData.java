package com.mobilelabops.techquiz.util;

import android.content.Context;

import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bhazarix on 11/29/2017.
 */

public class UploadCoinPurchaseData {

    public static void savePurchase(Context context, PurchaseHistory purchaseHistory, IServerCallBack callBack){

        try {
            JSONObject purchaseJsonObject = purchaseHistory.getJsonObj();
            ServerRequest request = new ServerRequest(Constant.KEY_COIN_PURCHASE_SAVE_URL,purchaseJsonObject.toString(), Util.getJsonContentTypeHeader(context));

            ServerAsynTask asynTask = new ServerAsynTask(callBack);

            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}

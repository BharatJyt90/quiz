package com.mobilelabops.techquiz.model; /**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

;

public class Contribution {

    public final static String DATABASE_TABLE_NAME = "contribution";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_CONTRIBUTION_DATE = "contribution_date";
    public static final String KEY_SUB_ID = "sub_id";
    public static final String KEY_LEVEL_ID = "level_id";
    public static final String KEY_SUB_LEVEL_ID = "sub_level_id";
    public static final String KEY_EARNED_POINTS = "earned_points";
    public static final String KEY_RATING = "rating";
    public static final String KEY_VIEWS = "views";

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_CONTRIBUTION_DATE, KEY_SUB_ID, KEY_LEVEL_ID, KEY_SUB_LEVEL_ID, KEY_EARNED_POINTS, KEY_RATING, KEY_VIEWS,};

    private String mUserId;
    private long mContributionDate;
    private int mSubId;
    private int mLevelId;
    private int mSubLevelId;
    private int mEarnedPoints;
    private int mRating;
    private int mViews;

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setContributionDate(long contributionDate) {

        this.mContributionDate = contributionDate;

    }

    public long getContributionDate() {

        return this.mContributionDate;

    }

    public void setSubId(int subId) {

        this.mSubId = subId;

    }

    public int getSubId() {

        return this.mSubId;

    }

    public void setLevelId(int levelId) {

        this.mLevelId = levelId;

    }

    public int getLevelId() {

        return this.mLevelId;

    }

    public void setSubLevelId(int subLevelId) {

        this.mSubLevelId = subLevelId;

    }

    public int getSubLevelId() {

        return this.mSubLevelId;

    }

    public void setEarnedPoints(int earnedPoints) {

        this.mEarnedPoints = earnedPoints;

    }

    public int getEarnedPoints() {

        return this.mEarnedPoints;

    }

    public void setRating(int rating) {

        this.mRating = rating;

    }

    public int getRating() {

        return this.mRating;

    }

    public void setViews(int views) {

        this.mViews = views;

    }

    public int getViews() {

        return this.mViews;

    }


    public static String getCrateTableStatement() {

        StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

        createStatement.append(DATABASE_TABLE_NAME).append(" (");

        createStatement.append(KEY_USER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_CONTRIBUTION_DATE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
        createStatement.append(KEY_SUB_ID).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_LEVEL_ID).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_SUB_LEVEL_ID).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_EARNED_POINTS).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_RATING).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_VIEWS).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(")");

        return createStatement.toString();
    }

    public ContentValues getContentValues() {
        ContentValues initialValues = new ContentValues();

        initialValues.put(KEY_USER_ID, this.mUserId);
        initialValues.put(KEY_CONTRIBUTION_DATE, this.mContributionDate);
        initialValues.put(KEY_SUB_ID, this.mSubId);
        initialValues.put(KEY_LEVEL_ID, this.mLevelId);
        initialValues.put(KEY_SUB_LEVEL_ID, this.mSubLevelId);
        initialValues.put(KEY_EARNED_POINTS, this.mEarnedPoints);
        initialValues.put(KEY_RATING, this.mRating);
        initialValues.put(KEY_VIEWS, this.mViews);
        return initialValues;
    }

    public static Contribution createObjectFromCursor(Cursor cursor) {
        Contribution obj = new Contribution();
        obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
        obj.setContributionDate(cursor.getLong((cursor.getColumnIndex(KEY_CONTRIBUTION_DATE))));
        obj.setSubId(cursor.getInt((cursor.getColumnIndex(KEY_SUB_ID))));
        obj.setLevelId(cursor.getInt((cursor.getColumnIndex(KEY_LEVEL_ID))));
        obj.setSubLevelId(cursor.getInt((cursor.getColumnIndex(KEY_SUB_LEVEL_ID))));
        obj.setEarnedPoints(cursor.getInt((cursor.getColumnIndex(KEY_EARNED_POINTS))));
        obj.setRating(cursor.getInt((cursor.getColumnIndex(KEY_RATING))));
        obj.setViews(cursor.getInt((cursor.getColumnIndex(KEY_VIEWS))));
        return obj;
    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_CONTRIBUTION_DATE, this.mContributionDate);
        json.put(KEY_SUB_ID, this.mSubId);
        json.put(KEY_LEVEL_ID, this.mLevelId);
        json.put(KEY_SUB_LEVEL_ID, this.mSubLevelId);
        json.put(KEY_EARNED_POINTS, this.mEarnedPoints);
        json.put(KEY_RATING, this.mRating);
        json.put(KEY_VIEWS, this.mViews);

        return json;

    }

    public static Contribution createObjectFromJson(JSONObject json) throws JSONException {

        Contribution obj = new Contribution();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_CONTRIBUTION_DATE)) {

            obj.mContributionDate = json.getLong(KEY_CONTRIBUTION_DATE);
        }
        if (!json.isNull(KEY_SUB_ID)) {

            obj.mSubId = json.getInt(KEY_SUB_ID);
        }
        if (!json.isNull(KEY_LEVEL_ID)) {

            obj.mLevelId = json.getInt(KEY_LEVEL_ID);
        }
        if (!json.isNull(KEY_SUB_LEVEL_ID)) {

            obj.mSubLevelId = json.getInt(KEY_SUB_LEVEL_ID);
        }
        if (!json.isNull(KEY_EARNED_POINTS)) {

            obj.mEarnedPoints = json.getInt(KEY_EARNED_POINTS);
        }
        if (!json.isNull(KEY_RATING)) {

            obj.mRating = json.getInt(KEY_RATING);
        }
        if (!json.isNull(KEY_VIEWS)) {

            obj.mViews = json.getInt(KEY_VIEWS);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<Contribution> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (Contribution obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }
}

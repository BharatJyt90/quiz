package com.mobilelabops.techquiz.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.ProgressBar;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.QuizAnswer;
import com.mobilelabops.techquiz.ui.CustomTextView;

import java.util.ArrayList;
import java.util.Collection;

public class QuizReviewActivity extends BaseActivity implements ViewPager.OnPageChangeListener{

    private ArrayList<Quiz> mQuizArrayList;
    private ArrayList<QuizAnswer> mOptedQuizAnswers;
    private CustomTextView mCountTxt,mAttemptedQTxt;

    private ProgressBar mCircularProgressbar;
    private int mQuizSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_quiz);

        mQuizArrayList = getIntent().getParcelableArrayListExtra(Quiz.class.getSimpleName());
        mOptedQuizAnswers = getIntent().getParcelableArrayListExtra(QuizAnswer.class.getSimpleName());
        mCountTxt = (CustomTextView) findViewById(R.id.countTxt);
        mCircularProgressbar = (ProgressBar) findViewById(R.id.circularProgressbar);
        mAttemptedQTxt = (CustomTextView) findViewById(R.id.attempted_q_txt);

        mQuizSize = mQuizArrayList.size();

        updateUI();
    }

    private void updateUI(){

        if(mQuizArrayList == null || mQuizArrayList.isEmpty()){

            return;
        }

        mCircularProgressbar.setMax(mQuizSize);
        mCircularProgressbar.setSecondaryProgress(mQuizSize);
        ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOnPageChangeListener(this);
        mPager.setAdapter(new QuizReviewActivity.QuizReviewAdapter(getSupportFragmentManager()));
        mPager.setPageTransformer(true, new QuizReviewActivity.DepthPageTransformer());

        if(mOptedQuizAnswers != null){

            mAttemptedQTxt.setText(getString(R.string.attempted_quiz)+mOptedQuizAnswers.size()+" /"+mQuizSize);
        }
        else
            mAttemptedQTxt.setText(getString(R.string.attempted_quiz)+" 0/"+mQuizSize);

        onPageSelected(0);

    }

    @Override
    public void onPageSelected(int position) {

        mCircularProgressbar.setProgress(position+1);
        mCountTxt.setText(String.valueOf(position+1));

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class QuizReviewAdapter extends FragmentStatePagerAdapter {

        public QuizReviewAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return QuizReviewFragment.newInstance(mQuizArrayList.get(position),mOptedQuizAnswers.get(position));
        }

        @Override
        public int getCount() {
            return mQuizArrayList.size();
        }
    }

    private class DepthPageTransformer implements ViewPager.PageTransformer {

        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}

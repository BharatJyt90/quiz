package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.ui.CustomButton;

public class ReferralActivity extends BaseActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral);
        setTitle(getString(R.string.invite_frnd));

        CustomButton inviteButton = (CustomButton) findViewById(R.id.inviteBtn);
        inviteButton.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void shareText(String subject, String body) {
        Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
        txtIntent .setType("text/plain");
        txtIntent .putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(txtIntent ,"Share"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.inviteBtn:

                //TODO : Referral code to be stored
                shareText(getString(R.string.referral_invite_subject), getString(R.string.referral_invite_body));

                break;
        }
    }
}

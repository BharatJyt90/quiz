package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.TempRegTable;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by bhazarix on 9/2/2017.
 */

public class EmailVerificationActivity extends AppCompatActivity implements IServerCallBack{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verification);

        String verificationCode = getIntent().getStringExtra(TempRegTable.KEY_VERFICATION_CODE);

        if(verificationCode != null ){

            findViewById(R.id.reqSendAgainTxt).setVisibility(View.INVISIBLE);
        }
    }

    private void sendVerificationCode(){

        String code = ((CustomEditText) findViewById(R.id.verificationCode)).getText().toString();

        if(code.isEmpty()){

            return;

        }

        String verificationCode = getIntent().getStringExtra(TempRegTable.KEY_VERFICATION_CODE);

        if(verificationCode != null ){

            if(verificationCode.equalsIgnoreCase(code)){

                Intent updatePassIntent = new Intent(this,UpdatePasswordActivity.class);

                String emailId = getIntent().getStringExtra(TempRegTable.KEY_EMAIL_ID);

                updatePassIntent.putExtra(User.KEY_EMAIL_ID,emailId);

                startActivity(updatePassIntent);

                finish();

            }else{

                Toast.makeText(getApplicationContext(),"Code not matching",Toast.LENGTH_SHORT).show();

            }

            return;
        }

        JSONObject params = new JSONObject();
        try {

            params.put(TempRegTable.KEY_EMAIL_ID,getIntent().getStringExtra(TempRegTable.KEY_EMAIL_ID));

            params.put(TempRegTable.KEY_USER_ID,getIntent().getStringExtra(TempRegTable.KEY_USER_ID));

            params.put(TempRegTable.KEY_VERFICATION_CODE, code);

            ServerRequest request = new ServerRequest(Constant.KEY_USER_VERIFY_URL,params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void reqVerificationCode(){


        JSONObject params = new JSONObject();
        try {

            params.put(TempRegTable.KEY_EMAIL_ID,getIntent().getStringExtra(TempRegTable.KEY_EMAIL_ID));

            params.put(TempRegTable.KEY_USER_ID,getIntent().getStringExtra(TempRegTable.KEY_USER_ID));

            ServerRequest request = new ServerRequest(Constant.KEY_USER_REQ_CODE_URL,params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(null);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void onClick(View v){

        switch (v.getId()){

            case R.id.verifyBtn:

                sendVerificationCode();

                break;

            case R.id.reqSendAgainTxt:

                reqVerificationCode();

                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    // success

                    setResult(RegistrationActivity.ACTIVITY_RESULT_CODE_SUCCESS);
                    finish();

                    break;

                case Constant.ERROR_USER_VERIFICATION_CODE_MISMATCH:

                    // ERROR_USER_VERIFICATION_CODE_MISMATCH
                    Toast.makeText(this,"ERROR_USER_VERIFICATION_CODE_MISMATCH",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_CODE_EXPIRED:

                    //ERROR_USER_VERIFICATION_CODE_MISMATCH
                    Toast.makeText(this,"ERROR_USER_VERIFICATION_CODE_EXPIRED",Toast.LENGTH_SHORT).show();

                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);
                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }

    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

    }
}

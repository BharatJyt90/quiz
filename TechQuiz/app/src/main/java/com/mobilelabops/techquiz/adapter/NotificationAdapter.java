package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Notification;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 18/11/17.
 */

public class NotificationAdapter extends BaseAdapter {

    ArrayList<Notification> sArrayList = new ArrayList<Notification>();
    private LayoutInflater mInflater;

    public NotificationAdapter(Context context, ArrayList<Notification> notifications){
        sArrayList = notifications;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (sArrayList == null)
            return 0;
        return sArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return sArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public TextView text;
        public TextView image;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = view;
        // reuse views
        if (rowView == null) {
            rowView = mInflater.inflate(R.layout.notification_element, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.notification_lb);
            viewHolder.image = (TextView) rowView.findViewById(R.id.notification_msg);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.text.setText(sArrayList.get(i).getTitle());
        holder.image.setText(sArrayList.get(i).getMsg());


        return rowView;
    }
}

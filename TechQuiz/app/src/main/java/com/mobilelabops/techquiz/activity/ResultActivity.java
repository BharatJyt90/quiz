package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.Result;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 16/05/17.
 */

public class ResultActivity extends AppCompatActivity implements IServerCallBack{

    // UI Elements
    Button SubmitResult;
    LinearLayout resultLayout;

    private static final String LEVEL_INDEX = "level_id";
    private static final String SUBJECT_INDEX = "sub_id";
    private static final String SUB_LEVEL_INDEX = "sub_level_id";
    private static final String QUESTION_COUNT = "question_count";
    private static final String QUIZ_SCORE = "quiz_score";

    private int level_id = 0, sub_id = 0, sub_level_id = 0, total_ques = 0, quiz_score = 0;
    private static ArrayList<Quiz> ansList = new ArrayList<Quiz>();

    Result quiz_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_answers);

        Intent intent   = getIntent();
        sub_id          = intent.getIntExtra(SUBJECT_INDEX,0);
        level_id        = intent.getIntExtra(LEVEL_INDEX,0);
        sub_level_id    = intent.getIntExtra(SUB_LEVEL_INDEX,0);
        total_ques      = intent.getIntExtra(QUESTION_COUNT,0);
        quiz_score      = intent.getIntExtra(QUIZ_SCORE,0);

        quiz_result = new Result("userID", sub_id, level_id, sub_level_id, total_ques, quiz_score, System.currentTimeMillis());

        resultLayout = (LinearLayout)findViewById(R.id.resultLayout);
        SubmitResult = (Button) findViewById(R.id.btnScore);

        SubmitResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView tv = (TextView)findViewById(R.id.txtCorrectAnswer);
                tv.setText("" + quiz_score);

                tv = (TextView)findViewById(R.id.txtWrongAnswer);
                int wrongscore = total_ques - quiz_score;
                tv.setText("" + wrongscore);

                resultLayout.setVisibility(View.VISIBLE);

                submitQuizResults();
            }
        });
    }

    private void submitQuizResults(){

        JSONObject params = new JSONObject();
        try {

            params = quiz_result.getJsonObj();

            ServerRequest request = new ServerRequest(Constant.KEY_UPLOAD_QUIZ_RESULT,params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServeReqResult(ServerResponse result) {
        if(result != null) {

            if(result.getResponseCode() == 200){

                Log.i("response ","result = " + result.getResponseString());

                parseServerResponse(result.getResponseString());
            }
        }
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    try {
                        if (jsonObject.has("result")) {
                            String resultStr = jsonObject.getString("result");
                            Log.i("response ","result = " + resultStr);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_NO_RECORDS_FOUND",Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this,"ERROR_CODE_PASSWORD_MISMATCH",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this,"ERROR_USER_VERIFICATION_PENDING",Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

    }
}

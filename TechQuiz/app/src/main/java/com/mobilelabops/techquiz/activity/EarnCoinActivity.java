package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.EarnCoinGridAdapter;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.UploadCoinPurchaseData;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.VungleAdEventListener;
import com.vungle.publisher.VungleInitListener;
import com.vungle.publisher.VunglePub;

import org.json.JSONException;
import org.json.JSONObject;

public class EarnCoinActivity extends BaseActivity  implements VungleInitListener,VungleAdEventListener {

    private GridView mGridView;
 //   private RewardedVideoAd mRewardedVideoAd;
    private VunglePub vunglePub = VunglePub.getInstance();
    private AdConfig globalAdConfig;
    private boolean isPlaying = false;
    private String mPlacementReferenceId;
    private User mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn_coin);
        setTitle(getString(R.string.earn_coins));
        mUser = getIntent().getParcelableExtra(User.class.getSimpleName());
        vunglePub.init(this,getString(R.string.vungle_appId),new String[]{"DEFAULT04228"},this);
        vunglePub.clearAndSetEventListeners(this);
        // Initialize the Mobile Ads SDK.
       /* mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();*/

        mGridView = (GridView) findViewById(R.id.grid);

        mGridView.setAdapter(new EarnCoinGridAdapter(this));

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position){

                    case 0:

                        showRewardedVideo();

                        break;

                    case 1:

                        Intent earnCoinIntent = new Intent(EarnCoinActivity.this,PaymentGatewaySelectionActivity.class);
                        earnCoinIntent.putExtra(User.class.getSimpleName(),mUser);

                        startActivity(earnCoinIntent);

                        break;

                    case 3:
                        startActivity(new Intent(EarnCoinActivity.this, ReferralActivity.class));
                        break;

                    case 4:

                        startActivity(new Intent(EarnCoinActivity.this,UserFeedbackActivity.class));

                        break;

                    case 5:

                        startActivity(new Intent(EarnCoinActivity.this,UserSubmitQuizActivity.class));

                        break;
                }


            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();

     //   mRewardedVideoAd.pause(this);
        vunglePub.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        vunglePub.onResume();

       // mRewardedVideoAd.resume(this);
    }

    @Override
    public void onDestroy() {
    //    mRewardedVideoAd.destroy(this);
        super.onDestroy();
        vunglePub.clearEventListeners();

    }


    private void showRewardedVideo() {

     if(mPlacementReferenceId != null && vunglePub.isAdPlayable(mPlacementReferenceId))
            vunglePub.playAd(mPlacementReferenceId,globalAdConfig);
    }

    private void showToast(final String msg){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(EarnCoinActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onSuccess() {


        // get a reference to the global AdConfig object
        globalAdConfig = vunglePub.getGlobalAdConfig();
        // For a full description of available options, see the 'Config Object'section.
        globalAdConfig.setSoundEnabled(true);
        globalAdConfig.setBackButtonImmediatelyEnabled(false);

        showToast("Video init success");
    }

    @Override
    public void onFailure(Throwable throwable) {

    }

    @Override
    public void onAdEnd(String placementReferenceId, boolean wasSuccessfulView, boolean wasCallToActionClicked) {

        isPlaying = false;

        if(wasSuccessfulView){

            // reward the user
            PurchaseHistory purchaseHistory = new PurchaseHistory();
            purchaseHistory.setPurchaseType(Constant.COIN_PURCHASE_TYPE_VIDEO);
            purchaseHistory.setUserId(mUser.getUserId());


            UploadCoinPurchaseData.savePurchase(this, purchaseHistory, new IServerCallBack() {
                @Override
                public void onServeReqResult(ServerResponse result) {

                    if(result != null && result.getResponseCode() == 200){

                        try {
                            JSONObject jsonObject = new JSONObject(result.getResponseString());
                            int status = jsonObject.getInt(Constant.KEY_STATUS);

                            if(status == Constant.ERROR_CODE_SUCCESS){

                                int amount = jsonObject.getInt(PurchaseHistory.KEY_COINS_CREDIT);
                                mUser.setCoins(mUser.getCoins()+amount);

                                Intent intent = new Intent(EarnCoinActivity.this,VideoWatchSuccessActivity.class);
                                startActivity(intent);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    EarnCoinActivity.this.closeProgressDialog();

                }

                @Override
                public void onServerReqError(Exception e) {


                }

                @Override
                public void onServerReqStarted() {

                    EarnCoinActivity.this.showProgressDialog("Please Wait...");
                }
            });




        }else{

            showToast("You must watch the full video to get rewarded");
        }
    }

    @Override
    public void onAdStart(@NonNull String s) {

       showToast("Video Started");
        isPlaying = true;
    }

    @Override
    public void onUnableToPlayAd(@NonNull String s, String s1) {

    }

    @Override
    public void onAdAvailabilityUpdate(String placementReferenceId, boolean isAdAvailable)  {

        if(isAdAvailable){

       //     Toast.makeText(this, "Video available", Toast.LENGTH_SHORT).show();
            this.mPlacementReferenceId = placementReferenceId;

        }

    }
}

package com.mobilelabops.techquiz.model;
/**
Auto Generated using DbCodeGenerator.. 
Please use appropriate import statement and modify as required.
**/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LeaderboardQuizSubmit{

public final static String DATABASE_TABLE_NAME = "leaderboard_quiz_submit";

public static final String KEY_USER_ID = "user_id";
public static final String KEY_POINT_EARNED = "point_earned";
public static final String KEY_QUIZ_COUNT = "quiz_count";

public static final String[] COLUMNS = new String[]{KEY_USER_ID,KEY_POINT_EARNED,KEY_QUIZ_COUNT,};

private String mUserId;
private int mPointEarned;
private int mQuizCount;

public void setUserId(String userId){

this.mUserId = userId;

}

public String getUserId(){

return this.mUserId;

}

public void setPointEarned(int pointEarned){

this.mPointEarned = pointEarned;

}

public int getPointEarned(){

return this.mPointEarned;

}

public void setQuizCount(int quizCount){

this.mQuizCount = quizCount;

}

public int getQuizCount(){

return this.mQuizCount;

}



 public static String getCrateTableStatement() {

StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

createStatement.append(DATABASE_TABLE_NAME).append(" (");

createStatement.append(KEY_USER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_POINT_EARNED).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
createStatement.append(KEY_QUIZ_COUNT).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(")");

return createStatement.toString();
}

 public ContentValues getContentValues(){
ContentValues initialValues = new ContentValues();

initialValues.put(KEY_USER_ID,this.mUserId);
initialValues.put(KEY_POINT_EARNED,this.mPointEarned);
initialValues.put(KEY_QUIZ_COUNT,this.mQuizCount);
return initialValues;
}

 public static LeaderboardQuizSubmit createObjectFromCursor(Cursor cursor){
LeaderboardQuizSubmit obj = new LeaderboardQuizSubmit();
obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
obj.setPointEarned(cursor.getInt((cursor.getColumnIndex(KEY_POINT_EARNED))));
obj.setQuizCount(cursor.getInt((cursor.getColumnIndex(KEY_QUIZ_COUNT))));
return obj;
}
public JSONObject getJsonObj() throws JSONException {

JSONObject json = new JSONObject();
json.put(KEY_USER_ID, this.mUserId);
json.put(KEY_POINT_EARNED, this.mPointEarned);
json.put(KEY_QUIZ_COUNT, this.mQuizCount);

 return json;

}

public static LeaderboardQuizSubmit createObjectFromJson(JSONObject json) throws JSONException{

LeaderboardQuizSubmit obj = new LeaderboardQuizSubmit();
if(!json.isNull(KEY_USER_ID)){

obj.mUserId = json.getString(KEY_USER_ID);
}
if(!json.isNull(KEY_POINT_EARNED)){

obj.mPointEarned = json.getInt(KEY_POINT_EARNED);
}
if(!json.isNull(KEY_QUIZ_COUNT)){

obj.mQuizCount = json.getInt(KEY_QUIZ_COUNT);
}

 return obj;

}

public static JSONArray objListToJSONObjectArray(ArrayList<LeaderboardQuizSubmit> list) throws JSONException{

 JSONArray array = new JSONArray();
for(LeaderboardQuizSubmit obj : list){

 array.put(obj.getJsonObj());
}
 
 return array;
}
}

/**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class User implements Parcelable {

    public final static String DATABASE_TABLE_NAME = "user";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL_ID = "email_id";
    public static final String KEY_DOB = "dob";
    public static final String KEY_PROFILE_PIC = "profile_pic";
    public static final String KEY_REG_TYPE = "reg_type";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_COINS = "coins";
    public static final String KEY_PROMO = "promo";
    public static final String KEY_REG_DATE = "reg_date";
    public static final String KEY_SESSION = "session";
    public static final String KEY_SESSION_EXPIRE = "session_expire";
    public static final String KEY_VERFIED = "verfied";
    public static final String KEY_ADMIN = "admin";

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_NAME, KEY_EMAIL_ID, KEY_DOB, KEY_PROFILE_PIC, KEY_REG_TYPE, KEY_PASSWORD, KEY_COINS, KEY_PROMO, KEY_REG_DATE, KEY_SESSION, KEY_SESSION_EXPIRE, KEY_VERFIED, KEY_ADMIN,};

    private String mUserId;
    private String mName;
    private String mEmailId;
    private String mDob;
    private String mProfilePic;
    private int mRegType;
    private String mPassword;
    private int mCoins;
    private String mPromo;
    private long mRegDate;
    private String mSession;
    private long mSessionExpire;
    private int mVerfied;
    private int mAdmin;

    public User() {

    }

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setName(String name) {

        this.mName = name;

    }

    public String getName() {

        return this.mName;

    }

    public void setEmailId(String emailId) {

        this.mEmailId = emailId;

    }

    public String getEmailId() {

        return this.mEmailId;

    }

    public void setDob(String dob) {

        this.mDob = dob;

    }

    public String getDob() {

        return this.mDob;

    }

    public void setProfilePic(String profilePic) {

        this.mProfilePic = profilePic;

    }

    public String getProfilePic() {

        return this.mProfilePic;

    }

    public void setRegType(int regType) {

        this.mRegType = regType;

    }

    public int getRegType() {

        return this.mRegType;

    }

    public void setPassword(String password) {

        this.mPassword = password;

    }

    public String getPassword() {

        return this.mPassword;

    }

    public void setCoins(int coins) {

        this.mCoins = coins;

    }

    public int getCoins() {

        return this.mCoins;

    }

    public void setPromo(String promo) {

        this.mPromo = promo;

    }

    public String getPromo() {

        return this.mPromo;

    }

    public void setRegDate(long regDate) {

        this.mRegDate = regDate;

    }

    public long getRegDate() {

        return this.mRegDate;

    }

    public void setSession(String session) {

        this.mSession = session;

    }

    public String getSession() {

        return this.mSession;

    }

    public void setSessionExpire(long sessionExpire) {

        this.mSessionExpire = sessionExpire;

    }

    public long getSessionExpire() {

        return this.mSessionExpire;

    }

    public void setVerfied(int verfied) {

        this.mVerfied = verfied;

    }

    public int getVerfied() {

        return this.mVerfied;

    }

    public void setAdmin(int admin) {

        this.mAdmin = admin;

    }

    public int getAdmin() {

        return this.mAdmin;

    }

    public static String getCreateTableStatement() {

        StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

        createStatement.append(DATABASE_TABLE_NAME).append(" (");

        createStatement.append(KEY_USER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(DbConstants.PRIMARY_KEY).append(", ");
        createStatement.append(KEY_NAME).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_EMAIL_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_DOB).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_PROFILE_PIC).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_REG_TYPE).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_PASSWORD).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_COINS).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_PROMO).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_REG_DATE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
        createStatement.append(KEY_SESSION).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_SESSION_EXPIRE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
        createStatement.append(KEY_VERFIED).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_ADMIN).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(")");

        return createStatement.toString();
    }

    public ContentValues getContentValues() {
        ContentValues initialValues = new ContentValues();

        initialValues.put(KEY_USER_ID, this.mUserId);
        initialValues.put(KEY_NAME, this.mName);
        initialValues.put(KEY_EMAIL_ID, this.mEmailId);
        initialValues.put(KEY_DOB, this.mDob);
        initialValues.put(KEY_PROFILE_PIC, this.mProfilePic);
        initialValues.put(KEY_REG_TYPE, this.mRegType);
        initialValues.put(KEY_PASSWORD, this.mPassword);
        initialValues.put(KEY_COINS, this.mCoins);
        initialValues.put(KEY_PROMO, this.mPromo);
        initialValues.put(KEY_REG_DATE, this.mRegDate);
        initialValues.put(KEY_SESSION, this.mSession);
        initialValues.put(KEY_SESSION_EXPIRE, this.mSessionExpire);
        initialValues.put(KEY_VERFIED, this.mVerfied);
        initialValues.put(KEY_ADMIN, this.mAdmin);
        return initialValues;
    }

    public static User createObjectFromCursor(Cursor cursor) {

        User obj = new User();
        obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
        obj.setName(cursor.getString((cursor.getColumnIndex(KEY_NAME))));
        obj.setEmailId(cursor.getString((cursor.getColumnIndex(KEY_EMAIL_ID))));
        obj.setDob(cursor.getString((cursor.getColumnIndex(KEY_DOB))));
        obj.setProfilePic(cursor.getString((cursor.getColumnIndex(KEY_PROFILE_PIC))));
        obj.setRegType(cursor.getInt((cursor.getColumnIndex(KEY_REG_TYPE))));
        obj.setPassword(cursor.getString((cursor.getColumnIndex(KEY_PASSWORD))));
        obj.setCoins(cursor.getInt((cursor.getColumnIndex(KEY_COINS))));
        obj.setPromo(cursor.getString((cursor.getColumnIndex(KEY_PROMO))));
        obj.setRegDate(cursor.getLong((cursor.getColumnIndex(KEY_REG_DATE))));
        obj.setSession(cursor.getString((cursor.getColumnIndex(KEY_SESSION))));
        obj.setSessionExpire(cursor.getLong((cursor.getColumnIndex(KEY_SESSION_EXPIRE))));
        obj.setVerfied(cursor.getInt((cursor.getColumnIndex(KEY_VERFIED))));
        obj.setAdmin(cursor.getInt((cursor.getColumnIndex(KEY_ADMIN))));
        return obj;
    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_NAME, this.mName);
        json.put(KEY_EMAIL_ID, this.mEmailId);
        json.put(KEY_DOB, this.mDob);
        json.put(KEY_PROFILE_PIC, this.mProfilePic);
        json.put(KEY_REG_TYPE, this.mRegType);
        json.put(KEY_PASSWORD, this.mPassword);
        json.put(KEY_COINS, this.mCoins);
        json.put(KEY_PROMO, this.mPromo);
        json.put(KEY_REG_DATE, this.mRegDate);
        json.put(KEY_SESSION, this.mSession);
        json.put(KEY_SESSION_EXPIRE, this.mSessionExpire);
        json.put(KEY_VERFIED, this.mVerfied);
        json.put(KEY_ADMIN, this.mAdmin);

        return json;

    }

    public static User createObjectFromJson(JSONObject json) throws JSONException {

        User obj = new User();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_NAME)) {

            obj.mName = json.getString(KEY_NAME);
        }
        if (!json.isNull(KEY_EMAIL_ID)) {

            obj.mEmailId = json.getString(KEY_EMAIL_ID);
        }
        if (!json.isNull(KEY_DOB)) {

            obj.mDob = json.getString(KEY_DOB);
        }
        if (!json.isNull(KEY_PROFILE_PIC)) {

            obj.mProfilePic = json.getString(KEY_PROFILE_PIC);
        }
        if (!json.isNull(KEY_REG_TYPE)) {

            obj.mRegType = json.getInt(KEY_REG_TYPE);
        }
        if (!json.isNull(KEY_PASSWORD)) {

            obj.mPassword = json.getString(KEY_PASSWORD);
        }
        if (!json.isNull(KEY_COINS)) {

            obj.mCoins = json.getInt(KEY_COINS);
        }
        if (!json.isNull(KEY_PROMO)) {

            obj.mPromo = json.getString(KEY_PROMO);
        }
        if (!json.isNull(KEY_REG_DATE)) {

            obj.mRegDate = json.getLong(KEY_REG_DATE);
        }
        if (!json.isNull(KEY_SESSION)) {

            obj.mSession = json.getString(KEY_SESSION);
        }
        if (!json.isNull(KEY_SESSION_EXPIRE)) {

            obj.mSessionExpire = json.getLong(KEY_SESSION_EXPIRE);
        }
        if (!json.isNull(KEY_VERFIED)) {

            obj.mVerfied = json.getBoolean(KEY_VERFIED) ? 1 : 0;
        }
        if (!json.isNull(KEY_ADMIN)) {

            obj.mAdmin = json.getInt(KEY_ADMIN);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<User> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (User obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }

    private User(Parcel in) {

        mUserId = in.readString();
        mName = in.readString();
        mEmailId = in.readString();
        mDob = in.readString();
        mProfilePic = in.readString();
        mRegType = in.readInt();
        mPassword = in.readString();
        mCoins = in.readInt();
        mPromo = in.readString();
        mRegDate = in.readLong();
        mSession = in.readString();
        mSessionExpire = in.readLong();
        mVerfied = in.readInt();

    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(mUserId);
        out.writeString(mName);
        out.writeString(mEmailId);
        out.writeString(mDob);
        out.writeString(mProfilePic);
        out.writeInt(mRegType);
        out.writeString(mPassword);
        out.writeInt(mCoins);
        out.writeString(mPromo);
        out.writeLong(mRegDate);
        out.writeString(mSession);
        out.writeLong(mSessionExpire);
        out.writeInt(mVerfied);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {

        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int describeContents() {
        return 0;
    }

}

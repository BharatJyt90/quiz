package com.mobilelabops.techquiz.activity;

import android.os.Bundle;
import android.view.View;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomTextView;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;


public class UserSubmitQuizActivity extends BaseActivity implements IServerCallBack{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_submit_quiz);

        setTitle(getString(R.string.create_quiz_token));
    }
    public void onClick(View v) {
        ServerAsynTask asynTask = new ServerAsynTask(this);
        ServerRequest request = new ServerRequest(Constant.KEY_GENERATE_TOKEN,null, Util.getJsonContentTypeHeader(this));
        asynTask.execute(request);
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        CustomTextView token = (CustomTextView) findViewById(R.id.tokenTxt);

        closeProgressDialog();

        if(result != null){

            if(result.getResponseCode() == 200){

                token.setText(R.string.token_msg);

                return;

            }


        }

        token.setText(R.string.token_msg_err);

    }

    @Override
    public void onServerReqError(Exception e) {

        closeProgressDialog();
    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog(getString(R.string.gen_token));
    }
}

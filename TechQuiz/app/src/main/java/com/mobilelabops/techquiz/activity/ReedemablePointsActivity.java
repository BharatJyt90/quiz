package com.mobilelabops.techquiz.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.RedeemablePoints;
import com.mobilelabops.techquiz.ui.CustomTextView;

public class ReedemablePointsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reedemable_points);

        CustomTextView pointsCount = (CustomTextView) findViewById(R.id.points_count);

        int pointsAvailable = getIntent().getIntExtra(RedeemablePoints.KEY_TOTAL_AVAILABLE,0);

        pointsCount.setText(pointsAvailable +"\nPoints");
    }

    public void onContinueClick(View view){

        RadioGroup paymentGroup = (RadioGroup) findViewById(R.id.paymentGroup);

        int radioBtn = paymentGroup.getCheckedRadioButtonId();

        //RedeemablePoints.KEY_TOTAL_AVAILABLE equiv_amnt
        //walletType

        Intent userIntent = new Intent(this,RedeemSummaryActivity.class);
        userIntent.putExtra(RedeemablePoints.KEY_TOTAL_AVAILABLE, getIntent().getIntExtra(RedeemablePoints.KEY_TOTAL_AVAILABLE,0));
        userIntent.putExtra("equiv_amnt", getIntent().getDoubleExtra("equiv_amnt",0));
        userIntent.putExtra("walletType", radioBtn);

        startActivity(userIntent);
    }
}

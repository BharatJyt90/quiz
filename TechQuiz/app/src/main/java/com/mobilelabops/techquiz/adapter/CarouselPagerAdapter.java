package com.mobilelabops.techquiz.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.activity.LevelFragment;
import com.mobilelabops.techquiz.activity.LevelActivity;
import com.mobilelabops.techquiz.ui.CarouselLinearLayout;

public class CarouselPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.7f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    private LevelActivity context;
    private FragmentManager fragmentManager;
    private float scale;
    private int sub_id = 0;
    private String sub_name="";

    public CarouselPagerAdapter(LevelActivity context, FragmentManager fm, int sub_id, String sub_name) {
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
        this.sub_id = sub_id;
        this.sub_name = sub_name;
    }

    @Override
    public Fragment getItem(int position) {
        // make the first pager bigger than others
        try {
            if (position == LevelActivity.FIRST_PAGE)
                scale = BIG_SCALE;
            else
                scale = SMALL_SCALE;

            position = position % LevelActivity.count;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return LevelFragment.newInstance(context, position, scale, sub_id, sub_name);
    }

    @Override
    public int getCount() {
        int count = 0;
        try {
            count = LevelActivity.count * LevelActivity.LOOPS;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        try {
            if (positionOffset >= 0f && positionOffset <= 1f) {
                CarouselLinearLayout cur = getRootView(position);
                CarouselLinearLayout next = getRootView(position + 1);

                if(position != 0) {
                    CarouselLinearLayout prev = getRootView(position - 1);
                    prev.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);
                }

                cur.setScaleBoth(BIG_SCALE - DIFF_SCALE * positionOffset);
                next.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @SuppressWarnings("ConstantConditions")
    private CarouselLinearLayout getRootView(int position) {
        return (CarouselLinearLayout) fragmentManager.findFragmentByTag(this.getFragmentTag(position))
                .getView().findViewById(R.id.root_container);
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + context.pager.getId() + ":" + position;
    }
}
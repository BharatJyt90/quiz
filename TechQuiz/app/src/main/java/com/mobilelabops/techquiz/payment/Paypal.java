package com.mobilelabops.techquiz.payment;

import android.content.Context;
import android.content.Intent;

import com.mobilelabops.techquiz.R;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;

import java.math.BigDecimal;

/**
 * Created by bhazarix on 11/21/2017.
 */

public class Paypal {


    public static final int AMOUNT = 2;// USD
    // private static final String TAG = "paymentdemoblog";
    /**
     * - Set to PaymentActivity.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PaymentActivity.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    // private static final String CONFIG_ENVIRONMENT =
    // PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    //private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox
    // environments.

    public static final int REQUEST_CODE_PAYMENT = 100;
    public static final int REQUEST_CODE_FUTURE_PAYMENT = 200;

    PayPalConfiguration config;

    public void startPaypalService(Context context){

        config = new PayPalConfiguration()
                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(context.getString(R.string.paypal_CLIENT_ID));

        Intent intent = new Intent(context, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        context.startService(intent);
    }

    public static void stopPayPalService(Context context){

        context.stopService(new Intent(context, PayPalService.class));
    }

    public Intent getPayPalActivityIntent(Context context, int amount){

        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "USD", "TechQuiz Digital Coins",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intentPaypal = new Intent(context, PaymentActivity.class);

        //putting the paypal configuration to the intent
        intentPaypal.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intentPaypal.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        return intentPaypal;

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
       // context.startActivityForResult(intentPaypal, PAYPAL_REQUEST_CODE);
    }
}

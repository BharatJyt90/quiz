package com.mobilelabops.techquiz.activity;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.RedeemablePoints;
import com.mobilelabops.techquiz.model.ReedemRequest;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class RedeemSummaryActivity extends BaseActivity implements IServerCallBack{

    CustomEditText mPointTextView,mEquivalentTextView,mWalletTextView,mWalletAddress,mConfirmAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_summary);

        int pointsAvailable = getIntent().getIntExtra(RedeemablePoints.KEY_TOTAL_AVAILABLE,0);
        double pointAmnt = getIntent().getDoubleExtra("equiv_amnt",0);

        mPointTextView = (CustomEditText) findViewById(R.id.points);
        mPointTextView.setText(String.valueOf(pointsAvailable));

        mEquivalentTextView = (CustomEditText) findViewById(R.id.equiv_amount);
        mEquivalentTextView.setText(String.valueOf(Math.floor(pointAmnt * pointsAvailable)));

        mWalletTextView = (CustomEditText) findViewById(R.id.wallet);
        mWalletAddress = (CustomEditText) findViewById(R.id.address);
        mConfirmAddress = (CustomEditText) findViewById(R.id.confirm_address);

        int walletType = getIntent().getIntExtra("walletType",-1);

        switch (walletType){

            case R.id.payumoney:

                mWalletTextView.setText("PayuMoney");
                mWalletAddress.setInputType(InputType.TYPE_CLASS_PHONE);
                mConfirmAddress.setInputType(InputType.TYPE_CLASS_PHONE);
                setMobileNum();
                break;

            case R.id.paypal:

                mWalletTextView.setText("PayPal");
                mWalletAddress.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                mConfirmAddress.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE);
                String email = preferences.getString(User.KEY_EMAIL_ID,null);
                mWalletAddress.setText(email);
                mConfirmAddress.setText(email);


                break;

            case R.id.paytm:

                mWalletTextView.setText("Paytm");
                mWalletAddress.setInputType(InputType.TYPE_CLASS_PHONE);
                mConfirmAddress.setInputType(InputType.TYPE_CLASS_PHONE);
                setMobileNum();

            break;


            case R.id.tech_quiz_point:

                mWalletTextView.setText("TechQuiz Points");

                mWalletAddress.setVisibility(View.GONE);
                mConfirmAddress.setVisibility(View.GONE);

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay!
            int status = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (status == PackageManager.PERMISSION_GRANTED) {

                TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                String mPhoneNumber = tMgr.getLine1Number();
                mWalletAddress.setText(mPhoneNumber);
                mConfirmAddress.setText(mPhoneNumber);
            }
        }
    }

    private void setMobileNum(){

        int status = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (status != PackageManager.PERMISSION_GRANTED) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        1);
        }else{

            TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();

            mWalletAddress.setText(mPhoneNumber);
            mConfirmAddress.setText(mPhoneNumber);
        }
    }

    public void proceed(View v){

        String pointsEnteredStr = mPointTextView.getText().toString();

        int pointsEntered = Integer.parseInt(pointsEnteredStr);

        int pointsAvailable = getIntent().getIntExtra(RedeemablePoints.KEY_TOTAL_AVAILABLE,0);

        if(pointsEntered > pointsAvailable){

            Toast.makeText(this,"Sorry!! You do not have sufficient points. Available points "+pointsAvailable,Toast.LENGTH_SHORT).show();

            return;
        }

        if(mWalletAddress.getVisibility() == View.VISIBLE){

            if(mWalletAddress.getText().toString().equals(mConfirmAddress.getText().toString()) == false){

                mConfirmAddress.setError("Wallet address not matching");

                return;
            }
        }

        ReedemRequest reedemRequest = new ReedemRequest();

        reedemRequest.setPaymentPartner(mWalletTextView.getText().toString());
        reedemRequest.setPointsCount(pointsEntered);
        reedemRequest.setWalletAddress(mWalletAddress.getText().toString());
        reedemRequest.setProposedCashbackAmount(Double.valueOf(mEquivalentTextView.getText().toString()));


        try {

            ServerRequest request = new ServerRequest(Constant.KEY_REQUEST_REDEEM,reedemRequest.getJsonObj().toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);

            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {

            if(result.getResponseCode() == 200){

                Log.i("response ","result = " + result.getResponseString());

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result.getResponseString());
                    int status = jsonObject.getInt(Constant.KEY_STATUS);

                    if(status == Constant.ERROR_CODE_SUCCESS){

                        //TODO show dialog

                        int pointsAvailable = jsonObject.getInt(RedeemablePoints.KEY_TOTAL_AVAILABLE);

                        mPointTextView.setText(String.valueOf(pointsAvailable));

                        closeProgressDialog();

                        Toast.makeText(this,"Reedem request has been created successfully. Please check your email id",Toast.LENGTH_LONG).show();

                        return;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }



                return;
            }
        }

        onServerReqError(null);
    }

    @Override
    public void onServerReqError(Exception e) {

        closeProgressDialog();

        Toast.makeText(this,"Sorry!!Could not create request. Please try again...",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog("Creating request...");
    }
}

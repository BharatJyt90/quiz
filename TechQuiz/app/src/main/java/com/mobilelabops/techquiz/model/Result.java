/**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 * */
package com.mobilelabops.techquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Result implements Parcelable {

    public final static String DATABASE_TABLE_NAME = "result";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_SUB_ID = "sub_id";
    public static final String KEY_LEVEL_ID = "level_id";
    public static final String KEY_SUB_LEVEL_ID = "sub_level_id";
    public static final String KEY_QUESTION_ATTEMPETED = "question_attempeted";
    public static final String KEY_CORRECT_ANS_COUNT = "correct_ans_count";
    public static final String KEY_UPDATE_TIME = "update_time";

    public static final String[] COLOUMNS = new String[]{KEY_USER_ID, KEY_SUB_ID, KEY_LEVEL_ID, KEY_SUB_LEVEL_ID, KEY_QUESTION_ATTEMPETED, KEY_CORRECT_ANS_COUNT, KEY_UPDATE_TIME,};

    private String mUserId;
    private int mSubId;
    private int mLevelId;
    private int mSubLevelId;
    private int mQuestionAttempeted;
    private int mCorrectAnsCount;
    private long mUpdateTime;

    public Result(){

    }

    public Result(String mUserId, int mSubId, int mLevelId, int mSubLevelId, int mQuestionAttempeted, int mCorrectAnsCount, long mUpdateTime) {
        this.mUserId = mUserId;
        this.mSubId = mSubId;
        this.mLevelId = mLevelId;
        this.mSubLevelId = mSubLevelId;
        this.mQuestionAttempeted = mQuestionAttempeted;
        this.mCorrectAnsCount = mCorrectAnsCount;
        this.mUpdateTime = mUpdateTime;
    }

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setSubId(int subId) {

        this.mSubId = subId;

    }

    public int getSubId() {

        return this.mSubId;

    }

    public void setLevelId(int levelId) {

        this.mLevelId = levelId;

    }

    public int getLevelId() {

        return this.mLevelId;

    }

    public void setSubLevelId(int subLevelId) {

        this.mSubLevelId = subLevelId;

    }

    public int getSubLevelId() {

        return this.mSubLevelId;

    }

    public void setQuestionAttempeted(int questionAttempeted) {

        this.mQuestionAttempeted = questionAttempeted;

    }

    public int getQuestionAttempeted() {

        return this.mQuestionAttempeted;

    }

    public void setCorrectAnsCount(int correctAnsCount) {

        this.mCorrectAnsCount = correctAnsCount;

    }

    public int getCorrectAnsCount() {

        return this.mCorrectAnsCount;

    }

    public void setUpdateTime(long updateTime) {

        this.mUpdateTime = updateTime;

    }

    public long getUpdateTime() {

        return this.mUpdateTime;

    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_SUB_ID, this.mSubId);
        json.put(KEY_LEVEL_ID, this.mLevelId);
        json.put(KEY_SUB_LEVEL_ID, this.mSubLevelId);
        json.put(KEY_QUESTION_ATTEMPETED, this.mQuestionAttempeted);
        json.put(KEY_CORRECT_ANS_COUNT, this.mCorrectAnsCount);
        json.put(KEY_UPDATE_TIME, this.mUpdateTime);

        return json;

    }

    public static Result createObjectFromJson(JSONObject json) throws JSONException {

        Result obj = new Result();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_SUB_ID)) {

            obj.mSubId = json.getInt(KEY_SUB_ID);
        }
        if (!json.isNull(KEY_LEVEL_ID)) {

            obj.mLevelId = json.getInt(KEY_LEVEL_ID);
        }
        if (!json.isNull(KEY_SUB_LEVEL_ID)) {

            obj.mSubLevelId = json.getInt(KEY_SUB_LEVEL_ID);
        }
        if (!json.isNull(KEY_QUESTION_ATTEMPETED)) {

            obj.mQuestionAttempeted = json.getInt(KEY_QUESTION_ATTEMPETED);
        }
        if (!json.isNull(KEY_CORRECT_ANS_COUNT)) {

            obj.mCorrectAnsCount = json.getInt(KEY_CORRECT_ANS_COUNT);
        }
        if (!json.isNull(KEY_UPDATE_TIME)) {

            obj.mUpdateTime = json.getLong(KEY_UPDATE_TIME);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<Result> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (Result obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }

    private Result(Parcel in) {

        mUserId = in.readString();
        mSubId = in.readInt();
        mLevelId = in.readInt();
        mSubLevelId = in.readInt();
        mQuestionAttempeted = in.readInt();
        mCorrectAnsCount = in.readInt();
        mUpdateTime = in.readLong();

    }
    public void writeToParcel(Parcel out, int flags) {

        out.writeString(mUserId);
        out.writeInt(mSubId);
        out.writeInt(mLevelId);
        out.writeInt(mSubLevelId);
        out.writeInt(mQuestionAttempeted);
        out.writeInt(mCorrectAnsCount);
        out.writeLong(mUpdateTime);
    }
    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result> () {

        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public int describeContents() {
        return 0;
    }

}

package com.mobilelabops.techquiz.model; /**
Auto Generated using DbCodeGenerator.. 
Please use appropriate import statement and modify as required.
**/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
public class AdminEarning{

public final static String DATABASE_TABLE_NAME = "admin_earning";

public static final String KEY_CREDIT_POINTS = "credit_points";
public static final String KEY_DATE = "date";
public static final String KEY_DEBITED_USER = "debited_user";

public static final String[] COLUMNS = new String[]{KEY_CREDIT_POINTS,KEY_DATE,KEY_DEBITED_USER,};

private int mCreditPoints;
private long mDate;
private String mDebitedUser;

public void setCreditPoints(int creditPoints){

this.mCreditPoints = creditPoints;

}

public int getCreditPoints(){

return this.mCreditPoints;

}

public void setDate(long date){

this.mDate = date;

}

public long getDate(){

return this.mDate;

}

public void setDebitedUser(String debitedUser){

this.mDebitedUser = debitedUser;

}

public String getDebitedUser(){

return this.mDebitedUser;

}



 public static String getCrateTableStatement() {

StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

createStatement.append(DATABASE_TABLE_NAME).append(" (");

createStatement.append(KEY_CREDIT_POINTS).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
createStatement.append(KEY_DATE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
createStatement.append(KEY_DEBITED_USER).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(")");

return createStatement.toString();
}

 public ContentValues getContentValues(){
ContentValues initialValues = new ContentValues();

initialValues.put(KEY_CREDIT_POINTS,this.mCreditPoints);
initialValues.put(KEY_DATE,this.mDate);
initialValues.put(KEY_DEBITED_USER,this.mDebitedUser);
return initialValues;
}

 public static AdminEarning createObjectFromCursor(Cursor cursor){
AdminEarning obj = new AdminEarning();
obj.setCreditPoints(cursor.getInt((cursor.getColumnIndex(KEY_CREDIT_POINTS))));
obj.setDate(cursor.getLong((cursor.getColumnIndex(KEY_DATE))));
obj.setDebitedUser(cursor.getString((cursor.getColumnIndex(KEY_DEBITED_USER))));
return obj;
}
public JSONObject getJsonObj() throws JSONException {

JSONObject json = new JSONObject();
json.put(KEY_CREDIT_POINTS, this.mCreditPoints);
json.put(KEY_DATE, this.mDate);
json.put(KEY_DEBITED_USER, this.mDebitedUser);

 return json;

}

public static AdminEarning createObjectFromJson(JSONObject json) throws JSONException{

AdminEarning obj = new AdminEarning();
if(!json.isNull(KEY_CREDIT_POINTS)){

obj.mCreditPoints = json.getInt(KEY_CREDIT_POINTS);
}
if(!json.isNull(KEY_DATE)){

obj.mDate = json.getLong(KEY_DATE);
}
if(!json.isNull(KEY_DEBITED_USER)){

obj.mDebitedUser = json.getString(KEY_DEBITED_USER);
}

 return obj;

}

public static JSONArray objListToJSONObjectArray(ArrayList<AdminEarning> list) throws JSONException{

 JSONArray array = new JSONArray();
for(AdminEarning obj : list){

 array.put(obj.getJsonObj());
}
 
 return array;
}
}

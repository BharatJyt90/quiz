package com.mobilelabops.techquiz.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;

/**
 * Created by bhazarix on 10/18/2017.
 */

public class EarnCoinGridBox extends LinearLayout {

    public EarnCoinGridBox(Context context) {
        super(context);
        init(context,null);
    }

    public EarnCoinGridBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public EarnCoinGridBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public EarnCoinGridBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs){

        if (attrs != null) {

            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EarnCoinGridBox);
            Drawable imgSrc = a.getDrawable(R.styleable.EarnCoinGridBox_earnCoinImg);

            View view = inflate(getContext(), R.layout.grid_box_earn_coin, null);

            if(imgSrc != null){

                ImageView imageIcon = view.findViewById(R.id.imageIcon);
                imageIcon.setImageDrawable(imgSrc);

            }

            String title = a.getString(R.styleable.EarnCoinGridBox_earnCoinText);

            if(title != null){

                TextView textView = (TextView) view.findViewById(R.id.title);
                textView.setText(title);
            }

            String pointsCount = a.getString(R.styleable.EarnCoinGridBox_coinCount);

            if(pointsCount != null){

                TextView textView = (TextView) view.findViewById(R.id.pointsCount);
                textView.setText(pointsCount);
            }

            addView(view);

        }

    }
}

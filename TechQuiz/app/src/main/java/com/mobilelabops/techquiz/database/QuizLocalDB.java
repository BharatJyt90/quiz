package com.mobilelabops.techquiz.database;

/**
 * Created by sumanranjan on 14/11/17.
 */

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.Subject;
import com.mobilelabops.techquiz.model.User;

import java.util.ArrayList;

public class QuizLocalDB {
    public static final String TAG = "LocalDB";

    protected static QuizLocalDB sSingleton = null;
    protected static SubjectDbAdapter subjectDbAdapter = null;
    protected Context context = null;


    public static void init(Context context) {
        sSingleton = new QuizLocalDB(context);
    }

    protected QuizLocalDB(Context inContext) {
        context = inContext;

        SubjectDbAdapter.open(context);
        subjectDbAdapter = new SubjectDbAdapter();

        // Do work to refresh the list here.
        new PupulateDBTask().execute();
    }

    private class PupulateDBTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            // Simulates a background job.
            getSubjects();
            getUserUpdates();
            return null;

        }
    }
    ArrayList<Subject> subList = new ArrayList<Subject>();

    protected ArrayList<Subject> getSubjects() {

        // Try to get data from DB
        Cursor cursor = subjectDbAdapter.query(Subject.DATABASE_TABLE_NAME, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Subject subject = Subject.createObjectFromCursor(cursor);
                subList.add(subject);
            }
        }

        return subList;
    }

    protected void deleteSubjects(){
        subjectDbAdapter.delete("null");
    }

    protected void getUserUpdates(){

    }

    public static ArrayList<Subject> getAllSubjects(){
        return sSingleton.getSubjects();
    }
    public static void deleteSubjectTable(){
         sSingleton.deleteSubjects();
    }


    // Public Methods
    public void addSubject(Subject subject){
        subjectDbAdapter.insertData(subject);
    }

    public void addUser(User user){

    }

    public static void addSingleSubject(Subject subject){
        sSingleton.addSubject(subject);
    }

    public void addQuiz(Quiz quiz){

    }

}

package com.mobilelabops.techquiz.activity;

import android.graphics.drawable.Animatable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.ui.CustomTextView;

public class VideoWatchSuccessActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_watch_success);

        ((CustomTextView) findViewById(R.id.message)).setText(getString(R.string.vdo_success));

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        ((Animatable) imageView.getDrawable()).start();
    }
}

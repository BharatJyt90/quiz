package com.mobilelabops.techquiz.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.database.PurchaseHistoryDbAdapter;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.payment.GooglePayment;
import com.mobilelabops.techquiz.payment.PayUMoney;
import com.mobilelabops.techquiz.payment.Paypal;
import com.mobilelabops.techquiz.payment.googleHelper.IabHelper;
import com.mobilelabops.techquiz.payment.googleHelper.IabResult;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.UploadCoinPurchaseData;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;

import org.json.JSONException;
import org.json.JSONObject;


public class PaymentGatewaySelectionActivity extends BaseActivity {

    private static final String TAG = "PaymentActivity";

    public static final int GATEWAY_GOOGLE = 0, GATEWAY_PAYU = 1, GATEWAY_PAYPAL = 2;

    private User mUser;

    public static final boolean DEBUG = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_gateway_selection);
        setTitle(getString(R.string.payment));

        mUser = getIntent().getParcelableExtra(User.class.getSimpleName());
    }

    public void buy(View v){

        RadioGroup paymentGroup = (RadioGroup) findViewById(R.id.paymentGroup);

        int radioBtn = paymentGroup.getCheckedRadioButtonId();

        switch (radioBtn){

            case R.id.payumoney:

                if(DEBUG) {
                    SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
                    String email = preferences.getString(User.KEY_EMAIL_ID, null);

                    com.payumoney.core.PayUmoneySdkInitializer.PaymentParam paymentParam = PayUMoney.getParams(PayUMoney.AMOUNT, email, mUser.getName());

                    PayUmoneyFlowManager.startPayUMoneyFlow(
                            paymentParam, this, R.style.payuAppTheme_Green, false);
                }
                else {

                    Intent earnCoinIntent = new Intent(this,PayUMobileNumberActivity.class);
                    earnCoinIntent.putExtra(User.class.getSimpleName(),mUser);

                    startActivityForResult(earnCoinIntent,101);
                }
                break;

            case R.id.paypal:

                Paypal paypal = new Paypal();
                paypal.startPaypalService(this);

                Intent intentPaypal = paypal.getPayPalActivityIntent(this,Paypal.AMOUNT);
                startActivityForResult(intentPaypal, Paypal.REQUEST_CODE_PAYMENT);

                break;

            case R.id.g_play:

                final GooglePayment googlePayment = new GooglePayment();
                googlePayment.init(this, new IabHelper.OnIabSetupFinishedListener() {
                    @Override
                    public void onIabSetupFinished(IabResult result) {

                        Log.d(TAG, "Setup finished.");

                        if (result.isSuccess()) {

                            Log.d(TAG," setting up in-app billing: " + result);
                            try {
                                googlePayment.purchase(PaymentGatewaySelectionActivity.this);
                            } catch (IabHelper.IabAsyncInProgressException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

                break;
        }
    }

    public void navigateToSuccessPage(String orderId){

        Intent intent = new Intent(this,PaymentSuccessActivity.class);
        intent.putExtra("oderId",orderId);
        startActivity(intent);

    }

    private void savePaymentInfoToDb( PurchaseHistory purchaseHistory){

        PurchaseHistoryDbAdapter dbAdapter = new PurchaseHistoryDbAdapter();
        long ret = dbAdapter.insertData(purchaseHistory);

        if(ret > 0){

            Toast.makeText(this,"Saved data for Network Availability",Toast.LENGTH_SHORT).show();
        }
    }

    public void saveOrder(final String orderId, int gateWayType, int amount){

        final PurchaseHistory purchaseHistory = new PurchaseHistory();
        purchaseHistory.setPurchaseAmount(amount);
        purchaseHistory.setPurchaseType(Constant.COIN_PURCHASE_TYPE_MONEY);
        purchaseHistory.setGateway(gateWayType);
        purchaseHistory.setUserId(mUser.getUserId());
        purchaseHistory.setOrderId(orderId);

        UploadCoinPurchaseData.savePurchase(this, purchaseHistory, new IServerCallBack() {
            @Override
            public void onServeReqResult(ServerResponse result) {

                if(result != null && result.getResponseCode() == 200){

                    try {
                        JSONObject jsonObject = new JSONObject(result.getResponseString());
                        int status = jsonObject.getInt(Constant.KEY_STATUS);

                        if(status == Constant.ERROR_CODE_SUCCESS){

                            int amount = jsonObject.getInt(PurchaseHistory.KEY_COINS_CREDIT);
                            mUser.setCoins(mUser.getCoins()+amount);

                        }else{

                            savePaymentInfoToDb(purchaseHistory);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{

                    savePaymentInfoToDb(purchaseHistory);
                }

                PaymentGatewaySelectionActivity.this.closeProgressDialog();
                navigateToSuccessPage(orderId);
            }

            @Override
            public void onServerReqError(Exception e) {


            }

            @Override
            public void onServerReqStarted() {

                PaymentGatewaySelectionActivity.this.showProgressDialog("Please Wait...");
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Paypal.REQUEST_CODE_PAYMENT) {

            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(com.paypal.android.sdk.payments.PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {

                    try {

                        String paymentDetails = confirm.toJSONObject().toString(4);
                        JSONObject jsonDetails = new JSONObject(paymentDetails);

                        String transactionId = jsonDetails.getString("id");

                        Toast.makeText(getApplicationContext(), "Order placed",
                                Toast.LENGTH_LONG).show();

                        saveOrder(transactionId,GATEWAY_PAYPAL,Paypal.AMOUNT);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

                Toast.makeText(getApplicationContext(), getString(R.string.payment_cancel),
                        Toast.LENGTH_LONG).show();

            } else if (resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {

                Toast.makeText(getApplicationContext(), getString(R.string.payment_invalid),
                        Toast.LENGTH_LONG).show();

            }
        }

        else if (requestCode == 101 ) {

            if (resultCode == RESULT_OK && data != null) {
                TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                        .INTENT_EXTRA_TRANSACTION_RESPONSE);

                ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

                // Check which object is non-null
                if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                    if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                        //Success Transaction

                        String merchantResponse = transactionResponse.getTransactionDetails();

                        saveOrder(merchantResponse,GATEWAY_PAYU,PayUMoney.AMOUNT);

                    } else {
                        //Failure Transaction
                    }

                } else if (resultModel != null && resultModel.getError() != null) {
                    Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
                } else {
                    Log.d(TAG, "Both objects are null!");
                }
            }else{

                Toast.makeText(getApplicationContext(), getString(R.string.payment_cancel),
                        Toast.LENGTH_LONG).show();
            }


        }
        else if(requestCode ==  GooglePayment.REQUEST_PAYMENT){

            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    String orderId = jo.getString("orderId");

                    Toast.makeText(getApplicationContext(), sku+", Order id: "+orderId,
                            Toast.LENGTH_LONG).show();

                    saveOrder(orderId,GATEWAY_GOOGLE,GooglePayment.AMOUNT);
                }
                catch (JSONException e) {
                    Log.e(TAG,"Failed to parse purchase data.");
                    e.printStackTrace();
                }
            }
            else{

                Toast.makeText(getApplicationContext(), getString(R.string.payment_failed),
                        Toast.LENGTH_LONG).show();
            }

        }
    }
}

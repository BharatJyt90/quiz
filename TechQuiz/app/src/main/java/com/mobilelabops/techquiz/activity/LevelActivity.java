package com.mobilelabops.techquiz.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.CarouselPagerAdapter;
import com.mobilelabops.techquiz.ui.CustomTextView;

/**
 * Created by sumanranjan on 03/09/17.
 */

public class LevelActivity extends BaseActivity {
    public final static int LOOPS = 1000;
    public static int count = 3;
    public static int FIRST_PAGE = 0;
    public ViewPager pager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        int sub_id  = getIntent().getIntExtra(MainActivity.SUB_ID_INDEX, 0) ;
        String sub_name = getIntent().getStringExtra(MainActivity.SUB_NAME) ;

        // UI Elements
        CarouselPagerAdapter adapter;

        CustomTextView title = (CustomTextView) findViewById(R.id.toolbar_title);
        title.setText(getText(R.string.level_title));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pager = (ViewPager) findViewById(R.id.myviewpager);

        //set page margin between pages for viewpager
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int pageMargin = ((metrics.widthPixels / 4) + 100);
        pager.setPageMargin(-pageMargin);

        adapter = new CarouselPagerAdapter(this, getSupportFragmentManager(), sub_id, sub_name);
        pager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        pager.addOnPageChangeListener(adapter);

        // Set current item to the middle page so we can fling to both
        // directions left and right
        pager.setCurrentItem(1);
       // pager.setCurrentItem(0);
        pager.setOffscreenPageLimit(3);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

package com.mobilelabops.techquiz.model; /**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/


import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class Feedback implements Parcelable {

    public final static String DATABASE_TABLE_NAME = "feedback";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_FEEDBACK_TYPE = "feedback_type";
    public static final String KEY_FEEDBACK = "feedback";
    public static final String KEY_IMAGE_PATH = "image_path";
    public static final String KEY_TIME = "time";
    public static final String KEY_APPROVED = "approved";
    public static final String KEY_REVIEWED = "reviewed";
    public static final String KEY_ID = "id";

    public Feedback(){

    }

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_FEEDBACK_TYPE, KEY_FEEDBACK, KEY_IMAGE_PATH, KEY_TIME, KEY_APPROVED, KEY_REVIEWED, KEY_ID,
    };

    private String mUserId;
    private int mFeedbackType;
    private String mFeedback;
    private String mImagePath;
    private long mTime;
    private int mApproved;
    private int mReviewed;
    private int mId;

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setFeedbackType(int feedbackType) {

        this.mFeedbackType = feedbackType;

    }

    public int getFeedbackType() {

        return this.mFeedbackType;

    }

    public void setFeedback(String feedback) {

        this.mFeedback = feedback;

    }

    public String getFeedback() {

        return this.mFeedback;

    }

    public void setImagePath(String imagePath) {

        this.mImagePath = imagePath;

    }

    public String getImagePath() {

        return this.mImagePath;

    }

    public void setTime(long time) {

        this.mTime = time;

    }

    public long getTime() {

        return this.mTime;

    }

    public void setApproved(int approved) {

        this.mApproved = approved;

    }

    public int getApproved() {

        return this.mApproved;

    }

    public void setReviewed(int reviewed) {

        this.mReviewed = reviewed;

    }

    public int getReviewed() {

        return this.mReviewed;

    }

    public void setId(int id) {

        this.mId = id;

    }

    public int getId() {

        return this.mId;

    }

    public ContentValues getContentValues() {
        ContentValues initialValues = new ContentValues();

        initialValues.put(KEY_USER_ID, this.mUserId);
        initialValues.put(KEY_FEEDBACK_TYPE, this.mFeedbackType);
        initialValues.put(KEY_FEEDBACK, this.mFeedback);
        initialValues.put(KEY_IMAGE_PATH, this.mImagePath);
        initialValues.put(KEY_TIME, this.mTime);
        initialValues.put(KEY_APPROVED, this.mApproved);
        initialValues.put(KEY_REVIEWED, this.mReviewed);
        initialValues.put(KEY_ID, this.mId);
        return initialValues;
    }

    public static Feedback createObjectFromCursor(Cursor cursor) {
        Feedback obj = new Feedback();
        obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
        obj.setFeedbackType(cursor.getInt((cursor.getColumnIndex(KEY_FEEDBACK_TYPE))));
        obj.setFeedback(cursor.getString((cursor.getColumnIndex(KEY_FEEDBACK))));
        obj.setImagePath(cursor.getString((cursor.getColumnIndex(KEY_IMAGE_PATH))));
        obj.setTime(cursor.getLong((cursor.getColumnIndex(KEY_TIME))));
        obj.setApproved(cursor.getInt((cursor.getColumnIndex(KEY_APPROVED))));
        obj.setReviewed(cursor.getInt((cursor.getColumnIndex(KEY_REVIEWED))));
        obj.setId(cursor.getInt((cursor.getColumnIndex(KEY_ID))));
        return obj;
    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_FEEDBACK_TYPE, this.mFeedbackType);
        json.put(KEY_FEEDBACK, this.mFeedback);
        json.put(KEY_IMAGE_PATH, this.mImagePath);
        json.put(KEY_TIME, this.mTime);
        json.put(KEY_APPROVED, this.mApproved == 0 ? false : true);
        json.put(KEY_REVIEWED, this.mReviewed == 0 ? false : true);
        json.put(KEY_ID, this.mId);

        return json;

    }

    public static Feedback createObjectFromJson(JSONObject json) throws JSONException {

        Feedback obj = new Feedback();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_FEEDBACK_TYPE)) {

            obj.mFeedbackType = json.getInt(KEY_FEEDBACK_TYPE);
        }
        if (!json.isNull(KEY_FEEDBACK)) {

            obj.mFeedback = json.getString(KEY_FEEDBACK);
        }
        if (!json.isNull(KEY_IMAGE_PATH)) {

            obj.mImagePath = json.getString(KEY_IMAGE_PATH);
        }
        if (!json.isNull(KEY_TIME)) {

            obj.mTime = json.getLong(KEY_TIME);
        }
        if (!json.isNull(KEY_APPROVED)) {

            obj.mApproved = json.getInt(KEY_APPROVED);
        }
        if (!json.isNull(KEY_REVIEWED)) {

            obj.mReviewed = json.getInt(KEY_REVIEWED);
        }
        if (!json.isNull(KEY_ID)) {

            obj.mId = json.getInt(KEY_ID);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<Feedback> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (Feedback obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }

    private Feedback(Parcel in) {

        mUserId = in.readString();
        mFeedbackType = in.readInt();
        mFeedback = in.readString();
        mImagePath = in.readString();
        mTime = in.readLong();
        mApproved = in.readInt();
        mReviewed = in.readInt();
        mId = in.readInt();

    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(mUserId);
        out.writeInt(mFeedbackType);
        out.writeString(mFeedback);
        out.writeString(mImagePath);
        out.writeLong(mTime);
        out.writeInt(mApproved);
        out.writeInt(mReviewed);
        out.writeInt(mId);
    }

    public static final Creator<Feedback> CREATOR = new Creator<Feedback>() {

        public Feedback createFromParcel(Parcel in) {
            return new Feedback(in);
        }

        public Feedback[] newArray(int size) {
            return new Feedback[size];
        }
    };

    public int describeContents() {
        return 0;
    }
}

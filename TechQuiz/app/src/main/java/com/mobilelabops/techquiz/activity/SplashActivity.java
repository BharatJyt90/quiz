package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.loginhelper.FbLoginHelper;
import com.mobilelabops.techquiz.loginhelper.GoogleLoginHelper;
import com.mobilelabops.techquiz.loginhelper.ILoginHelper;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.CryptoUtils;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sumanranjan on 22/08/17.
 */

public class SplashActivity extends AppCompatActivity implements ILoginHelper, IServerCallBack{

    private GoogleLoginHelper mGoogleLoginHelper;
    private FbLoginHelper mFbLoginHelper;
    private static int SPLASH_TIME_OUT = 3000;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (Util.isNetworkConnected(SplashActivity.this)) {
                    checkLoginStatus();
                } else {

                    Util.showNoInternetDialog(SplashActivity.this);

                    // Show a button to refresh the UI
                }
            }
        }, SPLASH_TIME_OUT);

    }

    private void checkLoginStatus(){

        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE);
        int regType = preferences.getInt(User.KEY_REG_TYPE,0);

        switch (regType){

            case LoginActivity.LOGIN_TYPE_GOOGLE:

                mGoogleLoginHelper = GoogleLoginHelper.getInstance();
                mGoogleLoginHelper.init(this);
                mGoogleLoginHelper.setLoginHelper(this);
                mGoogleLoginHelper.checkGoogleLogin();

                break;

            case LoginActivity.LOGIN_TYPE_FB:

                mFbLoginHelper = FbLoginHelper.getInstance();
                mFbLoginHelper.facebookSDKInitialize(getApplicationContext());
                mFbLoginHelper.setLoginHelper(this);
                mFbLoginHelper.checkFbLogin();

                break;

            case LoginActivity.MANUAL_LOGIN:

                String email = preferences.getString(User.KEY_EMAIL_ID,null);

                String pass = preferences.getString(User.KEY_PASSWORD,null);

                if(email == null || pass == null) {

                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);

                    finish();

                }else{

                    String password = new String(Base64.decode(pass,Base64.DEFAULT));

                    doSignIn(email,null,password,LoginActivity.MANUAL_LOGIN);
                }

                break;

        }
    }

    @Override
    public void onStart() {

        super.onStart();

    }

    private void doSignIn(String emailId, String name,String password, int regType) {

        JSONObject params = new JSONObject();
        try {

            params.put(User.KEY_EMAIL_ID,emailId);

            if(name != null)
                params.put(User.KEY_NAME,name);

            if(password != null)
                params.put(User.KEY_PASSWORD, CryptoUtils.applyDoubleEncryption(emailId,password));

            params.put(User.KEY_REG_TYPE, regType);

            ServerRequest request = new ServerRequest(Constant.KEY_LOGIN_URL,params.toString(),Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLoginSuccess(String email, String name, String profilePic, String dob, int loginType) {

        // fire login

        SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).edit();
        editor.putString(User.KEY_PROFILE_PIC,profilePic);
        editor.commit();
        doSignIn(email,name,null,loginType);

    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    User user = User.createObjectFromJson(jsonObject);

                    Intent mainActIntent = new Intent(this,MainActivity.class);
                    mainActIntent.putExtra(User.class.getSimpleName(),user);

                    startActivity(mainActIntent);

                    SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE).edit();
                    editor.putInt(User.KEY_REG_TYPE,user.getRegType());
                    editor.putString(User.KEY_EMAIL_ID,user.getEmailId());
                    editor.putString(User.KEY_USER_ID,user.getUserId());
                    editor.commit();

                    finish();

                    return;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_NO_RECORDS_FOUND",Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this,"ERROR_CODE_PASSWORD_MISMATCH",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this,"ERROR_USER_VERIFICATION_PENDING",Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Log.i("SPLASH",exception);
                    Toast.makeText(this,exception,Toast.LENGTH_LONG).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }

    }


    @Override
    public void onServerReqError(Exception e) {


    }

    @Override
    public void onServerReqStarted() {


    }
}

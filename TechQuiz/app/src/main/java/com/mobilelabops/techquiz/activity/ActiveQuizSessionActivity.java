package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

//import android.content.DialogInterface;

/**
 * Created by sumanranjan on 20/04/17.
 */

public class ActiveQuizSessionActivity extends AppCompatActivity implements OnClickListener, IServerCallBack {

    private static final String LEVEL_INDEX = "level_id";
    private static final String SUBJECT_INDEX = "sub_id";
    private static final String SUB_LEVEL_INDEX = "sub_level_id";
    private static final String QUESTION_COUNT = "question_count";
    private static final String QUIZ_SCORE = "quiz_score";

    private ArrayList<Quiz> QuizArrayList = new ArrayList<>();
    private HashMap<Integer, Integer> answerMap = new HashMap<Integer, Integer>();

    // UI Elements
    TextView txtQuestion, txtLabel, txtQuizStatus;
    Button btnNextTo, btnPreviousTo;
    RadioButton ans1Rad, ans2Rad, ans3Rad, ans4Rad;
    RadioGroup radioGroup;
    ProgressBar spinner;

    private int totalQuestion = -1, currentQuesPos = -1, level_id = 0, sub_id = 0, sub_level_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tech_quiz_session);

        // UI Components
        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        txtLabel = (TextView) findViewById(R.id.quizLabel);
        txtQuizStatus = (TextView) findViewById(R.id.txtQuesRunningStatus);
        btnNextTo = (Button) findViewById(R.id.btnNext);
        btnPreviousTo = (Button) findViewById(R.id.btnPrevious);
        ans1Rad = (RadioButton) findViewById(R.id.ansRadio1);
        ans2Rad = (RadioButton) findViewById(R.id.ansRadio2);
        ans3Rad = (RadioButton) findViewById(R.id.ansRadio3);
        ans4Rad = (RadioButton) findViewById(R.id.ansRadio4);
        spinner = (ProgressBar) findViewById(R.id.progressBar);


        Intent intent = getIntent();
        sub_id = intent.getIntExtra(SUBJECT_INDEX, 0);
        level_id = intent.getIntExtra(LEVEL_INDEX, 0);
        sub_level_id = intent.getIntExtra(SUB_LEVEL_INDEX, 0) + 1;

        if (Util.isNetworkConnected(this)) {
            // Server API : Load Questions
            getQuestionDetails();

        } else {
            new AlertDialog.Builder(this)
                    .setTitle("No Internet Connection")
                    .setMessage("It looks like your internet connection is off. Please turn it " +
                            "on and try again")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }


        txtLabel.setText("Level : " + Integer.toString(sub_level_id));
        btnNextTo.setEnabled(false);
        btnNextTo.setOnClickListener(this);
        btnPreviousTo.setOnClickListener(this);

        radioGroup = findViewById(R.id.ansRadioGroup);
        radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);

                // Save Answer to Global Context
                int q_id = QuizArrayList.get(currentQuesPos).getQId();
                answerMap.put(q_id, index + 1);

                switch (index) {
                    case 0: // first button
                        ans1Rad.setTextColor(Color.GRAY);
                        ans2Rad.setTextColor(Color.BLACK);
                        ans3Rad.setTextColor(Color.BLACK);
                        ans4Rad.setTextColor(Color.BLACK);
                        btnNextTo.setEnabled(true);
                        break;
                    case 1: // second button
                        ans1Rad.setTextColor(Color.BLACK);
                        ans2Rad.setTextColor(Color.GRAY);
                        ans3Rad.setTextColor(Color.BLACK);
                        ans4Rad.setTextColor(Color.BLACK);
                        btnNextTo.setEnabled(true);
                        break;
                    case 2: // third button
                        ans1Rad.setTextColor(Color.BLACK);
                        ans2Rad.setTextColor(Color.BLACK);
                        ans3Rad.setTextColor(Color.GRAY);
                        ans4Rad.setTextColor(Color.BLACK);
                        btnNextTo.setEnabled(true);
                        break;
                    case 3: // fourth button
                        ans1Rad.setTextColor(Color.BLACK);
                        ans2Rad.setTextColor(Color.BLACK);
                        ans3Rad.setTextColor(Color.BLACK);
                        ans4Rad.setTextColor(Color.GRAY);
                        btnNextTo.setEnabled(true);
                        break;
                }
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    // Update UI here

    private void updateUI() {

        currentQuesPos++;

        txtQuestion.setText(QuizArrayList.get(currentQuesPos).getQuestion());
        ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
        ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());
        ans3Rad.setText(QuizArrayList.get(currentQuesPos).getOp3());
        ans4Rad.setText(QuizArrayList.get(currentQuesPos).getOp4());
        String runningCount = "" + (currentQuesPos + 1) + " / " + totalQuestion;
        txtQuizStatus.setText(runningCount);

    }

    @Override
    public void onClick(View v) {

        btnNextTo.setEnabled(false);

        switch (v.getId()) {
            case R.id.btnPrevious:

                if (currentQuesPos >= 1) {
                    --currentQuesPos;
                    txtQuestion.setText(QuizArrayList.get(currentQuesPos).getQuestion());

                    // Check size of Answer list, could be less than 4 options at times
                    // Dynamic UI Creation will happen here

                    int answerOptions = 4; //todo QuizArrayList.get(currentQuesPos).answerList.size();

                    switch (answerOptions) {

                        case 4:
                            ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
                            ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());
                            ans3Rad.setText(QuizArrayList.get(currentQuesPos).getOp3());
                            ans4Rad.setText(QuizArrayList.get(currentQuesPos).getOp4());

                            break;

                        case 3:
                            ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
                            ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());
                            ans3Rad.setText(QuizArrayList.get(currentQuesPos).getOp3());

                            break;

                        case 2:
                            ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
                            ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());

                            break;

                        default:
                    }

                    String runningCount = "" + (currentQuesPos + 1) + " / " + totalQuestion;
                    txtQuizStatus.setText(runningCount);
                }

                break;
            case R.id.btnNext:

                if (currentQuesPos < QuizArrayList.size() - 1) {
                    currentQuesPos++;
                    txtQuestion.setText(QuizArrayList.get(currentQuesPos).getQuestion());

                    // Check size of Answer list, could be less than 4 options at times
                    // Dynamic UI Creation will happen here

                    int answerOptions = 4; //todo QuizArrayList.get(currentQuesPos).answerList.size();

                    switch (answerOptions) {

                        case 4:
                            ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
                            ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());
                            ans3Rad.setText(QuizArrayList.get(currentQuesPos).getOp3());
                            ans4Rad.setText(QuizArrayList.get(currentQuesPos).getOp4());

                            break;

                        case 3:
                            ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
                            ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());
                            ans3Rad.setText(QuizArrayList.get(currentQuesPos).getOp3());
                            ans4Rad.setVisibility(View.INVISIBLE);
                            break;

                        case 2:
                            ans1Rad.setText(QuizArrayList.get(currentQuesPos).getOp1());
                            ans2Rad.setText(QuizArrayList.get(currentQuesPos).getOp2());
                            ans3Rad.setVisibility(View.INVISIBLE);
                            ans4Rad.setVisibility(View.INVISIBLE);
                            break;

                        default:
                    }

                    String runningCount = "" + (currentQuesPos + 1) + " / " + totalQuestion;
                    txtQuizStatus.setText(runningCount);

                    disableNextButton();

                } else {

                    int totalCrctAns = calculateQuizScore();

                    Intent intent = new Intent(ActiveQuizSessionActivity.this, ResultActivity.class);
                    intent.putExtra(SUBJECT_INDEX, sub_id);
                    intent.putExtra(LEVEL_INDEX, level_id);
                    intent.putExtra(SUB_LEVEL_INDEX, sub_level_id);
                    intent.putExtra(QUESTION_COUNT, totalQuestion);
                    intent.putExtra(QUIZ_SCORE, totalCrctAns);
                    startActivity(intent);
                }
                break;
        }

    }

    private int calculateQuizScore(){

        int quizScore = 0;

        /*for(Quiz quizItem : QuizArrayList){
            if(quizItem.getCrctOp() == answerMap.get(quizItem.getQId())){
                quizScore ++;
            }
        }*/

        return quizScore;
    }


    private void disableNextButton() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                btnNextTo.setEnabled(false);
            }

        }, 200);
    }

    private void getQuestionDetails() {

        JSONObject params = new JSONObject();
        try {

            params.put(SUBJECT_INDEX, sub_id);
            params.put(LEVEL_INDEX, level_id);
            params.put(SUB_LEVEL_INDEX, sub_level_id);

            ServerRequest request = new ServerRequest(Constant.KEY_QUIZ_URL, params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        spinner.setVisibility(View.INVISIBLE);
        if (result != null) {

            if (result.getResponseCode() == 200) {

                parseServerResponse(result.getResponseString());
            }
        }
    }

    private void parseServerResponse(String response) {

        findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status) {

                case Constant.ERROR_CODE_SUCCESS:

                    try {
                        if (jsonObject.has("result")) {
                            Quiz questionAnswer;
                            String resultStr = jsonObject.getString("result");
                            JSONArray jsonarray = new JSONArray(resultStr);
                            totalQuestion = jsonarray.length();

                            for (int i = 0; i < totalQuestion; i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                questionAnswer = Quiz.createObjectFromJson(jsonobject);
                                QuizArrayList.add(questionAnswer);
                            }
                            updateUI();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//
//                    subLevelAdapter.notifyDataSetInvalidated();

                    break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this, "ERROR_CODE_NO_RECORDS_FOUND", Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this, "ERROR_CODE_PASSWORD_MISMATCH", Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this, "ERROR_USER_VERIFICATION_PENDING", Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this, exception, Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

    }
}

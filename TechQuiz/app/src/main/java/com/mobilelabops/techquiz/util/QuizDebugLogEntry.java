package com.mobilelabops.techquiz.util;

import java.util.Date;

public class QuizDebugLogEntry
{
	public static final int TYPYE_INFO = 1;
	public static final int TYPYE_WARNING = 2;
	public static final int TYPYE_ERROR = 4;
	public static final int TYPYE_ALL = 0xffffffff;

	public int mType;
	public String mTag;
	public String mText;
	public String mDate;

	public static String getTypeString(int type)
	{
		String ret = new String("U");
		switch (type)
		{
			case TYPYE_INFO:
				ret = "I";
			break;
			case TYPYE_WARNING:
				ret = "W";
			break;
			case TYPYE_ERROR:
				ret = "E";
			break;
		}
		return ret;
	}

	QuizDebugLogEntry(int _type, String _tag, String _text)
	{
		Date d = new Date();
		mType = _type;
		mTag = _tag;
		mText = _text;
		mDate = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
	}
}

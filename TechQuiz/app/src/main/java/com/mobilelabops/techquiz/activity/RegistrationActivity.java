package com.mobilelabops.techquiz.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.TempRegTable;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomButton;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.CryptoUtils;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by bhazarix on 9/1/2017.
 */

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, IServerCallBack{

    private CustomEditText mEmailTxt,mNameTxt,mPassTxt,mPromoTxt,mDobTxt;

    public static int ACTIVITY_RESULT_CODE_SUCCESS = 0,ACTIVITY_RESULT_CODE_FAIL = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registartion);

        CustomButton signUpButton = (CustomButton) findViewById(R.id.registerBtn);
        signUpButton.setOnClickListener(this);

        mEmailTxt = (CustomEditText) findViewById(R.id.emailId);
        mNameTxt = (CustomEditText) findViewById(R.id.name);
        mPassTxt = (CustomEditText) findViewById(R.id.password);
        mPromoTxt = (CustomEditText) findViewById(R.id.refer_code);
        mDobTxt = (CustomEditText) findViewById(R.id.dob);
        mDobTxt.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == ACTIVITY_RESULT_CODE_SUCCESS){

            finish();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void sendRegistrationReq(){

        String emailId = mEmailTxt.getText().toString();

        if(Util.isValidEmail(emailId) == false){

            mEmailTxt.setError(getString(R.string.email_error));

            return;
        }

        String password = mPassTxt.getText().toString();

        if(password.length() < 6){

            mPassTxt.setError(getString(R.string.password_error));
            return;
        }

        String name = mNameTxt.getText().toString();

        if(name.isEmpty()){

            mNameTxt.setError(getString(R.string.name_error));
            return;
        }

        String dob = mDobTxt.getText().toString();
        String referCode = mPromoTxt.getText().toString();

        String url = Constant.KEY_REG_URL;

        JSONObject params = new JSONObject();
        try {

            params.put(User.KEY_EMAIL_ID,emailId);
            if(referCode.isEmpty() == false)
                params.put(User.KEY_PROMO,referCode);

            if(dob.isEmpty() == false)
                params.put(User.KEY_PROMO,dob);

            params.put(User.KEY_NAME,name);

            params.put(User.KEY_PASSWORD, CryptoUtils.applyDoubleEncryption(emailId,password));

            params.put(User.KEY_REG_TYPE, 0);

            ServerRequest request = new ServerRequest(url,params.toString(),Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.registerBtn:

                sendRegistrationReq();

                break;

            case R.id.dob:

                Calendar newCalendar = Calendar.getInstance();

                if(mDobTxt.getText().length() != 0){

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    try {

                        Date dob = sdf.parse(mDobTxt.getText().toString());
                        newCalendar.setTime(dob);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(this,  R.style.datepicker,new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String month = String.valueOf(monthOfYear);

                        if(monthOfYear < 10){

                            month = "0"+monthOfYear;
                        }

                        mDobTxt.setText(String.valueOf(dayOfMonth)+"-"+month+"-"+year);

                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();

                break;
        }
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

           switch (status){

               case Constant.ERROR_CODE_SUCCESS:

                   User user = User.createObjectFromJson(jsonObject);

                   Intent verificationIntent = new Intent(this,EmailVerificationActivity.class);
                   verificationIntent.putExtra(TempRegTable.KEY_USER_ID,user.getUserId());
                   verificationIntent.putExtra(TempRegTable.KEY_EMAIL_ID,user.getEmailId());

                   startActivityForResult(verificationIntent,0);

                   break;

               case Constant.ERROR_CODE_DUPLICATE_ENTRY:

                    // already exist
                   Toast.makeText(this,"ERROR_CODE_DUPLICATE_ENTRY",Toast.LENGTH_SHORT).show();

                   break;

               case Constant.ERROR_USER_VERIFICATION_PENDING:

                    //ERROR_USER_VERIFICATION_PENDING

                   Toast.makeText(this,"ERROR_USER_VERIFICATION_PENDING",Toast.LENGTH_SHORT).show();

                   break;

                    default:

                        // ops failed
                        String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                        Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                        break;
           }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                    parseServerResponse(result.getResponseString());
            }

        }

    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

    }
}

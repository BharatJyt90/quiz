package com.mobilelabops.techquiz.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseIntArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.QuizAnswer;
import com.mobilelabops.techquiz.model.Result;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomTextView;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class QuizActivity extends BaseActivity implements QuizFragment.OnFragmentInteractionListener, IServerCallBack, ViewPager.OnPageChangeListener {

    private ArrayList<Quiz> mQuizArrayList = null;
    private CustomTextView mCountTxt,mAttemptedQTxt;
    private ProgressBar mCircularProgressbar;
    private int mQuizSize;
    private SparseIntArray mAttemptedQArray;
    public static final int CODE_KILL_ME = 1,CODE_RESET = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_quiz);

        setTitle("Subject Name");

        getQuestionDetails();

        mCountTxt = (CustomTextView) findViewById(R.id.countTxt);
        mCircularProgressbar = (ProgressBar) findViewById(R.id.circularProgressbar);
        mAttemptedQTxt = (CustomTextView) findViewById(R.id.attempted_q_txt);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void updateUI(){

        if(mQuizArrayList == null || mQuizArrayList.isEmpty()){

            return;
        }

        mCircularProgressbar.setMax(mQuizSize);
        mCircularProgressbar.setSecondaryProgress(mQuizSize);
        ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setOnPageChangeListener(this);
        mPager.setAdapter(new QuizAdapter(getSupportFragmentManager()));
        mPager.setPageTransformer(true, new DepthPageTransformer());
        mAttemptedQTxt.setText(getString(R.string.attempted_quiz)+" 0/"+mQuizSize);
        onPageSelected(0);

        mAttemptedQArray = new SparseIntArray();


    }

    private void getQuestionDetails() {

        JSONObject params = new JSONObject();
        try {

            Intent intent = getIntent();

            int sub_id = intent.getIntExtra(Quiz.KEY_SUB_ID, 0);
            int level_id = intent.getIntExtra(Quiz.KEY_LEVEL_ID, 0);
            String sub_level_id = intent.getStringExtra(Quiz.KEY_SUB_LEVEL_ID) ;

            params.put(Quiz.KEY_SUB_ID, sub_id);
            params.put(Quiz.KEY_LEVEL_ID, level_id);
            params.put(Quiz.KEY_SUB_LEVEL_ID, sub_level_id);

            ServerRequest request = new ServerRequest(Constant.KEY_QUIZ_URL, params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_finish) {

            if(mQuizSize != mAttemptedQArray.size()){

                new AlertDialog.Builder(this)
                        .setMessage(getString(R.string.finish_alert))
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                finishQuiz();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .setIcon(android.R.drawable.ic_dialog_alert).show();
            }else{

                finishQuiz();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {

        QuizFragment.clearAnswerMap();
        super.onDestroy();
    }

    private void finishQuiz(){

        HashMap<Integer,QuizAnswer> answerMap = QuizFragment.getQuizAnswerMap();

        int attemptedQuestion = mAttemptedQArray.size();

        int crctAnsCount = 0;

        ArrayList<QuizAnswer> quizAnswers = new ArrayList<>();

        int size = answerMap.size();

        for( int i = 0; i<size; i++){

            QuizAnswer quizAnswer = answerMap.get(i);

            HashSet<Integer> optedAns = quizAnswer.getOptedAns();

            if(optedAns != null){

                Integer[] optedArray = optedAns.toArray(new Integer[optedAns.size()]);

                int[] crctAns = quizAnswer.getCrctAns();


                Integer[] newArray = new Integer[crctAns.length];
                int index = 0;
                for (int value : crctAns) {
                    newArray[index++] = Integer.valueOf(value);
                }

                if(Arrays.equals(optedArray,newArray)){

                    crctAnsCount ++;
                }

            }

            quizAnswers.add(quizAnswer);
        }

        Result result = new Result();
        result.setCorrectAnsCount(crctAnsCount);
        result.setQuestionAttempeted(attemptedQuestion);

        Intent intent = getIntent();
        int sub_id = intent.getIntExtra(Quiz.KEY_SUB_ID, 0);
        int level_id = intent.getIntExtra(Quiz.KEY_LEVEL_ID, 0);
        int sub_level_id = intent.getIntExtra(Quiz.KEY_SUB_LEVEL_ID, 0);

        result.setSubId(sub_id);
        result.setLevelId(level_id);
        result.setSubLevelId(sub_level_id);

        Intent summaryIntent = new Intent(this,QuizSummaryActivity.class);
        summaryIntent.putExtra(Result.class.getSimpleName(),result);
        summaryIntent.putExtra(Quiz.class.getSimpleName(),mQuizArrayList);
        summaryIntent.putExtra(QuizAnswer.class.getSimpleName(),quizAnswers);
        summaryIntent.putExtra("QuizSize",mQuizSize);
        startActivityForResult(summaryIntent,0);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == CODE_KILL_ME){

            finish();
        }else if(resultCode == CODE_RESET){

            updateUI();
            QuizFragment.clearAnswerMap();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if (result != null) {

            if (result.getResponseCode() == 200) {

                parseServerResponse(result.getResponseString());
            }
        }
    }

    private void parseServerResponse(String response) {

        closeProgressDialog();
        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status) {

                case Constant.ERROR_CODE_SUCCESS:

                    try {
                        if (jsonObject.has(Constant.KEY_RESULT)) {
                            Quiz questionAnswer;
                            String resultStr = jsonObject.getString(Constant.KEY_RESULT);
                            JSONArray jsonarray = new JSONArray(resultStr);
                            mQuizSize = jsonarray.length();

                            mQuizArrayList = new ArrayList<>();

                            for (int i = 0; i < mQuizSize; i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);
                                questionAnswer = Quiz.createObjectFromJson(jsonobject);
                                mQuizArrayList.add(questionAnswer);
                            }
                            updateUI();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//
//                    subLevelAdapter.notifyDataSetInvalidated();

                    break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this, "ERROR_CODE_NO_RECORDS_FOUND", Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_PASSWORD_MISMATCH:

                    //ERROR_CODE_PASSWORD_MISMATCH

                    Toast.makeText(this, "ERROR_CODE_PASSWORD_MISMATCH", Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_USER_VERIFICATION_PENDING:

                    Toast.makeText(this, "ERROR_USER_VERIFICATION_PENDING", Toast.LENGTH_SHORT).show();


                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this, exception, Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServerReqError(Exception e) {

        closeProgressDialog();
        Toast.makeText(this,"Something went wrong.. PLease retry",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog(getString(R.string.cb_please_wait));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        mCircularProgressbar.setProgress(position+1);
        mCountTxt.setText(String.valueOf(position+1));

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onQuestionAttempted(int fragmentPos) {

        mAttemptedQArray.append(fragmentPos,1);
        mAttemptedQTxt.setText(getString(R.string.attempted_quiz)+" "+mAttemptedQArray.size()+"/"+mQuizSize);
    }

    private class QuizAdapter extends FragmentStatePagerAdapter {


        public QuizAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return QuizFragment.newInstance(mQuizArrayList.get(position),position);
        }

        @Override
        public int getCount() {
            return mQuizArrayList.size();
        }
    }

    private class DepthPageTransformer implements ViewPager.PageTransformer {

        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }
}

/**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PurchaseHistory implements Parcelable {

    public final static String DATABASE_TABLE_NAME = "purchase_history";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_DATE_TIME = "date_time";
    public static final String KEY_PURCHASE_TYPE = "purchase_type";
    public static final String KEY_PURCHASE_AMOUNT = "purchase_amount";
    public static final String KEY_COINS_CREDIT = "coins_credit";
    public static final String KEY_ORDER_ID = "order_id";
    public static final String KEY_GATEWAY = "gateway";

    public PurchaseHistory() {

    }

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_DATE_TIME, KEY_PURCHASE_TYPE, KEY_PURCHASE_AMOUNT, KEY_COINS_CREDIT, KEY_ORDER_ID, KEY_GATEWAY,
    };

    private String mUserId;
    private long mDateTime;
    private int mPurchaseType;
    private int mPurchaseAmount;
    private int mCoinsCredit;
    private String mOrderId;
    private int mGateway;

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setDateTime(long dateTime) {

        this.mDateTime = dateTime;

    }

    public long getDateTime() {

        return this.mDateTime;

    }

    public void setPurchaseType(int purchaseType) {

        this.mPurchaseType = purchaseType;

    }

    public int getPurchaseType() {

        return this.mPurchaseType;

    }

    public void setPurchaseAmount(int purchaseAmount) {

        this.mPurchaseAmount = purchaseAmount;

    }

    public int getPurchaseAmount() {

        return this.mPurchaseAmount;

    }

    public void setCoinsCredit(int coinsCredit) {

        this.mCoinsCredit = coinsCredit;

    }

    public int getCoinsCredit() {

        return this.mCoinsCredit;

    }

    public void setOrderId(String orderId) {

        this.mOrderId = orderId;

    }

    public String getOrderId() {

        return this.mOrderId;

    }

    public void setGateway(int gateway) {

        this.mGateway = gateway;

    }

    public int getGateway() {

        return this.mGateway;

    }

    public static String getCrateTableStatement() {

        StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

        createStatement.append(DATABASE_TABLE_NAME).append(" (");

        createStatement.append(KEY_USER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_DATE_TIME).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
        createStatement.append(KEY_PURCHASE_TYPE).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_PURCHASE_AMOUNT).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_COINS_CREDIT).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_ORDER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_GATEWAY).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(")");

        return createStatement.toString();
    }

    public ContentValues getContentValues() {
        ContentValues initialValues = new ContentValues();

        initialValues.put(KEY_USER_ID, this.mUserId);
        initialValues.put(KEY_DATE_TIME, this.mDateTime);
        initialValues.put(KEY_PURCHASE_TYPE, this.mPurchaseType);
        initialValues.put(KEY_PURCHASE_AMOUNT, this.mPurchaseAmount);
        initialValues.put(KEY_COINS_CREDIT, this.mCoinsCredit);
        initialValues.put(KEY_ORDER_ID, this.mOrderId);
        initialValues.put(KEY_GATEWAY, this.mGateway);
        return initialValues;
    }

    public static PurchaseHistory createObjectFromCursor(Cursor cursor) {
        PurchaseHistory obj = new PurchaseHistory();
        obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
        obj.setDateTime(cursor.getLong((cursor.getColumnIndex(KEY_DATE_TIME))));
        obj.setPurchaseType(cursor.getInt((cursor.getColumnIndex(KEY_PURCHASE_TYPE))));
        obj.setPurchaseAmount(cursor.getInt((cursor.getColumnIndex(KEY_PURCHASE_AMOUNT))));
        obj.setCoinsCredit(cursor.getInt((cursor.getColumnIndex(KEY_COINS_CREDIT))));
        obj.setOrderId(cursor.getString((cursor.getColumnIndex(KEY_ORDER_ID))));
        obj.setGateway(cursor.getInt((cursor.getColumnIndex(KEY_GATEWAY))));
        return obj;
    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_DATE_TIME, this.mDateTime);
        json.put(KEY_PURCHASE_TYPE, this.mPurchaseType);
        json.put(KEY_PURCHASE_AMOUNT, this.mPurchaseAmount);
        json.put(KEY_COINS_CREDIT, this.mCoinsCredit);
        json.put(KEY_ORDER_ID, this.mOrderId);
        json.put(KEY_GATEWAY, this.mGateway);

        return json;

    }

    public static PurchaseHistory createObjectFromJson(JSONObject json) throws JSONException {

        PurchaseHistory obj = new PurchaseHistory();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_DATE_TIME)) {

            obj.mDateTime = json.getLong(KEY_DATE_TIME);
        }
        if (!json.isNull(KEY_PURCHASE_TYPE)) {

            obj.mPurchaseType = json.getInt(KEY_PURCHASE_TYPE);
        }
        if (!json.isNull(KEY_PURCHASE_AMOUNT)) {

            obj.mPurchaseAmount = json.getInt(KEY_PURCHASE_AMOUNT);
        }
        if (!json.isNull(KEY_COINS_CREDIT)) {

            obj.mCoinsCredit = json.getInt(KEY_COINS_CREDIT);
        }
        if (!json.isNull(KEY_ORDER_ID)) {

            obj.mOrderId = json.getString(KEY_ORDER_ID);
        }
        if (!json.isNull(KEY_GATEWAY)) {

            obj.mGateway = json.getInt(KEY_GATEWAY);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<PurchaseHistory> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (PurchaseHistory obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }

    private PurchaseHistory(Parcel in) {

        mUserId = in.readString();
        mDateTime = in.readLong();
        mPurchaseType = in.readInt();
        mPurchaseAmount = in.readInt();
        mCoinsCredit = in.readInt();
        mOrderId = in.readString();
        mGateway = in.readInt();

    }

    public void writeToParcel(Parcel out, int flags) {

        out.writeString(mUserId);
        out.writeLong(mDateTime);
        out.writeInt(mPurchaseType);
        out.writeInt(mPurchaseAmount);
        out.writeInt(mCoinsCredit);
        out.writeString(mOrderId);
        out.writeInt(mGateway);
    }

    public static final Parcelable.Creator<PurchaseHistory> CREATOR = new Parcelable.Creator<PurchaseHistory>() {

        public PurchaseHistory createFromParcel(Parcel in) {
            return new PurchaseHistory(in);
        }

        public PurchaseHistory[] newArray(int size) {
            return new PurchaseHistory[size];
        }
    };

    public int describeContents() {
        return 0;
    }
}

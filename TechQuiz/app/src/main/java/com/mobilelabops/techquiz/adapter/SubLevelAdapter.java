package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Level;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 14/04/17.
 */

public class SubLevelAdapter extends BaseAdapter {

    private Level mLevel;
    private LayoutInflater mInflater;
    private Context mContext;

    public SubLevelAdapter(Context context, Level level) {
        mContext = context;
        mLevel = level;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        if (mLevel == null)
            return 0;

        String[] levels = mLevel.getSubLevels();

        return levels == null ? 0 : levels.length;

    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        if (view == null) {
            view = mInflater.inflate(R.layout.sublevel_grid_element, null);
        }
        Log.i("SubLevelAdapter ", "getView ");
        // Subject Name
        TextView tv = (TextView) view.findViewById(R.id.lvlTitle);
        String[] subLevelsArr = mLevel.getSubLevels();

        tv.setText((subLevelsArr[position]));

        // Subject Image

        ImageView img = (ImageView)view.findViewById(R.id.menu);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(mContext, view);
                popupMenu.getMenuInflater().inflate(R.menu.menu_sub_level,
                        popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        Toast.makeText(mContext,menuItem.getTitle(),Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        return view;
    }
}

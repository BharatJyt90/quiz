package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.adapter.CoinHistoryAdapter;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CoinPurchaseActivity extends BaseActivity implements IServerCallBack {

    ArrayList<PurchaseHistory> sPurchaseArrayList = new ArrayList<PurchaseHistory>();
    CoinHistoryAdapter coinHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin_purchase);
        setTitle(getString(R.string.coin_purchase));

        final ListView listview = (ListView) findViewById(R.id.coin_purchase_list);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        String user_id = preferences.getString(User.KEY_USER_ID,null);

        JSONObject params = new JSONObject();
        try {

            //params.put(User.KEY_USER_ID,user_id);

            ServerRequest request = new ServerRequest(Constant.KEY_COIN_PURCHASE_URL,params.toString(),Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (Exception e) {
            e.printStackTrace();
        }

        coinHistoryAdapter = new CoinHistoryAdapter(this, sPurchaseArrayList);
        listview.setAdapter(coinHistoryAdapter);

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }

    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status) {

                case Constant.ERROR_CODE_SUCCESS:

                        String resultStr = jsonObject.getString(Constant.KEY_RESULT);
                        PurchaseHistory creditHistory;
                        JSONArray jsonarray = new JSONArray(resultStr);
                        for (int i = 0; i < jsonarray.length(); i++) {
                            creditHistory = PurchaseHistory.createObjectFromJson(jsonarray.getJSONObject(i));
                            sPurchaseArrayList.add(creditHistory);
                        }

                    coinHistoryAdapter.notifyDataSetInvalidated();
                    break;
                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this, exception, Toast.LENGTH_SHORT).show();

                    break;
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

    }
            @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {
    }

        @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

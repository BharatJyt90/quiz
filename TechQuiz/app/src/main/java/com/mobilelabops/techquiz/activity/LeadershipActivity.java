package com.mobilelabops.techquiz.activity;

import android.os.Bundle;

import com.mobilelabops.techquiz.R;

public class LeadershipActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leadership);
        setTitle(getString(R.string.leadership_title));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

package com.mobilelabops.techquiz.net;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.mobilelabops.techquiz.util.Constant;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by bhazarix on 9/10/2017.
 */

public class UploadImageToServer extends AsyncTask<String, Double, ServerResponse> {

    Context mContext;
    IUploadImageCallBack mCallBack;

    public UploadImageToServer(Context context,IUploadImageCallBack callBack){

        mContext = context;
        mCallBack = callBack;
    }

    @Override
    protected void onPreExecute() {
        // setting progress bar to zero
        //  progressBar.setProgress(0);
        super.onPreExecute();
    }

    @Override
    protected ServerResponse doInBackground(String... params) {
        return uploadFile(params[0],params[1]);
    }


    @SuppressWarnings("deprecation")
    private ServerResponse uploadFile(String sourceFile, String fileName) {


        try {

            URL urlObj = new URL(Constant.KEY_UPLOAD_IMAGE_IP);

            Uri sourceFileUri = Uri.parse(sourceFile);

         //   InputStream fileInputStream = mContext.getContentResolver().openInputStream(sourceFileUri);


            FileInputStream streamIn = new FileInputStream(sourceFile);

            HttpURLConnection httpURLConnection = (HttpURLConnection) urlObj.openConnection();

            httpURLConnection.setRequestMethod("POST");

            String boundary = "*****";
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("ENCTYPE", "multipart/form-data");
            httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            httpURLConnection.setRequestProperty("file", fileName);
            httpURLConnection.setRequestProperty(Constant.ACCEPT_ENCODING, "*/*");

            httpURLConnection.connect();

            DataOutputStream dos = new DataOutputStream(httpURLConnection.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                    + fileName + "\"" + lineEnd);

            dos.writeBytes(lineEnd);


            int bytesAvailable = streamIn.available();

            int originalSize = bytesAvailable;
            int bufferSize = Math.min(bytesAvailable, 1024);
            byte[] buffer = new byte[bufferSize];

            // read file and write it into form...
            int bytesRead = streamIn.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = streamIn.available();
                bufferSize = Math.min(bytesAvailable, 1024);
                bytesRead = streamIn.read(buffer, 0, bufferSize);

            //    publishProgress((double)bytesRead/originalSize);
            }

            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            OutputStream os = httpURLConnection.getOutputStream();
            BufferedReader br =
                    new BufferedReader(
                            new InputStreamReader(httpURLConnection.getInputStream()));  //read response from gateway
            StringBuilder response = null;

            String line;
            while ((line = br.readLine()) != null) {


                if (response == null)
                    response = new StringBuilder();

                response.append(line);

            }

            ServerResponse serverResponse = new ServerResponse();

            if(response != null)
                serverResponse.setResponseString(response.toString());

            Map<String,List<String>> headers = httpURLConnection.getHeaderFields();

            serverResponse.setHeaderFields(httpURLConnection.getHeaderFields());

            serverResponse.setResponseCode(httpURLConnection.getResponseCode());

            os.close();
            dos.close();
            httpURLConnection.disconnect();

            return serverResponse;

        }
        catch (Exception e){
            e.printStackTrace();
        }

        return null;

    }

    @Override
    protected void onPostExecute(ServerResponse result) {

        // showing the server response in an alert dialog

        if(mCallBack != null)
            mCallBack.onUploadStatus(result);

        super.onPostExecute(result);

    }

    public interface IUploadImageCallBack{

        void onUploadStatus(ServerResponse response);
    }



}
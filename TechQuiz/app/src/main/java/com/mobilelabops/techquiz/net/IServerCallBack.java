package com.mobilelabops.techquiz.net;

/**
 * Created by bhazarix on 8/29/2017.
 */

public interface IServerCallBack {

    public void onServeReqResult(ServerResponse result);
    public void onServerReqError(Exception e);
    public void onServerReqStarted();
}

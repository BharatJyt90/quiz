package com.mobilelabops.techquiz.model;
/**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Subject {

    public final static String DATABASE_TABLE_NAME = "subject";

    public static final String KEY_SUB_ID = "sub_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_IMG_URL = "img_url";
    public static final String KEY_CATEGORY = "category";

    public static final String[] COLUMNS = new String[]{KEY_SUB_ID, KEY_NAME, KEY_IMG_URL, KEY_CATEGORY,};

    private int mSubId;
    private String mName;
    private String mImgUrl;
    private int mCategory;

    public void setSubId(int subId) {

        this.mSubId = subId;

    }

    public int getSubId() {

        return this.mSubId;

    }

    public void setName(String name) {

        this.mName = name;

    }

    public String getName() {

        return this.mName;

    }

    public void setImgUrl(String imgUrl) {

        this.mImgUrl = imgUrl;

    }

    public String getImgUrl() {

        return this.mImgUrl;

    }

    public void setCategory(int category) {

        this.mCategory = category;

    }

    public int getCategory() {

        return this.mCategory;

    }



	public static String getCreateTableStatement() {

		StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

		createStatement.append(DATABASE_TABLE_NAME).append(" (");

		createStatement.append(KEY_SUB_ID).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(DbConstants.PRIMARY_KEY).append(", ");
		createStatement.append(KEY_NAME).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
		createStatement.append(KEY_IMG_URL).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
		createStatement.append(KEY_CATEGORY).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(")");

		return createStatement.toString();
 	}

 	public ContentValues getContentValues(){
		ContentValues initialValues = new ContentValues();

		initialValues.put(KEY_SUB_ID,this.mSubId);
		initialValues.put(KEY_NAME,this.mName);
		initialValues.put(KEY_IMG_URL,this.mImgUrl);
		initialValues.put(KEY_CATEGORY,this.mCategory);
		return initialValues;
	}

	public static Subject createObjectFromCursor(Cursor cursor){
		Subject obj = new Subject();
		obj.setSubId(cursor.getInt((cursor.getColumnIndex(KEY_SUB_ID))));
		obj.setName(cursor.getString((cursor.getColumnIndex(KEY_NAME))));
		obj.setImgUrl(cursor.getString((cursor.getColumnIndex(KEY_IMG_URL))));
		obj.setCategory(cursor.getInt((cursor.getColumnIndex(KEY_CATEGORY))));
		return obj;
	}
	
    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_SUB_ID, this.mSubId);
        json.put(KEY_NAME, this.mName);
        json.put(KEY_IMG_URL, this.mImgUrl);
        json.put(KEY_CATEGORY, this.mCategory);

        return json;

    }

    public static Subject createObjectFromJson(JSONObject json) throws JSONException {

        Subject obj = new Subject();
        if (!json.isNull(KEY_SUB_ID)) {

            obj.mSubId = json.getInt(KEY_SUB_ID);
        }
        if (!json.isNull(KEY_NAME)) {

            obj.mName = json.getString(KEY_NAME);
        }
        if (!json.isNull(KEY_IMG_URL)) {

            obj.mImgUrl = json.getString(KEY_IMG_URL);
        }
        if (!json.isNull(KEY_CATEGORY)) {

            obj.mCategory = json.getInt(KEY_CATEGORY);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<Subject> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (Subject obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }
}

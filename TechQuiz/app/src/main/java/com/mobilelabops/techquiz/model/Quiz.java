/**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Quiz implements Parcelable{

    public final static String DATABASE_TABLE_NAME = "quiz";

    public static final String KEY_SUB_ID = "sub_id";
    public static final String KEY_Q_ID = "q_id";
    public static final String KEY_QUESTION = "question";
    public static final String KEY_OP1 = "op1";
    public static final String KEY_OP2 = "op2";
    public static final String KEY_OP3 = "op3";
    public static final String KEY_OP4 = "op4";
    public static final String KEY_CRCT_OP = "crct_op";
    public static final String KEY_LEVEL_ID = "level_id";
    public static final String KEY_SUB_LEVEL_ID = "sub_level_id";
    public static final String KEY_EXPLANATION = "explanation";
    public static final String KEY_TYPE = "type";
    public static final String KEY_CODE_SEGMENT = "code_segment";

    public static final String[] COLOUMNS = new String[]{KEY_SUB_ID, KEY_Q_ID, KEY_QUESTION, KEY_OP1, KEY_OP2, KEY_OP3, KEY_OP4, KEY_CRCT_OP, KEY_LEVEL_ID, KEY_SUB_LEVEL_ID, KEY_EXPLANATION, KEY_TYPE,KEY_CODE_SEGMENT};

    private int mSubId;
    private int mQId;
    private String mQuestion;
    private String mOp1;
    private String mOp2;
    private String mOp3;
    private String mOp4;
    private String mCrctOp;
    private int mLevelId;
    private int mSubLevelId;
    private String mExplanation;
    private int mType;
    private String mCodeSegment;

    public String getCodeSegment() {
        return mCodeSegment;
    }

    public void setCodeSegment(String mCodeSegment) {
        this.mCodeSegment = mCodeSegment;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(mSubId);
        dest.writeInt(mQId);
        dest.writeString(mQuestion);
        dest.writeString(mOp1);
        dest.writeString(mOp2);
        dest.writeString(mOp3);
        dest.writeString(mOp4);
        dest.writeString(mCrctOp);
        dest.writeInt(mLevelId);
        dest.writeInt(mSubLevelId);
        dest.writeString(mExplanation);
        dest.writeInt(mType);
        dest.writeString(mCodeSegment);
    }

    public Quiz(){}

    public Quiz(Parcel in){

        mSubId = in.readInt();
        mQId = in.readInt();
        mQuestion = in.readString();
        mOp1 = in.readString();
        mOp2 = in.readString();
        mOp3 = in.readString();
        mOp4 = in.readString();
        mCrctOp = in.readString();
        mLevelId = in.readInt();
        mSubLevelId = in.readInt();
        mExplanation = in.readString();
        mType = in.readInt();
        mCodeSegment = in.readString();

    }

    public void setSubId(int subId) {

        this.mSubId = subId;

    }

    public int getSubId() {

        return this.mSubId;

    }

    public void setQId(int qId) {

        this.mQId = qId;

    }

    public int getQId() {

        return this.mQId;

    }

    public void setQuestion(String question) {

        this.mQuestion = question;

    }

    public String getQuestion() {

        return this.mQuestion;

    }

    public void setOp1(String op1) {

        this.mOp1 = op1;

    }

    public String getOp1() {

        return this.mOp1;

    }

    public void setOp2(String op2) {

        this.mOp2 = op2;

    }

    public String getOp2() {

        return this.mOp2;

    }

    public void setOp3(String op3) {

        this.mOp3 = op3;

    }

    public String getOp3() {

        return this.mOp3;

    }

    public void setOp4(String op4) {

        this.mOp4 = op4;

    }

    public String getOp4() {

        return this.mOp4;

    }

    public void setCrctOp(String crctOp) {

        this.mCrctOp = crctOp;

    }

    public String getCrctOp() {

        return this.mCrctOp;

    }

    public void setLevelId(int levelId) {

        this.mLevelId = levelId;

    }

    public int getLevelId() {

        return this.mLevelId;

    }

    public void setSubLevelId(int subLevelId) {

        this.mSubLevelId = subLevelId;

    }

    public int getSubLevelId() {

        return this.mSubLevelId;

    }

    public void setExplanation(String explanation) {

        this.mExplanation = explanation;

    }

    public String getExplanation() {

        return this.mExplanation;

    }

    public void setType(int type) {

        this.mType = type;

    }

    public int getType() {

        return this.mType;

    }


    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_SUB_ID, this.mSubId);
        json.put(KEY_Q_ID, this.mQId);
        json.put(KEY_QUESTION, this.mQuestion);
        json.put(KEY_OP1, this.mOp1);
        json.put(KEY_OP2, this.mOp2);
        json.put(KEY_OP3, this.mOp3);
        json.put(KEY_OP4, this.mOp4);
        json.put(KEY_CRCT_OP, this.mCrctOp);
        json.put(KEY_LEVEL_ID, this.mLevelId);
        json.put(KEY_SUB_LEVEL_ID, this.mSubLevelId);
        json.put(KEY_EXPLANATION, this.mExplanation);
        json.put(KEY_TYPE, this.mType);
        json.put(KEY_CODE_SEGMENT,this.mCodeSegment);

        return json;

    }

    public static Quiz createObjectFromJson(JSONObject json) throws JSONException {

        Quiz obj = new Quiz();
        if (!json.isNull(KEY_SUB_ID)) {

            obj.mSubId = json.getInt(KEY_SUB_ID);
        }
        if (!json.isNull(KEY_Q_ID)) {

            obj.mQId = json.getInt(KEY_Q_ID);
        }
        if (!json.isNull(KEY_QUESTION)) {

            obj.mQuestion = json.getString(KEY_QUESTION);
        }
        if (!json.isNull(KEY_OP1)) {

            obj.mOp1 = json.getString(KEY_OP1);
        }
        if (!json.isNull(KEY_OP2)) {

            obj.mOp2 = json.getString(KEY_OP2);
        }
        if (!json.isNull(KEY_OP3)) {

            obj.mOp3 = json.getString(KEY_OP3);
        }
        if (!json.isNull(KEY_OP4)) {

            obj.mOp4 = json.getString(KEY_OP4);
        }
        if (!json.isNull(KEY_CRCT_OP)) {

            obj.mCrctOp = json.getString(KEY_CRCT_OP);
        }
        if (!json.isNull(KEY_LEVEL_ID)) {

            obj.mLevelId = json.getInt(KEY_LEVEL_ID);
        }
        if (!json.isNull(KEY_SUB_LEVEL_ID)) {

            obj.mSubLevelId = json.getInt(KEY_SUB_LEVEL_ID);
        }
        if (!json.isNull(KEY_EXPLANATION)) {

            obj.mExplanation = json.getString(KEY_EXPLANATION);
        }
        if (!json.isNull(KEY_TYPE)) {

            obj.mType = json.getInt(KEY_TYPE);
        }
        if (!json.isNull(KEY_CODE_SEGMENT)) {

            obj.mCodeSegment = json.getString(KEY_CODE_SEGMENT);
        }
        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<Quiz> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (Quiz obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }

    public static final Parcelable.Creator<Quiz> CREATOR
            = new Parcelable.Creator<Quiz>() {
        public Quiz createFromParcel(Parcel in) {
            return new Quiz(in);
        }

        public Quiz[] newArray(int size) {
            return new Quiz[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


}

package com.mobilelabops.techquiz.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.database.PurchaseHistoryDbAdapter;
import com.mobilelabops.techquiz.model.PurchaseHistory;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by bhazarix on 9/2/2017.
 */

public class Util {

    public enum LEVELS{Beginners,Intermediate,Advanced,Professional};

    public static boolean isValidEmail(String email){

        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);

        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }

    private static byte[] generateSalt(String password,String email){

        byte[] salt = new byte[32];

        int passLen = password.length();
        int emailLen = email.length();

        byte crntPassIndex =0,crntEmailIndex=0;

        for(int index=0; index < 32; index++){

            byte b;

            if(index %2 == 0){

                if(crntPassIndex == passLen -1)
                    crntPassIndex = 0;

                b = Byte.valueOf(String.valueOf(password.charAt(crntPassIndex)));
                crntPassIndex++;

            }else {


                if(crntEmailIndex == emailLen -1)
                    crntEmailIndex = 0;

                b = Byte.valueOf(String.valueOf(email.charAt(crntEmailIndex)));

                crntEmailIndex++;
            }

            salt[index] = b;

        }

        return salt;
    }

    public static String applyEncryption(String password,String email) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {

        int iterationCount = 1000;
        int keyLength = 256;
        int saltLength = keyLength / 8; // same size as key output

        SecureRandom random = new SecureRandom();
        byte[] salt = generateSalt(password,email);
     //   random.nextBytes(salt);
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt,
                iterationCount, keyLength);
        SecretKeyFactory keyFactory = SecretKeyFactory
                .getInstance("PBKDF2WithHmacSHA1");
        byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();
        SecretKey key = new SecretKeySpec(keyBytes, "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = new byte[cipher.getBlockSize()];
        random.nextBytes(iv);
        IvParameterSpec ivParams = new IvParameterSpec(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, ivParams);
        byte[] ciphertext = cipher.doFinal(password.getBytes("UTF-8"));

        return new String((ciphertext));
    }

    public static void showNoInternetDialog(Context context){

        new AlertDialog.Builder(context)
                .setTitle("No Internet Connection")
                .setMessage("It looks like your internet connection is off. Please turn it " +
                        "on and try again")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    public static boolean isNetworkConnected(Context context){

        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE); // 1
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo(); // 2
        return networkInfo != null && networkInfo.isConnected(); // 3
    }

    public static void uploadPurchaseDataIfAny(Context context, IServerCallBack callBack){

        if(Util.isNetworkConnected(context)){

            PurchaseHistoryDbAdapter dbAdapter = new PurchaseHistoryDbAdapter();

            ArrayList<PurchaseHistory> pendingPurchaseUploads = dbAdapter.getList(null,null);

            if(pendingPurchaseUploads != null){

                for (PurchaseHistory purchaseHistory : pendingPurchaseUploads) {

                    UploadCoinPurchaseData.savePurchase(context,purchaseHistory,callBack);
                    dbAdapter.delete(PurchaseHistory.KEY_ORDER_ID+" ='"+purchaseHistory.getOrderId()+"'");
                }
            }
        }
    }

    public static HashMap<String, String> getJsonContentTypeHeader(Context context){

        HashMap<String, String> header = new HashMap<>();

        header.put(Constant.ACCEPT, "*/*");
        header.put(Constant.CONTENT_TYPE,"application/json");

        try {

            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            header.put(Constant.USER_AGENT,Constant.getUserAgent(version));

        }
        catch (PackageManager.NameNotFoundException e){

            e.printStackTrace();

        }

        header.put(Constant.ACCEPT_ENCODING, "*/*");

        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.app_name),Context.MODE_PRIVATE);

        header.put(User.KEY_USER_ID, preferences.getString(User.KEY_USER_ID,"") );

        return header;

    }
}

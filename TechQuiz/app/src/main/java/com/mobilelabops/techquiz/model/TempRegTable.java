/**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/
package com.mobilelabops.techquiz.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TempRegTable {

    public final static String DATABASE_TABLE_NAME = "temp_reg_table";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_EMAIL_ID = "email_id";
    public static final String KEY_VERFICATION_CODE = "verfication_code";
    public static final String KEY_EXPIRY = "expiry";

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_EMAIL_ID, KEY_VERFICATION_CODE, KEY_EXPIRY,};

    private String mUserId;
    private String mEmailId;
    private String mVerficationCode;
    private long mExpiry;

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setEmailId(String emailId) {

        this.mEmailId = emailId;

    }

    public String getEmailId() {

        return this.mEmailId;

    }

    public void setVerficationCode(String verficationCode) {

        this.mVerficationCode = verficationCode;

    }

    public String getVerficationCode() {

        return this.mVerficationCode;

    }

    public void setExpiry(long expiry) {

        this.mExpiry = expiry;

    }

    public long getExpiry() {

        return this.mExpiry;

    }


    public ContentValues getContentValues() {
        ContentValues initialValues = new ContentValues();

        initialValues.put(KEY_USER_ID, this.mUserId);
        initialValues.put(KEY_EMAIL_ID, this.mEmailId);
        initialValues.put(KEY_VERFICATION_CODE, this.mVerficationCode);
        initialValues.put(KEY_EXPIRY, this.mExpiry);
        return initialValues;
    }

    public static TempRegTable createObjectFromCursor(Cursor cursor) {
        TempRegTable obj = new TempRegTable();
        obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
        obj.setEmailId(cursor.getString((cursor.getColumnIndex(KEY_EMAIL_ID))));
        obj.setVerficationCode(cursor.getString((cursor.getColumnIndex(KEY_VERFICATION_CODE))));
        obj.setExpiry(cursor.getLong((cursor.getColumnIndex(KEY_EXPIRY))));
        return obj;
    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_EMAIL_ID, this.mEmailId);
        json.put(KEY_VERFICATION_CODE, this.mVerficationCode);
        json.put(KEY_EXPIRY, this.mExpiry);

        return json;

    }

    public static TempRegTable createObjectFromJson(JSONObject json) throws JSONException {

        TempRegTable obj = new TempRegTable();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_EMAIL_ID)) {

            obj.mEmailId = json.getString(KEY_EMAIL_ID);
        }
        if (!json.isNull(KEY_VERFICATION_CODE)) {

            obj.mVerficationCode = json.getString(KEY_VERFICATION_CODE);
        }
        if (!json.isNull(KEY_EXPIRY)) {

            obj.mExpiry = json.getLong(KEY_EXPIRY);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<TempRegTable> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (TempRegTable obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }
}

package com.mobilelabops.techquiz.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.net.UploadImageToServer;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DashboardActivity extends BaseActivity implements UploadImageToServer.IUploadImageCallBack,IServerCallBack, View.OnClickListener {


    private Uri mCropImageUri,mResultUri;
    private ProgressDialog mProgressBar;
    private AppCompatImageView imageButton;
    private User mCurrentUser;
  //  private CustomEditText mNameTxt,mEmailIdTxt,mDobTxt,mPointTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        setTitle(getString(R.string.nav_dashboard));

        imageButton = (AppCompatImageView) findViewById(R.id.quick_start_cropped_image);

        mCurrentUser = getIntent().getParcelableExtra(User.class.getSimpleName());

        LinearLayout itemLayout = (LinearLayout) findViewById(R.id.item_layout);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE);
        String profilePic = preferences.getString(User.KEY_PROFILE_PIC,null);

        if(profilePic != null){

            try{

                if(profilePic.startsWith("http"))
                    Glide.with(getApplicationContext()).load(profilePic).thumbnail(0.2f).into(imageButton);
                else
                 imageButton.setImageURI(Uri.parse(profilePic));

            }
            catch (Exception e){


            }
        }else{

            profilePic = Constant.KEY_DOWNLOAD_IMAGE_IP+mCurrentUser.getUserId()+".jpg";

            Glide.with(getApplicationContext()).load(profilePic).thumbnail(0.2f).into(imageButton);
        }

        int[] itemTxtSrc = new int[]{R.string.submit_quiz,R.string.dash_contrib,R.string.coin_purchase, R.string.coin_usage};
        String[] itemValTxt = new String[]{getString(R.string.submit_quiz),getString(R.string.dash_quiz_contrib),getString(R.string.dash_earnings),getString(R.string.dash_debit)};


        int childCount = itemLayout.getChildCount();

        for(int i=0; i<childCount;i++){

            View view = itemLayout.getChildAt(i);

            TextView itemTxt = view.findViewById(R.id.item_name);
            itemTxt.setText(itemTxtSrc[i]);

            TextView itemDescTxt = view.findViewById(R.id.item_desc_txt);
            itemDescTxt.setText(itemValTxt[i]);

            view.setTag(itemTxtSrc[i]);

            view.setOnClickListener(this);
        }


    }

    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }

    private void upload(){

        if(mResultUri == null)
            return;

        mProgressBar = new ProgressDialog(this);
        mProgressBar.setMessage("Uploading...");
        mProgressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setCancelable(false);
        mProgressBar.show();

        UploadImageToServer upload = new UploadImageToServer(this,this);

        upload.execute(mResultUri.toString(),mCurrentUser.getUserId()+".jpg");//TODO

    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);


            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

             //   mResultUri = result.getUri();

                resizeBitmap(result.getUri());

           //     upload();
            //    Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    public void resizeBitmap(Uri photoPath) {

        BitmapFactory.Options options=new BitmapFactory.Options();
        InputStream is = null;
        try {
            is = getContentResolver().openInputStream(photoPath);

            Bitmap bm = BitmapFactory.decodeStream(is,null,options);
            int Height = bm.getHeight();
            int Width = bm.getWidth();
            Resources r = getResources();
            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, r.getDisplayMetrics());

            int newHeight = px;
            int newWidth = px;
            float scaleWidth = ((float) newWidth) / Width;
            float scaleHeight = ((float) newHeight) / Height;
            Matrix matrix = new Matrix();
            matrix.postScale(scaleWidth, scaleHeight);
            Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0,Width, Height, matrix, true);

            ContextWrapper cw = new ContextWrapper(this);
            File directory = cw.getDir("media", Context.MODE_PRIVATE);

            File newResizedFile = new File(directory,mCurrentUser.getUserId()+".jpg");

            if(newResizedFile.exists())
                newResizedFile.delete();

            newResizedFile.createNewFile();

            FileOutputStream ostream = new FileOutputStream(newResizedFile);

            resizedBitmap.compress(Bitmap.CompressFormat.JPEG,100 , ostream);

            ostream.close();

            mResultUri = Uri.fromFile(newResizedFile);

            imageButton.setImageURI(null);
            imageButton.setImageURI(mResultUri);


          //  upload();

        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        File file = new File(photoPath.toString());
        file.delete();

    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {

      Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, r.getDisplayMetrics());

      /*  DisplayMetrics metrics = r.getDisplayMetrics();
        int px = 200 * ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);*/
     //   return px;

        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setScaleType(CropImageView.ScaleType.FIT_CENTER)
                .setBackgroundColor(getResources().getColor(android.R.color.transparent))
                .setFixAspectRatio(true)
                .setAutoZoomEnabled(false)
                .setMultiTouchEnabled(true)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setMinCropWindowSize(px,px)
               // .setRequestedSize(px,px)
                .start(this);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void updateUser( JSONObject updateObj){

        //JSONObject updateObj = new JSONObject();

        try {

            /*String fileName = mResultUri.getLastPathSegment();

            updateObj.put(User.KEY_PROFILE_PIC,fileName);

            updateObj.put(User.KEY_NAME,mNameTxt.getText().toString());

            updateObj.put(User.KEY_DOB,mDobTxt.getText().toString());*/

            updateObj.put(User.KEY_USER_ID,mCurrentUser.getUserId());

            ServerRequest request = new ServerRequest(Constant.KEY_UPDATE_USER_URL,updateObj.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUploadStatus(ServerResponse response) {

            if(response != null && response.getResponseCode() == 200){

                setResult(MainActivity.RESULT_CODE_PROFILE_PIC_CHANGED);
                SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE).edit();
                editor.putString(User.KEY_PROFILE_PIC,mResultUri.toString());
                editor.commit();

                String[] splittedArr = response.getResponseString().split("/");

                String fileName = splittedArr[splittedArr.length - 1];

                JSONObject updateObj = new JSONObject();
                try {
                    updateObj.put(User.KEY_PROFILE_PIC,fileName);
                    updateUser(updateObj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{

                mProgressBar.dismiss();

                Toast.makeText(this,"Error in upload",Toast.LENGTH_SHORT).show();
            }
    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    Toast.makeText(this,"Updated successfully",Toast.LENGTH_SHORT).show();

                    break;

                case Constant.ERROR_CODE_NO_RECORDS_FOUND:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_NO_RECORDS_FOUND",Toast.LENGTH_SHORT).show();
                    break;

                case Constant.ERROR_CODE_OPERATION_FAILED:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_CODE_OPERATION_FAILED",Toast.LENGTH_SHORT).show();
                    break;
                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(mProgressBar != null)
            mProgressBar.dismiss();

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());

                return;
            }

            Toast.makeText(this,"Error in upload: "+result.getResponseCode(),Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onServerReqError(final Exception e) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(DashboardActivity.this,"Error in upload: "+e.getMessage(),Toast.LENGTH_SHORT).show();
                mProgressBar.dismiss();
            }
        });

    }

    @Override
    public void onServerReqStarted() {

    }


    @Override
    public void onClick(View v) {

        int tag = (int) v.getTag();

        switch (tag){
            case R.string.submit_quiz:
                startActivity(new Intent(DashboardActivity.this, UserSubmitQuizActivity.class));
                break;

            case R.string.dash_contrib:
                startActivity(new Intent(DashboardActivity.this, ContributionActivity.class));
                break;

            case R.string.coin_purchase:
                startActivity(new Intent(DashboardActivity.this, CoinPurchaseActivity.class));
                break;
            case R.string.coin_usage:
                startActivity(new Intent(DashboardActivity.this, CoinUsageActivity.class));
                break;
        }
    }
}

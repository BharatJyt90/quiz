package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.database.SubjectDbAdapter;
import com.mobilelabops.techquiz.model.Quiz;
import com.mobilelabops.techquiz.model.QuizAnswer;
import com.mobilelabops.techquiz.model.Result;
import com.mobilelabops.techquiz.model.Subject;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomTextView;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class QuizSummaryActivity extends BaseActivity implements IServerCallBack{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_summary);

        CustomTextView title = (CustomTextView) findViewById(R.id.toolbar_title);
        title.setText(getText(R.string.summary));

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        ((Animatable) imageView.getDrawable()).start();

        Result result = getIntent().getParcelableExtra(Result.class.getSimpleName());

        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),Context.MODE_PRIVATE);

        result.setUserId(preferences.getString(User.KEY_USER_ID,"") );

        SubjectDbAdapter subjectDbAdapter = new SubjectDbAdapter();
        subjectDbAdapter.open(this);

        Subject subject = subjectDbAdapter.getData(Subject.KEY_SUB_ID+" = "+result.getSubId(),null);

        ((CustomTextView) findViewById(R.id.subject)).setText(subject.getName());
        ((CustomTextView) findViewById(R.id.level)).setText("Sub Level "+result.getSubLevelId());
        ((CustomTextView) findViewById(R.id.total_qstn)).setText(String.valueOf(getIntent().getIntExtra("QuizSize",0)));
        ((CustomTextView) findViewById(R.id.attempted)).setText(String.valueOf(result.getQuestionAttempeted()));
        ((CustomTextView) findViewById(R.id.crct_ans)).setText(String.valueOf(result.getCorrectAnsCount()));
        //((CustomTextView) findViewById(R.id.rating)).setText("5");

        String subLevel = "";

        switch (result.getLevelId()){

            case 0:

                subLevel = Util.LEVELS.Beginners.name();

                break;

            case 1:

                subLevel = Util.LEVELS.Intermediate.name();

                break;

            case 2:

                subLevel = Util.LEVELS.Advanced.name();

                break;

            case 3:

                subLevel = Util.LEVELS.Professional.name();

                break;


        }

        ((CustomTextView) findViewById(R.id.difficulty)).setText(subLevel);



      //  mAnimationHandler.postDelayed(mAnimationDoneRunnable, 1000);

// Start the animation like this

        TableLayout reportTable = (TableLayout) findViewById(R.id.report_table);


        reportTable.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide));
        reportTable.setVisibility(View.VISIBLE);

       //

        uploadResult(result);
    }

    private void uploadResult(Result result){

        try {

            JSONObject params = result.getJsonObj();
            ServerRequest request = new ServerRequest(Constant.KEY_UPLOAD_QUIZ_RESULT,params.toString() , Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_summary, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_retry) {

            setResult(QuizActivity.CODE_RESET);
            finish();

            return true;
        }else if(id == R.id.action_review){

            Intent summaryIntent = new Intent(this,QuizReviewActivity.class);
            summaryIntent.putExtra(Quiz.class.getSimpleName(),getIntent().getParcelableArrayListExtra(Quiz.class.getSimpleName()));
            summaryIntent.putExtra(QuizAnswer.class.getSimpleName(),getIntent().getParcelableArrayListExtra(QuizAnswer.class.getSimpleName()));
            startActivity(summaryIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        setResult(QuizActivity.CODE_KILL_ME);

        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onServeReqResult(ServerResponse result) {


        if (result != null) {

            if (result.getResponseCode() == 200) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result.getResponseString());
                    int status = jsonObject.getInt(Constant.KEY_STATUS);
                    if(status == Constant.ERROR_CODE_SUCCESS){

                        Toast.makeText(this,"Success fully uploaded",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        closeProgressDialog();

    }

    @Override
    public void onServerReqError(Exception e) {

        closeProgressDialog();
    }

    @Override
    public void onServerReqStarted() {

        showProgressDialog("Please wait");
    }
}

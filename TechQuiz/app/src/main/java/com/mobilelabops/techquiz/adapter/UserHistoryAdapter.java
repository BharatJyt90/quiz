package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Result;
import com.mobilelabops.techquiz.model.Subject;
import com.mobilelabops.techquiz.util.Util;

import java.util.ArrayList;

/**
 * Created by 20066954 on 2/15/2018.
 */

public class UserHistoryAdapter extends RecyclerView.Adapter<UserHistoryAdapter.MyViewHolder> {

    private ArrayList<Result> mResultHistory;
    private Context mContext;
    private ArrayList<Subject> mSubjects;
    //private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("dd-MM-YYYY hh:mm a");

    public UserHistoryAdapter(Context context){

        this.mContext = context;
    }

    public void setSubjects(ArrayList<Subject> subjects){

        this.mSubjects = subjects;
    }

    public void setResultHistory(ArrayList<Result> resultHistory){

        this.mResultHistory = resultHistory;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.quiz_history_row, parent, false);
        // Call the view holder's constructor, and pass the view to it;
        // return that new view holder
        return new MyViewHolder(itemView);
    }

    private Subject getSubject(int subId){

        for(Subject sub : mSubjects){

            if(sub.getSubId() == subId)
                return sub;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Result result = mResultHistory.get(position);

        Subject subject = getSubject(result.getSubId());

        if(subject != null)
            holder.mCourseNameTxt.setText(mContext.getString(R.string.course_name,subject.getName()));

        String subLevel = "";

        switch (result.getLevelId()){

            case 0:

                subLevel = Util.LEVELS.Beginners.name();

                break;

            case 1:

                subLevel = Util.LEVELS.Intermediate.name();

                break;

            case 2:

                subLevel = Util.LEVELS.Advanced.name();

                break;

            case 3:

                subLevel = Util.LEVELS.Professional.name();

                break;

        }

        holder.mLevelTxt.setText(mContext.getString(R.string.level_name,subLevel));
        holder.mSubLevelTxt.setText(mContext.getString(R.string.sub_level_name,result.getSubLevelId()));
        //holder.mDateTxt.setText(mSimpleDateFormat.format(new Date(result.getUpdateTime())));
        holder.mDateTxt.setText("11-Dec-2017");
        holder.mCorrectQuestionTxt.setText(mContext.getString(R.string.crct_ans,result.getCorrectAnsCount(),10));
    }

    @Override
    public int getItemCount() {
        return mResultHistory == null ? 0:mResultHistory.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView mCourseNameTxt,mLevelTxt,mSubLevelTxt,mDateTxt,mCorrectQuestionTxt;

        public MyViewHolder(View itemView) {
            super(itemView);

            mCourseNameTxt = (TextView) itemView.findViewById(R.id.courseName);
            mLevelTxt = (TextView) itemView.findViewById(R.id.levelTxt);
            mSubLevelTxt = (TextView) itemView.findViewById(R.id.subLevelTxt);
            mDateTxt = (TextView) itemView.findViewById(R.id.date);
            mCorrectQuestionTxt = (TextView) itemView.findViewById(R.id.question_crct);
        }
    }
}

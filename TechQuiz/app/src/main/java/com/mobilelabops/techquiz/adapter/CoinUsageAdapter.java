package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.CoinUseHistory;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 18/11/17.
 */

public class CoinUsageAdapter extends BaseAdapter {

    ArrayList<CoinUseHistory> sArrayList = new ArrayList<CoinUseHistory>();
    private LayoutInflater mInflater;

    public CoinUsageAdapter(Context context, ArrayList<CoinUseHistory> coinUsageList){
        sArrayList = coinUsageList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (sArrayList == null)
            return 0;
        return sArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return sArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        public TextView subject;
        public TextView level;
        public TextView sub_level;
        public TextView coin_debit;
        public TextView debit_date;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = view;
        // reuse views
        if (rowView == null) {
            rowView = mInflater.inflate(R.layout.coin_usage_element, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.subject = (TextView) rowView.findViewById(R.id.quiz_subject);
            viewHolder.level = (TextView) rowView.findViewById(R.id.quiz_level);
            viewHolder.sub_level = (TextView) rowView.findViewById(R.id.earning_mode);
            viewHolder.coin_debit = (TextView) rowView.findViewById(R.id.coin_credited);
            viewHolder.debit_date = (TextView) rowView.findViewById(R.id.coin_credited_date);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.subject.setText("C++");
        String sLevel = "";
        switch (sArrayList.get(i).getLevelId()){
            case 0: sLevel = "Beginner"; break;
            case 1: sLevel = "Intermediate"; break;
            case 2: sLevel = "Advanced"; break;
            case 3: sLevel = "Professional"; break;
        }
        holder.level.setText(sLevel);
        sLevel = Integer.toString(sArrayList.get(i).getSubLevelId());
        holder.sub_level.setText(sLevel);
        String coin_debit = Integer.toString(sArrayList.get(i).getCoinsDebit());
        holder.coin_debit.setText(coin_debit);
        holder.debit_date.setText("10Dec 2017");

        return rowView;
    }
}

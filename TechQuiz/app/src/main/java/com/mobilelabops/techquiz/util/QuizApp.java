package com.mobilelabops.techquiz.util;

import android.app.Application;
import android.content.Context;

import com.mobilelabops.techquiz.database.QuizLocalDB;

import io.github.kbiakov.codeview.classifier.CodeProcessor;

/**
 * Created by sumanranjan on 15/04/17.
 */

public class QuizApp extends Application {

    public static final String TAG = "QuizApp";
    public static Context context;

    @Override
    public void onCreate() {
        context = getApplicationContext();
        super.onCreate();
        QuizDebug.i(TAG, "starting QuizApp");

        QuizDebug.i(TAG, "init Configs");
        QuizConfig.init(this);
        QuizDebug.i(TAG, "init QuizDB");
        QuizLocalDB.init(this);
        CodeProcessor.init(this);
    }
}

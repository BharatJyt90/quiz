package com.mobilelabops.techquiz.database; /**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.model.PurchaseHistory;

public class PurchaseHistoryDbAdapter extends AbstractSqliteDbAdapter<PurchaseHistory> {

    public PurchaseHistoryDbAdapter() {

        super(PurchaseHistory.DATABASE_TABLE_NAME);

    }

    @Override
    String[] getColumnNames() {

        return PurchaseHistory.COLUMNS;

    }


    @Override
    protected ContentValues getContentValues(PurchaseHistory object) {

        return object.getContentValues();
    }

    @Override
    PurchaseHistory createObjectFromCursor(Cursor cursor) {
        return PurchaseHistory.createObjectFromCursor(cursor);
    }

}

package com.mobilelabops.techquiz.model; /**
Auto Generated using DbCodeGenerator.. 
Please use appropriate import statement and modify as required.
**/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
public class CopyrightClaim{

public final static String DATABASE_TABLE_NAME = "copyright_claim";

public static final String KEY_CLAIM_ID = "claim_id";
public static final String KEY_COMPLAINEE = "complainee";
public static final String KEY_CONTRIBUTOR_ID = "contributor_id";
public static final String KEY_DATE = "date";
public static final String KEY_STATUS = "status";
public static final String KEY_CLAIM_DESC = "claim_desc";
public static final String KEY_LINK_FOR_PROOF = "link_for_proof";
public static final String KEY_RESOLUTION_UNIQUE_CODE = "resolution_unique_code";
public static final String KEY_CONVERSATION_HISTORY = "conversation_history";

public static final String[] COLUMNS = new String[]{KEY_CLAIM_ID,KEY_COMPLAINEE,KEY_CONTRIBUTOR_ID,KEY_DATE,KEY_STATUS,KEY_CLAIM_DESC,KEY_LINK_FOR_PROOF,KEY_RESOLUTION_UNIQUE_CODE,KEY_CONVERSATION_HISTORY,};

private int mClaimId;
private String mComplainee;
private String mContributorId;
private long mDate;
private int mStatus;
private String mClaimDesc;
private String mLinkForProof;
private String mResolutionUniqueCode;
private long mConversationHistory;

public void setClaimId(int claimId){

this.mClaimId = claimId;

}

public int getClaimId(){

return this.mClaimId;

}

public void setComplainee(String complainee){

this.mComplainee = complainee;

}

public String getComplainee(){

return this.mComplainee;

}

public void setContributorId(String contributorId){

this.mContributorId = contributorId;

}

public String getContributorId(){

return this.mContributorId;

}

public void setDate(long date){

this.mDate = date;

}

public long getDate(){

return this.mDate;

}

public void setStatus(int status){

this.mStatus = status;

}

public int getStatus(){

return this.mStatus;

}

public void setClaimDesc(String claimDesc){

this.mClaimDesc = claimDesc;

}

public String getClaimDesc(){

return this.mClaimDesc;

}

public void setLinkForProof(String linkForProof){

this.mLinkForProof = linkForProof;

}

public String getLinkForProof(){

return this.mLinkForProof;

}

public void setResolutionUniqueCode(String resolutionUniqueCode){

this.mResolutionUniqueCode = resolutionUniqueCode;

}

public String getResolutionUniqueCode(){

return this.mResolutionUniqueCode;

}

public void setConversationHistory(long conversationHistory){

this.mConversationHistory = conversationHistory;

}

public long getConversationHistory(){

return this.mConversationHistory;

}



 public static String getCrateTableStatement() {

StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

createStatement.append(DATABASE_TABLE_NAME).append(" (");

createStatement.append(KEY_CLAIM_ID).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(DbConstants.PRIMARY_KEY).append(", ");
createStatement.append(KEY_COMPLAINEE).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_CONTRIBUTOR_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_DATE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
createStatement.append(KEY_STATUS).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
createStatement.append(KEY_CLAIM_DESC).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_LINK_FOR_PROOF).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_RESOLUTION_UNIQUE_CODE).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_CONVERSATION_HISTORY).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(")");

return createStatement.toString();
}

 public ContentValues getContentValues(){
ContentValues initialValues = new ContentValues();

initialValues.put(KEY_CLAIM_ID,this.mClaimId);
initialValues.put(KEY_COMPLAINEE,this.mComplainee);
initialValues.put(KEY_CONTRIBUTOR_ID,this.mContributorId);
initialValues.put(KEY_DATE,this.mDate);
initialValues.put(KEY_STATUS,this.mStatus);
initialValues.put(KEY_CLAIM_DESC,this.mClaimDesc);
initialValues.put(KEY_LINK_FOR_PROOF,this.mLinkForProof);
initialValues.put(KEY_RESOLUTION_UNIQUE_CODE,this.mResolutionUniqueCode);
initialValues.put(KEY_CONVERSATION_HISTORY,this.mConversationHistory);
return initialValues;
}

 public static CopyrightClaim createObjectFromCursor(Cursor cursor){
CopyrightClaim obj = new CopyrightClaim();
obj.setClaimId(cursor.getInt((cursor.getColumnIndex(KEY_CLAIM_ID))));
obj.setComplainee(cursor.getString((cursor.getColumnIndex(KEY_COMPLAINEE))));
obj.setContributorId(cursor.getString((cursor.getColumnIndex(KEY_CONTRIBUTOR_ID))));
obj.setDate(cursor.getLong((cursor.getColumnIndex(KEY_DATE))));
obj.setStatus(cursor.getInt((cursor.getColumnIndex(KEY_STATUS))));
obj.setClaimDesc(cursor.getString((cursor.getColumnIndex(KEY_CLAIM_DESC))));
obj.setLinkForProof(cursor.getString((cursor.getColumnIndex(KEY_LINK_FOR_PROOF))));
obj.setResolutionUniqueCode(cursor.getString((cursor.getColumnIndex(KEY_RESOLUTION_UNIQUE_CODE))));
obj.setConversationHistory(cursor.getLong((cursor.getColumnIndex(KEY_CONVERSATION_HISTORY))));
return obj;
}
public JSONObject getJsonObj() throws JSONException {

JSONObject json = new JSONObject();
json.put(KEY_CLAIM_ID, this.mClaimId);
json.put(KEY_COMPLAINEE, this.mComplainee);
json.put(KEY_CONTRIBUTOR_ID, this.mContributorId);
json.put(KEY_DATE, this.mDate);
json.put(KEY_STATUS, this.mStatus);
json.put(KEY_CLAIM_DESC, this.mClaimDesc);
json.put(KEY_LINK_FOR_PROOF, this.mLinkForProof);
json.put(KEY_RESOLUTION_UNIQUE_CODE, this.mResolutionUniqueCode);
json.put(KEY_CONVERSATION_HISTORY, this.mConversationHistory);

 return json;

}

public static CopyrightClaim createObjectFromJson(JSONObject json) throws JSONException{

CopyrightClaim obj = new CopyrightClaim();
if(!json.isNull(KEY_CLAIM_ID)){

obj.mClaimId = json.getInt(KEY_CLAIM_ID);
}
if(!json.isNull(KEY_COMPLAINEE)){

obj.mComplainee = json.getString(KEY_COMPLAINEE);
}
if(!json.isNull(KEY_CONTRIBUTOR_ID)){

obj.mContributorId = json.getString(KEY_CONTRIBUTOR_ID);
}
if(!json.isNull(KEY_DATE)){

obj.mDate = json.getLong(KEY_DATE);
}
if(!json.isNull(KEY_STATUS)){

obj.mStatus = json.getInt(KEY_STATUS);
}
if(!json.isNull(KEY_CLAIM_DESC)){

obj.mClaimDesc = json.getString(KEY_CLAIM_DESC);
}
if(!json.isNull(KEY_LINK_FOR_PROOF)){

obj.mLinkForProof = json.getString(KEY_LINK_FOR_PROOF);
}
if(!json.isNull(KEY_RESOLUTION_UNIQUE_CODE)){

obj.mResolutionUniqueCode = json.getString(KEY_RESOLUTION_UNIQUE_CODE);
}
if(!json.isNull(KEY_CONVERSATION_HISTORY)){

obj.mConversationHistory = json.getLong(KEY_CONVERSATION_HISTORY);
}

 return obj;

}

public static JSONArray objListToJSONObjectArray(ArrayList<CopyrightClaim> list) throws JSONException{

 JSONArray array = new JSONArray();
for(CopyrightClaim obj : list){

 array.put(obj.getJsonObj());
}
 
 return array;
}
}

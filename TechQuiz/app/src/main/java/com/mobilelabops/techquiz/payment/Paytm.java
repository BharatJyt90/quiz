package com.mobilelabops.techquiz.payment;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
/*import com.paytm.pgsdk.Log;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;*/

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bhazarix on 11/19/2017.
 */
/**
 * The following Staging Wallet can be used for testing transactions:
 Mobile number: 7777777777
 Password: Paytm12345
 OTP – 489871

 Response Codes:-
 * 101-  Transaction Successful
 141-  Transaction cancelled by customer after landing on Payment Gateway Page.
 227-  Payment Failed due to a technical error. Please try after some time.
 810-  Page closed by customer after landing on Payment Gateway Page.
 8102-   Transaction cancelled by customer post login. Customer had sufficient Wallet balance for completing transaction.
 8103-  Transaction cancelled by customer post login. Customer had in-sufficient Wallet balance for completing transaction.
 *
 */


public class Paytm /*implements PaytmPaymentTransactionCallback*/ {

    private Context mContext;

    private String mOrderNumber;
    private String mCustomerId;
    private int mAmount;

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String mOrderNumber) {
        this.mOrderNumber = mOrderNumber;
    }

    public String getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(String mCustomerId) {
        this.mCustomerId = mCustomerId;
    }

    public int getAmount() {
        return mAmount;
    }

    public void setAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public void startTransaction(Context appContext){

    /*    mContext = appContext;
        PaytmPGService Service = PaytmPGService.getStagingService();
        //TODO: Enable for Production
      //  Service = PaytmPGService.getProductionService();
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("MID" , appContext.getString(R.string.paytm_MID));
        paramMap.put("ORDER_ID" , mOrderNumber);
        paramMap.put("CUST_ID" , mCustomerId);
        paramMap.put("INDUSTRY_TYPE_ID" , appContext.getString(R.string.paytm_INDUSTRY_TYPE_ID));
        paramMap.put("CHANNEL_ID" , appContext.getString(R.string.paytm_CHANNEL_ID));
        paramMap.put("TXN_AMOUNT" , String.valueOf(mAmount));
        paramMap.put("WEBSITE" , appContext.getString(R.string.paytm_website));
        paramMap.put("CALLBACK_URL" , appContext.getString(R.string.paytm_CALLBACK_URL));
        paramMap.put("CHECKSUMHASH" , "w2QDRMgp1/BNdEnJEAPCIOmNgQvsi+BhpqijfM9KvFfRiPmGSt3Ddzw+oTaGCLneJwxFFq5mqTMwJXdQE2EzK4px2xruDqKZjHupz9yXev4=");
        PaytmOrder Order = new PaytmOrder(paramMap);

        Service.initialize(Order,null);

        Service.startPaymentTransaction(appContext, true, true,this);*/
    }

   /* @Override
    public void someUIErrorOccurred(String inErrorMessage) {
        // Some UI Error Occurred in Payment Gateway Activity.
        // // This may be due to initialization of views in
        // Payment Gateway Activity or may be due to //
        // initialization of webview. // Error Message details
        // the error occurred.
    }

    @Override
    public void onTransactionResponse(Bundle inResponse) {
        Log.d("LOG", "Payment Transaction : " + inResponse);
        Toast.makeText(mContext, "Payment Transaction response "+inResponse.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkNotAvailable() {
        // If network is not
        // available, then this
        // method gets called.
    }

    @Override
    public void clientAuthenticationFailed(String inErrorMessage) {
        // This method gets called if client authentication
        // failed. // Failure may be due to following reasons //
        // 1. Server error or downtime. // 2. Server unable to
        // generate checksum or checksum response is not in
        // proper format. // 3. Server failed to authenticate
        // that client. That is value of payt_STATUS is 2. //
        // Error Message describes the reason for failure.
    }

    @Override
    public void onErrorLoadingWebPage(int iniErrorCode,
                                      String inErrorMessage, String inFailingUrl) {

    }

    // had to be added: NOTE
    @Override
    public void onBackPressedCancelTransaction() {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
        Toast.makeText(mContext, "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
    }*/
}

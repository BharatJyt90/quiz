package com.mobilelabops.techquiz.model; /**
Auto Generated using DbCodeGenerator.. 
Please use appropriate import statement and modify as required.
**/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReedemRequest{

public final static String DATABASE_TABLE_NAME = "reedem_request";

public static final String KEY_REEDEM_ID = "reedem_id";
public static final String KEY_USER_ID = "user_id";
public static final String KEY_REQUEST_DATE = "request_date";
public static final String KEY_POINTS_COUNT = "points_count";
public static final String KEY_PAYMENT_PARTNER = "payment_partner";
public static final String KEY_STATUS = "status";
public static final String KEY_LAST_UPDATED_DATE = "last_updated_date";
public static final String KEY_COMMENT = "comment";
public static final String KEY_TRANSCATION_ID = "transcation_id";
public static final String KEY_PROPOSED_CASHBACK_AMOUNT = "proposed_cashback_amount";
public static final String KEY_CURRENCY = "currency";
public static final String KEY_WALLET_ADDRESS = "wallet_address";

public static final String[] COLUMNS = new String[]{KEY_REEDEM_ID,KEY_USER_ID,KEY_REQUEST_DATE,KEY_POINTS_COUNT,KEY_PAYMENT_PARTNER,KEY_STATUS,KEY_LAST_UPDATED_DATE,KEY_COMMENT,KEY_TRANSCATION_ID,KEY_PROPOSED_CASHBACK_AMOUNT,KEY_CURRENCY,KEY_WALLET_ADDRESS,};

private String mReedemId;
private String mUserId;
private long mRequestDate;
private int mPointsCount;
private String mPaymentPartner;
private int mStatus;
private long mLastUpdatedDate;
private String mComment;
private String mTranscationId;
private double mProposedCashbackAmount;
private String mCurrency;
private String mWalletAddress;

public void setReedemId(String reedemId){

this.mReedemId = reedemId;

}

public String getReedemId(){

return this.mReedemId;

}

public void setUserId(String userId){

this.mUserId = userId;

}

public String getUserId(){

return this.mUserId;

}

public void setRequestDate(long requestDate){

this.mRequestDate = requestDate;

}

public long getRequestDate(){

return this.mRequestDate;

}

public void setPointsCount(int pointsCount){

this.mPointsCount = pointsCount;

}

public int getPointsCount(){

return this.mPointsCount;

}

public void setPaymentPartner(String paymentPartner){

this.mPaymentPartner = paymentPartner;

}

public String getPaymentPartner(){

return this.mPaymentPartner;

}

public void setStatus(int status){

this.mStatus = status;

}

public int getStatus(){

return this.mStatus;

}

public void setLastUpdatedDate(long lastUpdatedDate){

this.mLastUpdatedDate = lastUpdatedDate;

}

public long getLastUpdatedDate(){

return this.mLastUpdatedDate;

}

public void setComment(String comment){

this.mComment = comment;

}

public String getComment(){

return this.mComment;

}

public void setTranscationId(String transcationId){

this.mTranscationId = transcationId;

}

public String getTranscationId(){

return this.mTranscationId;

}

public void setProposedCashbackAmount(double proposedCashbackAmount){

this.mProposedCashbackAmount = proposedCashbackAmount;

}

public double getProposedCashbackAmount(){

return this.mProposedCashbackAmount;

}

public void setCurrency(String currency){

this.mCurrency = currency;

}

public String getCurrency(){

return this.mCurrency;

}

public void setWalletAddress(String walletAddress){

this.mWalletAddress = walletAddress;

}

public String getWalletAddress(){

return this.mWalletAddress;

}



 public static String getCrateTableStatement() {

StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

createStatement.append(DATABASE_TABLE_NAME).append(" (");

createStatement.append(KEY_REEDEM_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_USER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_REQUEST_DATE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
createStatement.append(KEY_POINTS_COUNT).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
createStatement.append(KEY_PAYMENT_PARTNER).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_STATUS).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
createStatement.append(KEY_LAST_UPDATED_DATE).append(" ").append(DbConstants.DATA_TYPE_NUMERIC).append(", ");
createStatement.append(KEY_COMMENT).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_TRANSCATION_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_PROPOSED_CASHBACK_AMOUNT).append(" ").append(DbConstants.DATA_TYPE_REAL).append(", ");
createStatement.append(KEY_CURRENCY).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
createStatement.append(KEY_WALLET_ADDRESS).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(")");

return createStatement.toString();
}

 public ContentValues getContentValues(){
ContentValues initialValues = new ContentValues();

initialValues.put(KEY_REEDEM_ID,this.mReedemId);
initialValues.put(KEY_USER_ID,this.mUserId);
initialValues.put(KEY_REQUEST_DATE,this.mRequestDate);
initialValues.put(KEY_POINTS_COUNT,this.mPointsCount);
initialValues.put(KEY_PAYMENT_PARTNER,this.mPaymentPartner);
initialValues.put(KEY_STATUS,this.mStatus);
initialValues.put(KEY_LAST_UPDATED_DATE,this.mLastUpdatedDate);
initialValues.put(KEY_COMMENT,this.mComment);
initialValues.put(KEY_TRANSCATION_ID,this.mTranscationId);
initialValues.put(KEY_PROPOSED_CASHBACK_AMOUNT,this.mProposedCashbackAmount);
initialValues.put(KEY_CURRENCY,this.mCurrency);
initialValues.put(KEY_WALLET_ADDRESS,this.mWalletAddress);
return initialValues;
}

 public static ReedemRequest createObjectFromCursor(Cursor cursor){
ReedemRequest obj = new ReedemRequest();
obj.setReedemId(cursor.getString((cursor.getColumnIndex(KEY_REEDEM_ID))));
obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
obj.setRequestDate(cursor.getLong((cursor.getColumnIndex(KEY_REQUEST_DATE))));
obj.setPointsCount(cursor.getInt((cursor.getColumnIndex(KEY_POINTS_COUNT))));
obj.setPaymentPartner(cursor.getString((cursor.getColumnIndex(KEY_PAYMENT_PARTNER))));
obj.setStatus(cursor.getInt((cursor.getColumnIndex(KEY_STATUS))));
obj.setLastUpdatedDate(cursor.getLong((cursor.getColumnIndex(KEY_LAST_UPDATED_DATE))));
obj.setComment(cursor.getString((cursor.getColumnIndex(KEY_COMMENT))));
obj.setTranscationId(cursor.getString((cursor.getColumnIndex(KEY_TRANSCATION_ID))));
obj.setProposedCashbackAmount(cursor.getDouble((cursor.getColumnIndex(KEY_PROPOSED_CASHBACK_AMOUNT))));
obj.setCurrency(cursor.getString((cursor.getColumnIndex(KEY_CURRENCY))));
obj.setWalletAddress(cursor.getString((cursor.getColumnIndex(KEY_WALLET_ADDRESS))));
return obj;
}
public JSONObject getJsonObj() throws JSONException {

JSONObject json = new JSONObject();
json.put(KEY_REEDEM_ID, this.mReedemId);
json.put(KEY_USER_ID, this.mUserId);
json.put(KEY_REQUEST_DATE, this.mRequestDate);
json.put(KEY_POINTS_COUNT, this.mPointsCount);
json.put(KEY_PAYMENT_PARTNER, this.mPaymentPartner);
json.put(KEY_STATUS, this.mStatus);
json.put(KEY_LAST_UPDATED_DATE, this.mLastUpdatedDate);
json.put(KEY_COMMENT, this.mComment);
json.put(KEY_TRANSCATION_ID, this.mTranscationId);
json.put(KEY_PROPOSED_CASHBACK_AMOUNT, this.mProposedCashbackAmount);
json.put(KEY_CURRENCY, this.mCurrency);
json.put(KEY_WALLET_ADDRESS, this.mWalletAddress);

 return json;

}

public static ReedemRequest createObjectFromJson(JSONObject json) throws JSONException{

ReedemRequest obj = new ReedemRequest();
if(!json.isNull(KEY_REEDEM_ID)){

obj.mReedemId = json.getString(KEY_REEDEM_ID);
}
if(!json.isNull(KEY_USER_ID)){

obj.mUserId = json.getString(KEY_USER_ID);
}
if(!json.isNull(KEY_REQUEST_DATE)){

obj.mRequestDate = json.getLong(KEY_REQUEST_DATE);
}
if(!json.isNull(KEY_POINTS_COUNT)){

obj.mPointsCount = json.getInt(KEY_POINTS_COUNT);
}
if(!json.isNull(KEY_PAYMENT_PARTNER)){

obj.mPaymentPartner = json.getString(KEY_PAYMENT_PARTNER);
}
if(!json.isNull(KEY_STATUS)){

obj.mStatus = json.getInt(KEY_STATUS);
}
if(!json.isNull(KEY_LAST_UPDATED_DATE)){

obj.mLastUpdatedDate = json.getLong(KEY_LAST_UPDATED_DATE);
}
if(!json.isNull(KEY_COMMENT)){

obj.mComment = json.getString(KEY_COMMENT);
}
if(!json.isNull(KEY_TRANSCATION_ID)){

obj.mTranscationId = json.getString(KEY_TRANSCATION_ID);
}
if(!json.isNull(KEY_PROPOSED_CASHBACK_AMOUNT)){

obj.mProposedCashbackAmount = json.getDouble(KEY_PROPOSED_CASHBACK_AMOUNT);
}
if(!json.isNull(KEY_CURRENCY)){

obj.mCurrency = json.getString(KEY_CURRENCY);
}
if(!json.isNull(KEY_WALLET_ADDRESS)){

obj.mWalletAddress = json.getString(KEY_WALLET_ADDRESS);
}

 return obj;

}

public static JSONArray objListToJSONObjectArray(ArrayList<ReedemRequest> list) throws JSONException{

 JSONArray array = new JSONArray();
for(ReedemRequest obj : list){

 array.put(obj.getJsonObj());
}
 
 return array;
}
}

package com.mobilelabops.techquiz.net;

import java.util.HashMap;

/**
 * Created by bhazarix on 9/2/2017.
 */

public class ServerRequest {

    private String mUrl;
    private String mBody;
    private HashMap<String, String> mHeader;

    public ServerRequest(String url, String body, HashMap<String, String> header) {
        this.mUrl = url;
        this.mBody = body;
        this.mHeader = header;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getBody() {
        return mBody;
    }

    public HashMap<String, String> getHeader() {
        return mHeader;
    }
}

package com.mobilelabops.techquiz.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sumanranjan on 13/11/17.
 */

public class QuizConfig {
    public static final String TAG = "Configs";

    // Boolean Config Parameters
    public static final int CFG_PARAM_B_FIRSTRUN = 0; // App First Run

    // Integer Config Parameters
    public static final int CFG_PARAM_I_USER_REG_TYPE = 0; // Registration Type

    // String Config Parameters
    public static final int CFG_PARAM_S_PASSWORD = 0; // User Password
    public static final int CFG_PARAM_S_USER_EMAIL = 1; // Email ID
    public static final int CFG_PARAM_S_USER_ID = 2; // User ID
    public static final int CFG_PARAM_S_USER_PIC = 3; // User PIC

    // Preference file name
    private static final String PREFS_NAME = "TechQuizPrefs";

    private static final String sBoolNames[] = { "CFG_PARAM_B_FIRSTRUN"};
    private static final boolean sBoolDefs[] = { true };
    private static boolean sBoolData[] = null;

    private static final String sIntNames[] = { "CFG_PARAM_I_USER_REG_TYPE"};
    private static final int sIntDefs[] = { 0 };
    private static int sIntData[] = null;

    private static final String sStringNames[] = { "CFG_PARAM_S_PASSWORD", "CFG_PARAM_S_USER_EMAIL",
            "CFG_PARAM_S_USER_ID", "CFG_PARAM_S_USER_PIC"};
    private static final String sStrDefs[] = { "unknown", "unknown", "unknown", "unknown"};
    private static String sStrData[] = null;

    private static SharedPreferences.Editor sEditor = null;

    public static boolean init(Context ctx) {
        int i;
        sBoolData = new boolean[sBoolNames.length];
        sIntData = new int[sIntNames.length];
        sStrData = new String[sStringNames.length];

        SharedPreferences configs = ctx.getSharedPreferences(PREFS_NAME, 0);
        sEditor = configs.edit();

        // Read bools
        for (i = 0; i < sBoolNames.length; i++) {
            sBoolData[i] = configs.getBoolean(sBoolNames[i], sBoolDefs[i]);
        }

        // Read integer
        for (i = 0; i < sIntNames.length; i++) {
            sIntData[i] = configs.getInt(sIntNames[i], sIntDefs[i]);
        }

        // Read string
        for (i = 0; i < sStringNames.length; i++) {
            sStrData[i] = configs.getString(sStringNames[i], sStrDefs[i]);
        }

        return getBool(CFG_PARAM_B_FIRSTRUN);
    }

    public static boolean getBool(final int cfgParam) {
        return sBoolData[cfgParam];
    }

    public static int getInt(final int cfgParam) {
        return sIntData[cfgParam];
    }

    public static String getString(final int cfgParam) {
        return sStrData[cfgParam];
    }


    public static void setBool(final int cfgParam, final boolean bValue,
                               boolean commit) {
        sBoolData[cfgParam] = bValue;
        sEditor.putBoolean(sBoolNames[cfgParam], bValue);
        if (commit)
            sEditor.commit();
    }

    public static void setInt(final int cfgParam, final int iValue,
                              boolean commit) {

        sIntData[cfgParam] = iValue;
        sEditor.putInt(sIntNames[cfgParam], iValue);
        if (commit)
            sEditor.commit();

    }

    public static void setString(final int cfgParam, final String sValue,
                                 boolean commit) {
        sStrData[cfgParam] = sValue;
        sEditor.putString(sStringNames[cfgParam], sValue);
        if (commit)
            sEditor.commit();
    }

}

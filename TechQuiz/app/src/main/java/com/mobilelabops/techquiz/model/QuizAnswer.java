package com.mobilelabops.techquiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by bhazarix on 10/10/2017.
 */

public class QuizAnswer implements Parcelable{

    private int mQId;
    private HashSet<Integer> mOptedAns;
    private int[] mCrctAns;
    private int mParcellableOptedAns = 0xFFFF;

    public QuizAnswer(){

        mOptedAns = new HashSet<>();
    }

    public int getParcelableOptedAns() {
        return mParcellableOptedAns;
    }

    public QuizAnswer(Parcel in){

        mQId = in.readInt();
        mParcellableOptedAns = in.readInt();
        mCrctAns = new int[in.readInt()];
        in.readIntArray(mCrctAns);

    }

    public int getQId() {
        return mQId;
    }

    public void setQId(int mQId) {
        this.mQId = mQId;
    }

    public HashSet<Integer> getOptedAns() {
        return mOptedAns;
    }

    /*//public void setOptedAns(String mOptedAns) {
        this.mOptedAns = mOptedAns;
    }*/

    public int[] getCrctAns() {
        return mCrctAns;
    }

    public void setCrctAns(String crctAns) {

        if(crctAns != null){

            if(crctAns.length() == 1){

                mCrctAns = new int[1];
                mCrctAns[0] = Integer.valueOf(crctAns);
            }
            else{

                String[] answers = crctAns.split(",");

                mCrctAns = new int[answers.length];

                byte index = 0;

                for(String ans : answers){

                    mCrctAns[index++] = Integer.valueOf(ans);
                }
            }
        }


    }

    public static final Parcelable.Creator<QuizAnswer> CREATOR
            = new Parcelable.Creator<QuizAnswer>() {
        public QuizAnswer createFromParcel(Parcel in) {
            return new QuizAnswer(in);
        }

        public QuizAnswer[] newArray(int size) {
            return new QuizAnswer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeInt(mQId);

        if(mOptedAns != null){

            mParcellableOptedAns = 0xffff;
            Iterator it = mOptedAns.iterator();
            while (it.hasNext()) {

                Integer val = (Integer) it.next();
                mParcellableOptedAns = mParcellableOptedAns << 4 | val;

            }
        }

        parcel.writeInt(mParcellableOptedAns);
        parcel.writeInt(mCrctAns.length);
        parcel.writeIntArray(mCrctAns);


    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobilelabops.techquiz.util;

import android.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
 
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author bhazarix
 */
public class CryptoUtils {
    
    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES";

    public static String applyDoubleEncryption(String email, String pass) {

        byte[] firstKey = generateKey(email,pass);

        String encryptedPass = encrypt(firstKey,pass);

        StringBuilder reverseEmail = new StringBuilder(email).reverse();
        StringBuilder reversePass = new StringBuilder(pass).reverse();

        byte[] secondKey = generateKey(reverseEmail.toString(),reversePass.toString());

      //  String decryptedPass = decrypt(secondKey,encryptedPass);*/

      /*  MessageDigest digest = null;
        try {

            digest = MessageDigest.getInstance("MD5");
            byte[] md5Pass = digest.digest(pass.getBytes());

            byte[] base64 = Base64.encode(md5Pass,Base64.DEFAULT);

            return  new String((md5Pass));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
*/

    //    return  pass;//new String(Base64.encode(pass.getBytes(),Base64.DEFAULT));
        return encrypt(secondKey,encryptedPass);

    }

    private static byte[] generateKey(String email,String password){

        byte[] salt = new byte[16];

        int passLen = password.length();
        int emailLen = email.length();

        byte crntPassIndex =0,crntEmailIndex=0;

        for(int index=0; index < 16; index++){

            byte b;

            if(index %2 == 0){

                if(crntPassIndex == passLen -1)
                    crntPassIndex = 0;

                b = (byte)password.charAt(crntPassIndex);
                crntPassIndex++;

            }else {


                if(crntEmailIndex == emailLen -1)
                    crntEmailIndex = 0;

                b = (byte)email.charAt(crntEmailIndex);

                crntEmailIndex++;
            }

            salt[index] = b;

        }

        return salt;
    }

    public static String encrypt(byte[] keyBytes,String text)
             {
        return doCrypto(Cipher.ENCRYPT_MODE, keyBytes, text);
    }
 
    public static String decrypt(byte[] keyBytes, String text)
            {
        return doCrypto(Cipher.DECRYPT_MODE, keyBytes, text);
    }
 
    private static String doCrypto(int cipherMode, byte[] keyBytes, String text) {
        try {

            Key secretKey = new SecretKeySpec(keyBytes, ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);
             
            byte[] outputBytes = cipher.doFinal(text.getBytes());

            return new String(outputBytes);

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException ex) {

            ex.printStackTrace();
        }

        return null;
    }
}

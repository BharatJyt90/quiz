package com.mobilelabops.techquiz.net;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bhazarix on 9/2/2017.
 */

public class ServerAsynTask extends AsyncTask<ServerRequest, Void, ServerResponse> {

    private IServerCallBack mServerCallBack;
    private static String SESSION_ID = null;

    public ServerAsynTask(IServerCallBack serverCallBack) {
        this.mServerCallBack = serverCallBack;
    }

    @Override
    protected void onPreExecute() {

        if(mServerCallBack != null)
            mServerCallBack.onServerReqStarted();

        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ServerResponse serverResponse) {

        if(mServerCallBack != null)
            mServerCallBack.onServeReqResult(serverResponse);

        super.onPostExecute(serverResponse);
    }

    @Override
    protected ServerResponse doInBackground(ServerRequest... obj) {

        ServerRequest request = obj[0];

        try {

            HashMap<String,String> headers = request.getHeader();

            if(SESSION_ID != null){

                headers.put("session",SESSION_ID);
            }

            ServerResponse response = HttpsRequestBuilder.doHttpPost(headers,request.getUrl(),request.getBody());

            if(response != null){

                if(response.getResponseCode() != 401){

                    Map<String,List<String>> resHeaders = response.getHeaderFields();

                    if(resHeaders.containsKey("session")){

                        SESSION_ID = resHeaders.get("session").get(0);
                    }
                }
            }

            return response;

        } catch (IOException e) {

            if(mServerCallBack != null)
                mServerCallBack.onServerReqError(e);

            e.printStackTrace();
        }

        return null;
    }
}

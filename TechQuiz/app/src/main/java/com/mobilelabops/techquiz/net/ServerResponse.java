package com.mobilelabops.techquiz.net;

import java.util.List;
import java.util.Map;

/**
 * Created by bhazarix on 8/29/2017.
 */

public class ServerResponse {

    private int mStatusCode;
    private String mResponseString;
    private Map<String,List<String>> mHeaderFields;

    public String getResponseString() {
        return mResponseString;
    }

    public void setResponseCode(int code){

        this.mStatusCode = code;
    }

    public int getResponseCode(){

        return mStatusCode;
    }

    public void setResponseString(String responseString) {
        this.mResponseString = responseString;
    }

    public Map<String,List<String>> getHeaderFields() {
        return mHeaderFields;
    }

    public void setHeaderFields(Map<String,List<String>> headerFields) {
        this.mHeaderFields = headerFields;
    }

    @Override
    public String toString() {
        return "ServerResponse{" +
                "mResponseString='" + mResponseString + '\'' +
                ", mHeaderFields=" + mHeaderFields +
                '}';
    }
}

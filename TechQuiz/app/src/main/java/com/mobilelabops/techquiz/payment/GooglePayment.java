package com.mobilelabops.techquiz.payment;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.payment.googleHelper.IabHelper;
import com.mobilelabops.techquiz.payment.googleHelper.IabResult;
import com.mobilelabops.techquiz.payment.googleHelper.Inventory;
import com.mobilelabops.techquiz.payment.googleHelper.Purchase;

/**
 * Created by bhazarix on 11/22/2017.
 */

public class GooglePayment  {

    public static String TAG = "GooglePayment";
    static final String ITEM_SKU = "android.techquiz.purchased";
    public static final int REQUEST_PAYMENT = 1001;
    public static final int AMOUNT = 100;

    // The helper object
    IabHelper mHelper;


    public void purchase(Activity app) throws IabHelper.IabAsyncInProgressException {

        if(mHelper != null){

            mHelper.launchPurchaseFlow(app, ITEM_SKU, REQUEST_PAYMENT,
                    mPurchaseFinishedListener, "mypurchasetoken");
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                // Handle error
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                try {
                    consumeItem();
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }

            }

        }
    };

    private void consumeItem() throws IabHelper.IabAsyncInProgressException {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                try {
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                            mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public void dispose() throws IabHelper.IabAsyncInProgressException {
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {


                    } else {
                        // handle error
                    }
                }
            };

    public void init(final Context context,IabHelper.OnIabSetupFinishedListener listener){

        String base64EncodedPublicKey = context.getString(R.string.google_payment_key);

        mHelper = new IabHelper(context, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        mHelper.startSetup(listener);

    }


    void complain(String message) {


    }


}

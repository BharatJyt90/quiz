package com.mobilelabops.techquiz.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.User;
import com.mobilelabops.techquiz.net.IServerCallBack;
import com.mobilelabops.techquiz.net.ServerAsynTask;
import com.mobilelabops.techquiz.net.ServerRequest;
import com.mobilelabops.techquiz.net.ServerResponse;
import com.mobilelabops.techquiz.ui.CustomEditText;
import com.mobilelabops.techquiz.util.Constant;
import com.mobilelabops.techquiz.util.CryptoUtils;
import com.mobilelabops.techquiz.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdatePasswordActivity extends BaseActivity implements IServerCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        setTitle(getString(R.string.update));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void sendUpdatePassReq(){

        CustomEditText passEditText = (CustomEditText) findViewById(R.id.newPassword);

        String password = passEditText.getText().toString();

        if(password.length() < 6){

            passEditText.setError(getString(R.string.password_error));
            return;
        }

        JSONObject params = new JSONObject();
        try {

            String emailId = getIntent().getStringExtra(User.KEY_EMAIL_ID);

            params.put(User.KEY_EMAIL_ID,emailId);

            params.put(User.KEY_PASSWORD, CryptoUtils.applyDoubleEncryption(emailId,password));

            ServerRequest request = new ServerRequest(Constant.KEY_UPDATE_PASS_URL,params.toString(), Util.getJsonContentTypeHeader(this));

            ServerAsynTask asynTask = new ServerAsynTask(this);
            asynTask.execute(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v){

        sendUpdatePassReq();

    }

    @Override
    public void onServeReqResult(ServerResponse result) {

        if(result != null) {
            Log.d("ServerResponse", result.toString());

            if(result.getResponseCode() == 200){

                parseServerResponse(result.getResponseString());
            }

        }
    }

    @Override
    public void onServerReqError(Exception e) {

    }

    @Override
    public void onServerReqStarted() {

    }

    private void parseServerResponse(String response){

        try {

            JSONObject jsonObject = new JSONObject(response);

            int status = jsonObject.getInt(Constant.KEY_STATUS);

            switch (status){

                case Constant.ERROR_CODE_SUCCESS:

                    Toast.makeText(this,"ERROR_CODE_SUCCESS",Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE).edit();
                    CustomEditText passEditText = (CustomEditText) findViewById(R.id.newPassword);
                    String password = passEditText.getText().toString();
                    editor.putString(User.KEY_PASSWORD, Base64.encodeToString(password.getBytes(),Base64.DEFAULT));
                    editor.commit();

                    finish();

                    break;

                case Constant.ERROR_USER_INVALID_EMAIL:

                    // ERROR_CODE_NO_RECORDS_FOUND

                    Toast.makeText(this,"ERROR_USER_INVALID_EMAIL",Toast.LENGTH_SHORT).show();
                    break;

                default:

                    // ops failed
                    String exception = jsonObject.getString(Constant.KEY_EXCEPTION);

                    Toast.makeText(this,exception,Toast.LENGTH_SHORT).show();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

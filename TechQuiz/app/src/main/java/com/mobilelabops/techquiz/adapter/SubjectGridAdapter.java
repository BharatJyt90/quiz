package com.mobilelabops.techquiz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mobilelabops.techquiz.R;
import com.mobilelabops.techquiz.model.Subject;

import java.util.ArrayList;

/**
 * Created by sumanranjan on 14/04/17.
 */

public class SubjectGridAdapter extends BaseAdapter {

    ArrayList<Subject> sArrayList = new ArrayList<Subject>();
    private LayoutInflater mInflater;

    public SubjectGridAdapter(Context context, ArrayList<Subject> subArrayList) {
        sArrayList = subArrayList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {

        if (sArrayList == null)
            return 0;
        return sArrayList.size();
    }

    @Override
    public Object getItem(int position) {

        return sArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent)
    {
        if (view == null) {
            view = mInflater.inflate(R.layout.sub_grid_element, null);
        }

        // Subject Name
        TextView tv = (TextView) view.findViewById(R.id.subject_title);
        tv.setText(sArrayList.get(position).getName());

        // Subject Image

        //ImageView img = (ImageView)view.findViewById(R.id.subject_image);
        //img.setImageResource(R.drawable.subject_poster_bg);

        return view;
    }
}

package com.mobilelabops.techquiz.database; /**
 * Auto Generated using JavaDbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.model.User;


public class UserDbAdapter extends AbstractSqliteDbAdapter<User> {

    public UserDbAdapter() {

        super(User.DATABASE_TABLE_NAME);

    }

    @Override
    String[] getColumnNames() {

        return User.COLUMNS;
    }


    @Override
    protected ContentValues getContentValues(User object) {

        return object.getContentValues();
    }

    @Override
    User createObjectFromCursor(Cursor cursor) {

        return User.createObjectFromCursor(cursor);
    }

}

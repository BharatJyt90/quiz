package com.mobilelabops.techquiz.model;
/**
 * Auto Generated using DbCodeGenerator..
 * Please use appropriate import statement and modify as required.
 **/

import android.content.ContentValues;
import android.database.Cursor;

import com.mobilelabops.techquiz.database.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RedeemablePoints {

    public final static String DATABASE_TABLE_NAME = "redeemable_points";

    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_TOTAL_AVAILABLE = "total_available";
    public static final String KEY_TOTAL_REDEEMED = "total_redeemed";

    public static final String[] COLUMNS = new String[]{KEY_USER_ID, KEY_TOTAL_AVAILABLE, KEY_TOTAL_REDEEMED,};

    private String mUserId;
    private int mTotalEarned;
    private int mTotalRedeemed;

    public void setUserId(String userId) {

        this.mUserId = userId;

    }

    public String getUserId() {

        return this.mUserId;

    }

    public void setTotalEarned(int totalEarned) {

        this.mTotalEarned = totalEarned;

    }

    public int getTotalEarned() {

        return this.mTotalEarned;

    }

    public void setTotalRedeemed(int totalRedeemed) {

        this.mTotalRedeemed = totalRedeemed;

    }

    public int getTotalRedeemed() {

        return this.mTotalRedeemed;

    }


    public static String getCrateTableStatement() {

        StringBuilder createStatement = new StringBuilder("CREATE TABLE ");

        createStatement.append(DATABASE_TABLE_NAME).append(" (");

        createStatement.append(KEY_USER_ID).append(" ").append(DbConstants.DATA_TYPE_TEXT).append(", ");
        createStatement.append(KEY_TOTAL_AVAILABLE).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(", ");
        createStatement.append(KEY_TOTAL_REDEEMED).append(" ").append(DbConstants.DATA_TYPE_INTEGER).append(")");

        return createStatement.toString();
    }

    public ContentValues getContentValues() {
        ContentValues initialValues = new ContentValues();

        initialValues.put(KEY_USER_ID, this.mUserId);
        initialValues.put(KEY_TOTAL_AVAILABLE, this.mTotalEarned);
        initialValues.put(KEY_TOTAL_REDEEMED, this.mTotalRedeemed);
        return initialValues;
    }

    public static RedeemablePoints createObjectFromCursor(Cursor cursor) {
        RedeemablePoints obj = new RedeemablePoints();
        obj.setUserId(cursor.getString((cursor.getColumnIndex(KEY_USER_ID))));
        obj.setTotalEarned(cursor.getInt((cursor.getColumnIndex(KEY_TOTAL_AVAILABLE))));
        obj.setTotalRedeemed(cursor.getInt((cursor.getColumnIndex(KEY_TOTAL_REDEEMED))));
        return obj;
    }

    public JSONObject getJsonObj() throws JSONException {

        JSONObject json = new JSONObject();
        json.put(KEY_USER_ID, this.mUserId);
        json.put(KEY_TOTAL_AVAILABLE, this.mTotalEarned);
        json.put(KEY_TOTAL_REDEEMED, this.mTotalRedeemed);

        return json;

    }

    public static RedeemablePoints createObjectFromJson(JSONObject json) throws JSONException {

        RedeemablePoints obj = new RedeemablePoints();
        if (!json.isNull(KEY_USER_ID)) {

            obj.mUserId = json.getString(KEY_USER_ID);
        }
        if (!json.isNull(KEY_TOTAL_AVAILABLE)) {

            obj.mTotalEarned = json.getInt(KEY_TOTAL_AVAILABLE);
        }
        if (!json.isNull(KEY_TOTAL_REDEEMED)) {

            obj.mTotalRedeemed = json.getInt(KEY_TOTAL_REDEEMED);
        }

        return obj;

    }

    public static JSONArray objListToJSONObjectArray(ArrayList<RedeemablePoints> list) throws JSONException {

        JSONArray array = new JSONArray();
        for (RedeemablePoints obj : list) {

            array.put(obj.getJsonObj());
        }

        return array;
    }
}
